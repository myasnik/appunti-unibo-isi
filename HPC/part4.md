### Part 4

- Pattern: a general solution to a recurring engineering problem
    - Types
        - Embarrassingly Parallel: when the computation can be decomposed in independent tasks that require little or no communication
        - Partition: the input data space (domain) is split in disjoint regions called partitions, each processor operates on one partition; useful when processors can refer to their own partition only and need little or no communication with other processors (locality of reference) 
            - Example (matrix-vector product)

                ![alt text](./res/partition-example.png "Example of partition in matrix-vector product")
            
            - Partition types (based on size)
                - Regular: the domain is split into partitions of roughly the same size and shape

                    ![alt text](./res/1d-part.png "1 dimension partitioning")

                    ![alt text](./res/2d-part-b.png "2 dimension block partitioning")

                    ![alt text](./res/2d-part-cyc.png "2 dimension cyclic partitioning")

                    ![alt text](./res/2d-part-cyc-2.png "2 dimension cyclic partitioning")

                - Irregular: partitions do not necessarily have the same size or shape
            - Granularity of partitions
                - Fine-Grained: a large number of small partitions
                    - Better load balancing
                    - If granularity is too fine, the computation / communication ratio might become too low 
                - Coarse-Grained: a few large partitions
                    - Improves the computation / communication ratio
                    - It might cause load imbalancing
                - Optimal-Grained: problem dependent (try)
            - Example: Mandelbrot set
                - Explanation
                    - $`z_{n}(c) = \begin{cases} 0 & \text{if } n = 0 \\ z_{n-1}^{2}(c) + c & \text{otherwhise} \end{cases}`$ not diverge when $`n \rightarrow +\inf`$

                        ![alt text](./res/mandelbrot.png "Mandelbrot")

                    - With colors (the color of each pixel can be computed independently from other pixels)
                        - If the modulus of $`z_{n}(c)`$ does not exceed 2 after nmax iterations, the pixel is black (the point is assumed to be part of the Mandelbrot set), otherwise the color depends on the number of iterations required for the modulus of $`z_{n}(c)`$ to become > 2
                - Resolution
                    - We can't use coarse-grained partitioning, it can result in uneven load distribution
                    - Two ways
                        - Use fine-grained partitioning, but beware of the possible communication overhead if the tasks need to communicate
                        - Use dynamic task allocation (master-worker paradigm), but beware that dynamic task allocation might incur in higher overhead with respect to static task allocation
                    - [omp-mandelbrot.c](./code/omp-mandelbrot.c)
                        - Coarse-grained partitioning: `OMP_SCHEDULE="static" ./omp-mandelbrot`
                        - Cyclic, fine-grained partitioning (64 rows per block): `OMP_SCHEDULE="static,64" ./omp-mandelbrot`
                        - Dynamic, fine-grained partitioning (64 rows per block): `OMP_SCHEDULE="dynamic,64" ./omp-mandelbrot`
                        - Dynamic, fine-grained partitioning (1 row per block): `OMP_SCHEDULE="dynamic" ./omp-mandelbrot`
        - Master-Worker
            - How it works
                - Apply a fine-grained partitioning (the optimal partition size is in general system and application dependent; it might be estimated by measurement)
                - The master assigns a task to the first available worker
            - Master-Worker vs Cyclic partitioning
                - Master-Worker: more time spent at runtime
                - Cyclic: less time at runtime but it may be that the load is unbalanced
        - Stencil: involve a grid whose values are updated according to a fixed pattern called stencil

            ![alt text](./res/2d-ste.png "2 dimension stencil")

            ![alt text](./res/3d-ste.png "3 dimension stencil")

            - It employs two domains to keep the current and next values
                - Values are read from the current domain
                - New values are written to the next domain (computing the next domain from the current one has embarassingly parallel structure)
                - Current and next are exchanged at the end of each step
            - To handle cells on the border of the domain there are 2 methods
                - Torus (very expensive)
                - Ghost cells (it can be done also in 1D arrays)
                    - Fill ghost cells

                        ![alt text](./res/gc1.png "Ghost cells fill 1")

                        ![alt text](./res/gc2.png "Ghost cells fill 2")

                        ![alt text](./res/gc3.png "Ghost cells fill 3")

                        ![alt text](./res/gc4.png "Ghost cells fill 4")

                        ![alt text](./res/gc5.png "Ghost cells fill 5")

                        ![alt text](./res/gc6.png "Ghost cells fill 6")

            - Example: [game-of-life.c](./code/game-of-life.c)
        - Reduce: is the application of an associative binary operator (e.g., sum, product, min, max...) to the elements of an array $`[x_{0}, x_{1}, ... x_{n-1}]`$
            - Complexity: $`O(log_{2}n)`$
            - How it works: [reduction.c](./code/reduction.c)

                ![alt text](./res/red-sum.png "Reduction sum example") 

            - Work efficient: the parallel algorithm performs the same amount of “work” of the optimal serial algorithm
        - Scan
            - Inclusive: $`y_{0} = x_{0}, y_{1} = x_{0} \space op \space x_{1}, y_{2} = x_{0} \space op \space x_{1} \space op \space x_{2} \space ... \space y_{n-1} = x_{0} \space op \space x_{1} \space op \space ... \space op \space x_{n-1}`$
            - Exclusive: $`y_{0} = 0, y_{1} = x_{0}, y_{2} = x_{0} \space op \space x_{1} \space ... \space y_{n-1} = x_{0} \space op \space x_{1} \space op \space ... \space op \space x_{n-2}`$ (first element is 0 in sum and 1 in product, neutral element)
                - How it works: [prefix-sum.c](./code/prefix-sum.c)
                    - Up-sweep

                        ![alt text](./res/scan-up.png "Up-sweep scan")

                    - Down-sweep

                        ![alt text](./res/scan-down.png "Down-sweep scan")