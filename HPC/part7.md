### Part 7

- Message passing: alternative to shared memory parallel programming, predominant programming model for supercomputers and clusters

- MPI
    - C library (`mpi.h`) that implements message passing programming based on Single Program, Multiple Data (SPMD) 
        - SPMD: same program is executed by P processes and each process may choose a different execution path depending on its ID (rank)
            ```c
            MPI_Init( &argc, &argv );
            foo();  /* executed by all processes */
            if ( my_id == 0 ) {
                do_something(); /* executed by process 0 only */
            } else {
                do_something_else(); /* executed by all other processes */
            }
            MPI_Finalize();
            ```
            - Example: [Hello world](./code/mpi-hello.c)
    - No data races, but communication errors possible
    - Exposes execution model and forces programmer to think about locality
        - Subroutines for
            - Communication
                - Point-to-point (send and receive)
                    - Example: [Point-to-point](./code/mpi-point-to-point.c)
                - Collectives (boradcast) involving multiple processes
            - Synchronization
                - Barrier
                - No locks because there are no shared variables to protect
            - Queries
                - How many processes? Which one am I? Any messages waiting?
                    - `MPI_Comm_size`: number of processes
                    - `MPI_Comm_rank`: a number between 0 and (size - 1), identifying the calling process
    - Needs `mpicc` to compile and `mpirun` to execute
        - Compilation: `mpicc -Wall <source_name> -o <out_name>`
        - Execution (8 processes on localhost): `mpirun -n 8 ./<program_name>`
        - Execution (two processes on host “foo” and one on host “bar”): `mpirun -H foo,foo,bar ./<program_name>`
        - Hostfile
            ```
            // This is myhostfile
            aa slots=4
            bb slots=4
            cc slots=4
            ```
            - To run 4 instances on node “aa” and 2 instances on node “bb”: `mpirun -hostfile myhostfile -n 6 ./<program_name>`
            - To run 2 instances on “aa”, 2 on “bb” and the remaining 2 on “cc”:`mpirun -loadbalance -hostfile myhostfile -n 6 ./<program_name>`
    - Basic concepts
        - Process organization
            - Processes can be collected into groups
            - A group and context together form a communicator
            - A process is identified by its rank in the group associated with a communicator
            - There is a default communicator `MPI_COMM_WORLD` whose group contains all processes
        - Datatypes and communication
            - The data to be sent or received is described by a triple
                - Address (worker address)
                - Count (how many pieces of data to be send)
                - Datatype

                    ![alt text](./res/datatype.png "Datatypes table")

                    - `MPI_DOUBLE_INT` (and similar): struct containing a `DOUBLE` and an `INT`

        - Tags
            - Messages are sent with an accompanying user-defined integer tag, to assist the receiving process in identifying the message ("is this message for me?")
            - Messages can be screened at the receiving end by specifying a specific tag, or not screened by specifying `MPI_ANY_TAG` as the tag in a receive
        - Send/receive paradigm
            - `MPI_Status`: C structure with
                - `int MPI_SOURCE;`
                - `int MPI_TAG;`
                - `int MPI_ERROR;`
            - `MPI_Get_count( &status, datatype, &count )`: can be used to know how many elements of type `datatype` have actually been received
                - Example: [Get count](./code/mpi-get-count.c)
            - Blocking actions
                - Send
                    ```c
                    int buf = 123456;
                    MPI_Send(&buf, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
                    // MPI_Send(&start,count,datatype,dest,tag,comm)
                    ```
                    - The message buffer is described by (`start`, `count`, `datatype`)
                    - `count` is the number of items to send
                    - The target process is specified by `dest`, which is the rank of the target process in the communicator specified by `comm`
                    - When this function returns, the data has been delivered to the system and the buffer can be reused but the message may have not been received yet by the target process
                - Receive
                    ```c
                    MPI_Recv( &buf, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status );
                    // MPI_Recv(&start, count, datatype, source, tag, comm, &status)
                    ```
                    - Waits until a matching (both `source` and `tag`) message is received from the system, and the buffer can then be used
                    - `source` is the process rank in the communicator specified by `comm`, or `MPI_ANY_SOURCE`
                    - `tag` is a tag to be matched, or `MPI_ANY_TAG`
                    - Receiving fewer than `count` occurrences is OK, but receiving more is an error
                    - `status` contains further information (e.g. size of message), or `MPI_STATUS_IGNORE` if no information is needed
                - Deadlocks

                    - Bad

                        ![alt text](./res/dead1.png "Deadlock example")

                    - Good

                        ![alt text](./res/deadlock2.png "Deadlock solution example")

            - NON Blocking actions (Example: [mpi-async.c](./code/mpi-async.c))
                - Send
                    ```c
                    int buf = 123456;
                    MPI_Request req;
                    MPI_Isend(&buf, 1, MPI_INT, 1, 0, MPI_COMM_WORLD, &req);
                    // MPI_Isend(&start,count,datatype,dest,tag,comm,&req)
                    ```
                    - A unique identifier of this request is stored to `req`
                    - This function returns immediately
                - Receive
                    ```c
                    MPI_Request req;
                    MPI_Irecv(&buf, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &req)
                    // MPI_Irecv(&start,count,datatype,source,tag,comm,&req)
                    ```
                    - Processing continues immediately without waiting for the message to be received
                    - A communication request handle `req` is returned for handling the pending message status
                    - The program must call `MPI_Wait()` or `MPI_Test()` to determine when the non-blocking receive operation completes
                        - `MPI_Test(&request, &flag, &status)`
                            - Checks the status of a specified non-blocking send or receive operation
                            - The integer `flag` parameter is set to 1 if the operation has completed, 0 if not
                            - For multiple non-blocking operations, there exist functions to specify any (`MPI_Testany`), all (`MPI_Testall`) or some (`MPI_Testsome`) completions
                        - `MPI_Wait(&request, &status)`
                            - Blocks until a specified non-blocking send or receive operation has completed
                            - For multiple non-blocking operations, there exists variants to specify any (`MPI_Waitany`), all (`MPI_Waitall`) or some (`MPI_Waitsome`) completions
                    - Note: it is OK to use `MPI_Isend` with the (blocking) `MPI_Recv`, and vice-versa
            - Example: [Trap](./code/trap.c)
                - Parallel version: [Trap MPI](./code/mpi-trap0.c)
        - Collective communications paradigm
            - `MPI_Barrier()`: executes a barrier synchronization in a group; when reaching the `MPI_Barrier()` call, a process blocks until all processes in the group reach the same `MPI_Barrier()`
            - `MPI_Bcast()`: broadcasts a message to all other processes of a group (the message is "copied" to all processes)
                ```c
                count = 3;
                src = 1; /* broadcast originates from process 1 */
                MPI_Bcast(buf, count, MPI_INT, src, MPI_COMM_WORLD);
                ```
            - `MPI_Scatter()`: distribute data to other processes in a group (the message is splitted in n_processes parts and each part is "sent" to the corresponding process)
                ```c
                sendcnt = 3; // how many items are sent to each process
                recvcnt = 3; // how many items are received by each process
                src = 1;  // process 1 contains the message to be scattered
                MPI_Scatter(sendbuf, sendcnt, MPI_INT, recvbuf, recvcnt, MPI_INT, src, MPI_COMM_WORLD);
                ```
            - `MPI_Gather()`: merge together data from other processes (every process "sends" his part of message to one process who merges everything)
                ```c
                sendcnt = 3; // how many items are sent by each process
                recvcnt = 3; // how many items are received from each process
                dst = 1;     // message will be merged at process 1
                MPI_Gather(sendbuf, sendcnt, MPI_INT, recvbuf, recvcnt, MPI_INT, dst, MPI_COMM_WORLD);
                ```
            - `MPI_Allgather()`: merge data from other processes and distribute to all (every process "sends" his part of message to every other process and everyone merges everything) 
                ```c
                sendcnt = 3;
                recvcnt = 3;
                MPI_Allgather(sendbuf, sendcnt, MPI_INT, recvbuf, recvcnt, MPI_INT, MPI_COMM_WORLD);
                ```
            - Example: [Parallel vector sum](./code/mpi-vecsum.c)

                ![alt text](./res/parvecsum.png "Parallel vector sum example")

            - `MPI_Scatterv()`: same as `MPI_Scatter` but here the array can be split in parts of arbitrary size
                ```c
                int sendbuf[] = {10, 11, 12, 13, 14, 15, 16}; /* at master */
                int displs[] = {3, 0, 1};      /* assume P=3 MPI processes */
                int sendcnts[] = {3, 1, 4}; 
                int recvbuf[5];
                // ...
                MPI_Scatterv(sendbuf, sendcnts, displs, MPI_INT, recvbuf, 5, MPI_INT, 0, MPI_COMM_WORLD);
                ```
                - `sendcnts`: array of dimensions of blocks (number of elements)
                - `displs`: array of pointers to single blocks to split
                - `recvcnt`: destination array dimension (number of elements)
                - `root`: main process index

                ![alt text](./res/scatterv.png "Example of MPI_Scatterv()")

            - `MPI_Gatherv()`: more or less the syntax is like `MPI_Scatterv`
            - `MPI_Reduce()`: performs a reduction and place result in one process
                ```c
                // Single value
                count = 1;
                dst = 1; /* result will be placed in process 1 */
                MPI_Reduce(sendbuf, recvbuf, count, MPI_INT, MPI_SUM, dst, MPI_COMM_WORLD);

                // Multiple values
                count = 3; // sendbuf will have 3 elements for each process, recvbuf will have 3 elements that are the sum (in this case) of each element i of each process 
                dst = 1;
                MPI_Reduce(sendbuf, recvbuf, count, MPI_INT, MPI_SUM, dst, MPI_COMM_WORLD);
                ```
                - Operators
                    
                    ![alt text](./res/redop.png "Reduction operators")

                - Example: [Parallel trapezoid - Uniform dimension blocks](./code/mpi-trap1.c)
            - `MPI_Allreduce()`: same as `MPI_Reduce` but the result is stored in all processes
                ```c
                count = 1;
                MPI_Allreduce(sendbuf, recvbuf, count, MPI_INT, MPI_SUM, MPI_COMM_WORLD); //No dest parameter
                ```
            - `MPI_Alltoall()`
                ```c
                sendcnt = 1;
                recvcnt = 1;
                MPI_Alltoall(sendbuf, sendcnt, MPI_INT, recvbuf, recvcnt, MPI_INT, MPI_COMM_WORLD);
                ```

                ![alt text](./res/alltoall.png "MPI_Alltoall scheme")

            - `MPI_Scan()`
                ```c
                // Single value
                count = 1;
                MPI_Scan(sendbuf, recvbuf, count, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

                // Multiple values
                count = 3; // Same as above but on more items at the same time
                MPI_Scan(sendbuf, recvbuf, count, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
                ```

                ![alt text](./res/scanmpi.png "MPI_Scan scheme")

                - Example: [mpi_scan.c](./code/mpi-scan.c)
        - `MPI_Abort(comm, err)`: "gracefully" terminates all running MPI processes on communicator `comm` returning the error code `err`
    - Odd-even sort with MPI
        - How it works
            1. Compare all (even, odd) pairs of adjacent elements; exchange them if in the wrong order
            2. Compare all (odd, even) pairs, exchanging if necessary; repeat the step above
        - Parallel: [MPI Odd Even](./code/mpi-odd-even.c)
            
            ![alt text](./res/parevod.png "MPI parallel version of odd-even sort")

            ![alt text](./res/parevod1.png "MPI parallel version of odd-even sort")

            - To safe the program (no deadlock) is useful using `MPI_Sendrecv()` that executes a blocking send and a receive in a single call
            - After p (proc_number) phases the array is sort!
    - Create a derived datatype
        - When useful?
            - For example when is necessary to send a matrix column you could create a datatype to "contain" this column

                ![alt text](./res/datatypecol.png "Sending a matrix column to another thread")

        - How to?
            - Useful functions/vars
                - `MPI_Datatype rowtype` creates the datatype `rowtype`
                - `MPI_Type_commit(...)` commits a new datatype
                - `MPI_Type_free(...)` deallocates a datatype object
            - Types of datatypes
                - Contiguous
                    - `MPI_Type_contiguous(count, oldtype, &newtype)`: contiguous block of `count` elements of an existing MPI type `oldtype` (can be another previously defined custom datatype)
                    - Example
                        ```c
                        MPI_Datatype rowtype; 
                        // Number of values, oldtype, newtype
                        MPI_Type_contiguous( 4, MPI_FLOAT, &rowtype );
                        MPI_Type_commit(&rowtype);
                        ```

                        ![alt text](./res/datatypematrix.png "Example of MPI_Datatype usage")

                        `MPI_Send(&a[2][0], 1, rowtype, dest, tag, MPI_COMM_WORLD);`
                - Vector
                    - `MPI_Type_vector(count, blocklen, stride, oldtype, &newtype)`: array of elements of the existing MPI type `oldtype`
                        - `count`: number of blocks
                        - `blocklen`: number of elements of each block
                        - `stride`: number of elements between start of contiguous blocks
                        - `oldtype`: can be another previously defined datatype
                    - Example
                        ```c
                        int count = 4, blocklen = 1, stride = 4;
                        MPI_Datatype columntype;
                        MPI_Type_vector(count, blocklen, stride, MPI_FLOAT, &columntype);
                        MPI_Type_commit(&columntype);
                        ```

                        ![alt text](./res/datavec.png "Wireshark flow diagram")

                        `MPI_Send(&a[0][1], 1, columntype, dest, tag, MPI_COMM_WORLD);`
                - Indexed
                    - `MPI_Type_indexed(count, array_of_blklen, array_of_displ, oldtype, &newtype)`: an irregularly spaced set of blocks of elements of an existing MPI type `oldtype`
                        - `count`: number of blocks
                        - `array_of_blklen`: number of elements in each block
                        - `array_of_displ`: displacement of each block with respect to the beginning of the data structure
                        - `oldtype`: can be another previously defined datatype
                    - Example
                        ```c
                        int count = 3; 
                        int blklens[] = {1, 3, 4}; 
                        int displs[] = {2, 5, 12};
                        MPI_Datatype newtype;
                        MPI_Type_indexed(count, blklens, displs, MPI_FLOAT, &newtype);
                        MPI_Type_commit(&newtype);
                        ```

                        ![alt text](./res/indexdatatype.png "Indexed datatype example")

                        `MPI_Send(&a[0], 1, newtype, dest, tag, MPI_COMM_WORLD);`

                        - Maintain datatype: `MPI_Recv(&b[0], 1, newtype, src, tag, MPI_COMM_WORLD);`

                            ![alt text](./res/resindexdat.png "Result of indexed recv")

                        - Overwrite datatype: `MPI_Recv(&b[0], 8, MPI_FLOAT, src, tag, MPI_COMM_WORLD);`

                            ![alt text](./res/indexresdat.png "Result of indexed recv")

                - Struct
                    - `MPI_Type_struct(count, array_of_blklen, array_of_displ, array_of_types, &newtype)`: an irregularly spaced set of blocks of elements of existing MPI types `array_of_types`
                        - `count`: number of blocks (and number of elements of the arrays `array_of_*`)
                        - `array_of_blklen`: number of elements in each block
                        - `array_of_displ`: displacement in bytes of each block with respect to the beginning of the data structure (of type `MPI_Aint`)
                        - `array_of_types`: array of `MPI_Datatype`
                    - Example
                        ```c
                        typedef struct {
                            float x, y, z, v;
                            int n, t;
                        } particle_t;
                        
                        int count = 2; 
                        int blklens[] = {4, 2}; 
                        MPI_Aint displs[2], lb, extent; 
                        MPI_Datatype oldtypes[2] = {MPI_FLOAT, MPI_INT}, newtype;
                        MPI_Type_get_extent(MPI_FLOAT, &lb, &extent);
                        displs[0] = 0; 
                        displs[1] = 4*extent;
                        MPI_Type_struct(count, blklens, displs, oldtypes, &newtype);
                        MPI_Type_commit(&newtype);
                        ```

                        ![alt text](./res/structdatatype.png "Example of struct datatype")

                    - Example: [mpi-type-struct.c](./code/mpi-type-struct.c)