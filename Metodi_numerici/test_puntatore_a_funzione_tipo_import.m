x=linspace(-pi,pi,100)
fp=@funzione_sinusoidale;

figure
plot(x,fp(x))




f=@(x) sin(x);

[massimo,minimo]=calcolo(f,-pi,pi)
x=linspace(-pi,pi,100)
figure
plot(x,f(x))

f1=@(x) cos(x)+x.^2;

[massimo,minimo]=calcolo(f1,-pi,pi)
figure
plot(x,f1(x))

function [massimo,minimo]=calcolo(fp,ei,ef)
n=100;
x=linspace(ei,ef,n);
y=fp(x);
massimo=max(y);
minimo=min(y);
end