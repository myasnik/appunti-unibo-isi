% Es 1
a=[2 2 2 3]
A=[a; a; a]
% oppure 
A=[ones(3)*2 ones(3,1)*3] 

% Es 2
b=diag(ones(3,1)*2)
c=[1:10; 1:10; 1:10]
B=[b c]
% oppure
B=[eye(3)*2 [1:10; 1: 10;1:10]]

% Es 3
a=diag(ones(5,1)*2)
b=diag(ones(4,1)*-1,-1)
c=diag(ones(4,1)*-1,1)
C=[a+b+c]
% oppure tutto su una linea con i più

a=[2 2 3 3]
b=zeros(2,4)
c=eye(4)*5
D=[a; a; b; c(3,:); c(4,:)]
