%Regula falsi

function [x, xk, it] = falsa_posizione(fname,a,b,tol,maxit)

fa=fname(a);
fb=fname(b);

if sign(fa)*sign(fb) >= 0
    error('Intervallo non corretto');
else
    it = 0;
    fxk=fname(a);
    while it <= maxit && abs(b-a) >= tol+eps*max([abs(a) abs(b)]) && abs(fxk) >= tol
        it = it + 1;
        xk(it)=a-fa*(b-a)/(fb-fa); % Intersezione della retta che congiunge gli estremi dell'intervallo [a,b] con l'asse delle x (y = 0)
        fxk=fname(xk(it));
        if fxk == 0
            break;
        elseif sign(fxk)*sign(fa) > 0
            a=xk(it);
            fa=fxk;
        elseif sign(fxk)*sign(fb) > 0
            b=xk(it);
            fb=fxk;
        end
    end
    x=xk(it);
end