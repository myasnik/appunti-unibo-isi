clc
clear all
close all

syms x

fx=exp(-x)-(x+1);
alpha=0;
a=-1;
b=2;
x0=-0.5;
xm1=-0.3;

f=matlabFunction(fx);
df=matlabFunction(diff(fx,x,1));

tolx=1.e-12;
tolf=1.e-12;
nmax=1000;

[xbisez,xkbisez,itbisez]=bisez(f,a,b,tolx);
fprintf('Zero calcolato con bisezione = %8.15f in %d iterazioni \n',xbisez,itbisez);

[xfalsi,xkfalsi,itfalsi]=falsa_posizione(f,a,b,tolx,nmax);
fprintf('Zero calcolato con regula falsi = %8.15f in %d iterazioni \n',xfalsi,itfalsi);

[xcorde,xkcorde,itcorde]=corde(f,df,x0,tolx,tolf,nmax);
fprintf('Zero calcolato con corde = %8.15f in %d iterazioni \n',xcorde,itcorde);

[xsecanti,xksecanti,itsecanti]=secanti(f,xm1,x0,tolx,tolf,nmax);
fprintf('Zero calcolato con secanti = %8.15f in %d iterazioni \n',xsecanti,itsecanti);

[xnewton,xknewton,itnewton]=newton(f,df,x0,tolx,tolf,nmax);
fprintf('Zero calcolato con newton = %8.15f in %d iterazioni \n',xnewton,itnewton);

ek_bisezione=abs(xkbisez-alpha);
ek_falsi=abs(xkfalsi-alpha);
ek_corde=abs(xkcorde-alpha);
ek_newton=abs(xknewton-alpha);
ek_secanti=abs(xksecanti-alpha);

semilogy(1:itbisez,ek_bisezione,'go-',1:itfalsi,ek_falsi,'mo-',...
    1:itcorde,ek_corde,'bo-',1:itnewton,ek_newton,'ro-',1:itsecanti,ek_secanti,'co-');

legend('bisezione','falsi','corde','newton','secanti');

ordinebisezione=stima_ordine(xkbisez,itbisez)
ordinefalsi=stima_ordine(xkfalsi,itfalsi)
ordinecorde=stima_ordine(xkcorde,itcorde)
ordinenewton=stima_ordine(xknewton,itnewton)
ordinesecanti=stima_ordine(xksecanti,itsecanti)

