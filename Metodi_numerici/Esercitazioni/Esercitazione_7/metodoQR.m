function [a]=metodoQR(x,y,n)
% n grado del polinomio approssimante e a vettore dei suoi coefficienti
% Costruzione della matrice rettangolare di Vandermonde
l = length(x);
H_completa = vander(x);
H = H_completa(:,l-n:l);
[Q, R] = qr(H);
y1 = Q' * y;
a = Usolve(R(1 : n + 1, :), y1(1 : n + 1));