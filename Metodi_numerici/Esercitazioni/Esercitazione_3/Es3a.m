clear all
close all
clc

format long e

A=[5 10; 2 1];
b=[15 ; 1];

K=cond(A)

x=A\b

deltaA=[0 0.1; 0 0];

% Soluzione sistema perturbato
xp=(A+deltaA)\b

% Errore relativo sui dati
err_dati=norm(deltaA, inf)/norm(A, inf)

err_dati_perc=err_dati*100

% Errore sulla soluzione del sistema
err_sol=norm(xp-x, inf)/norm(x, inf)
err_sol_perc=err_sol*100

% Grafico delle rette rappresentate dalle due equazioni
f1=@(x) (15-5*x)/10;
f2=@(x) (1-2*x);

f1_p=@(x) (15-5*x)/10.1;

xv=linspace(-1,1,100);

figure(1)
plot(xv,f1(xv),'b-',xv,f2(xv),'r--')

figure(2)
plot(xv,f2(xv),'b-',xv,f1_p(xv),'g--')