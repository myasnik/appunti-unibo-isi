function [appr_new,n]=esp_taylor_2(x)
% Output: appr_new=exp(x), n=ultimo indice di sommatoria considerato
% Calcolo exp(x) con serie di Taylor troncata in accordo a test dell'incremento 
% usando l'espressione equivalente exp(-x)=1/exp(x)
[appr_new,n]=esp_taylor_1(-x);
appr_new=1.e0/appr_new; 

%

 
