function yv=interpL(x,y,xv)

n=length(x);
L=zeros(n,length(xv));

for j=1:n
    p=plagr(x,j);
    L(j,:)=polyval(p,xv);
end

yv=y*L;