clc
clear all
close all

i = 0:20;
x = 10.^i;

A = (1./x) - (1./(x+1));
B = 1./((x+1).*x);

E = abs(A-B)./abs(B)

u = eps*0.5;

%Stima errore teorico caso in cui x,x+1 appartengono a F

E1 = u*(1 + abs(x) + abs(x+1))

%Stima errore teorico caso in cui x non appartiene a F

E2 = (2*abs(x+1)+2*abs(x)+1+(abs(x).^2)./abs(x+1)) * u

figure 
loglog(x,E,'b-',x,E1,'r--',x,E2,'k:')
legend('Errore calcolato', 'Stima errore 1', 'Stima errore 2')