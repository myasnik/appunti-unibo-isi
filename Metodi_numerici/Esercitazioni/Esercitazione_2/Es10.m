clc
clear all
close all

k = 0:16;
n = 10.^k;

e = exp(1)
matr = [e e e e e e e e e e e e e e e e e];

x1 = (1./n + 1).^n

figure 
semilogy(k,x1,'r--',k,matr,'b--')
legend('Risultati tramite approssimazione formula', 'Risultati tramite formula corretta')