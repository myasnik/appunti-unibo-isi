clc
clear all
close all

f=@es1_punto1;

[x1,xk1,it1]=bisezione(f,-1,0,10^(-6))

[x2,xk2,it2]=bisezione(f,0.9,1.1,10^(-6))

[x3,xk3,it3]=bisezione(f,1.1,1.2,10^(-6))