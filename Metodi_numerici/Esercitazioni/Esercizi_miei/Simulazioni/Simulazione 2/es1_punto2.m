function es1_punto2(func, interval)

f=zeros(1, length(interval));

for i=1:length(interval)
    f(i)=es1_punto1(interval(i));
end

figure
plot(interval, f, 'b-',interval, zeros(1, length(interval)), 'm-');