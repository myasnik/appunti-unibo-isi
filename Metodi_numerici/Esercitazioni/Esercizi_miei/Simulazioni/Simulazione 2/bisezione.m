function [x,xk,it]=bisezione(func,a,b,tol)

fa=func(a);
fb=func(b);

if sign(fa)*sign(fb)>0
    disp("Intervallo errato");
    return;    
else
    maxit=ceil(log(abs(b-a)/tol)/log(2));
    it=0;
    while it<maxit && abs(b-a)>=tol+eps*max(abs(a),abs(b))
        it=it+1;
        xk(it)=a+(b-a)/2;
        fxk=func(xk(it));
        if fxk==0
            break;
        elseif sign(fa)*sign(fxk)>0
            a=xk(it);
            fa=fxk;
        elseif sign(fb)*sign(fxk)>0
            b=xk(it);
            fb=fxk;
        end
    end
    x=xk(it);        
end