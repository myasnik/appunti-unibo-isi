function [x,it,mezzo]=bisez(fname,a,b,tol)

maxit=round(log((b-a)/tol)/log(2)+1);
fprintf('n. di passi necessari=%d \n',maxit);

fa=feval(fname,a);
fb=feval(fname,b);
if sign(fa)*sign(fb)>=0
    error('intervallo non corretto');
else
    it=0;
    while abs(b-a)>=tol+eps*max([abs(a) abs(b)]) && it<=maxit
        it=it+1;
        mezzo(it)=a+(b-a)*0.5;
        fprintf('it=%d, x=%12.7f\n',it,mezzo(it));
        fmezzo=feval(fname,mezzo(it));
        if fmezzo==0
            break;
        end
        if sign(fmezzo)*sign(fa)>0
            a=mezzo(it);
            fa=fmezzo;
        else
            b=mezzo(it);
            fb=fmezzo;
        end
    end
    if it>maxit
        error('Raggiunto numero max di iterazioni');
    else
        x=mezzo(it);
    end
end
