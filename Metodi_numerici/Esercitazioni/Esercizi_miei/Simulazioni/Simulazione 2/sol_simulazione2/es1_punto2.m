clear all
close all

t_range = -2:0.01:2;

f_range = [];

for t=t_range
    
    f=es1_punto1(t);
    
    f_range = [f_range f];
    
end

plot(t_range,f_range,'k-',t_range,zeros(size(t_range)),'r--')
grid on
title('grafico di det(A(t))')
