function [IN,N]=traptoll(fun,a,b,tol)

Nmax=3000;
err=1;

N=1;
IN=TrapComp(fun,a,b,N);

while N<=Nmax && err>tol
    N=2*N;
    I2N=TrapComp(fun,a,b,N);
    err=abs(IN-I2N)/3;
    IN=I2N;
end

if N>Nmax
    disp('ragginto nmax di intervalli')
else
    fprintf('Q=%5.12f, N=%d \n',IN,N)
end
