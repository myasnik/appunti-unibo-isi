function X=sol_nsis(A,B)
% Funzione per risolvere l'equazione matriciale AX=B

%Input:
% A, B matrici

%Output:
% X matrice soluzione del sistema AX=B

[L,U,P,flag]=LU_parziale(A);
%[L,U,P,flag]=LU_nopivot(A);

if flag==0
    % Prima versione
    [m,n]=size(B);
    for i=1:n
        Y(:,i)=Lsolve(L,P*B(:,i));  %L\(P*B(:,i));
        X(:,i)=Usolve(U,Y(:,i));    %U\Y(:,i);
      end
    % Versione alternatva
    % Y=L\(P*B);
    % X=U\Y;
    % %o semplicemente
    % %X=U\(L\(P*B));
    
else
    disp('non posso proseguire');
    X=[];
    return
end