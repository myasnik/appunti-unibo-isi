clc
close all
clear all
format long e

func=@(x) -x^2+2*x+5;
a = -2;
b = 3;
nmax = 100;

[x,xk,it]=secanti_mio(func,a,b,eps,eps,nmax)