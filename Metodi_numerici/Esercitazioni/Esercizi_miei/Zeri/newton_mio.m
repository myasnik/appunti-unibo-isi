function [x1,xk,it]=newton_mio(func,func1,x0,tolx,tolf,nmax)

fx0=func(x0);
dfx0=func1(x0);

if abs(dfx0)>eps
    d=fx0/dfx0;
    x1=x0-d;
    fx1=func(x1);
    it=1;
    xk(it)=x1;
else
    disp("Derivata prima nulla");
    return;
end

while it<nmax && abs(fx1)>=tolf && abs(d)>=tolx+eps*abs(x1)
    x0=x1;
    fx0=fx1;
    dfx0=func1(x0);
    if abs(dfx0)>eps
        d=fx0/dfx0;
        x1=x0-d;
        fx1=func(x1);
        it=it+1;
        xk(it)=x1;
    else
        disp("Derivata prima nulla");
        return;
    end
end

if it==nmax
    disp("MAXIT");
end