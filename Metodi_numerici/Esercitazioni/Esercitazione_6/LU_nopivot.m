  function [L,U,P,flag]=LU_nopivot(A)
  % Fattorizzazione PA=LU senza pivot 
  % In output:
  %  L matrice triangolare inferiore
  %  U matrice triangolare superiore
  %  P matrice identit�
  %    tali che  LU=PA=A

  % Test dimensione
  [n,m]=size(A);
  flag=0;
  if n ~= m, disp('errore: matrice non quadrata'), L=[]; U=[]; P=[]; flag=1; return, end
  
  P=eye(n);
  % Fattorizzazione
  for k=1:n-1
      %Test pivot 
      if A(k,k) ==  0, disp('elemento diagonale nullo'), L=[]; U=[]; flag=1; return, end
      %Eliminazione gaussiana
      A(k+1:n,k)=A(k+1:n,k)/A(k,k);                         % Memorizza i moltiplicatori	  
      A(k+1:n,k+1:n)=A(k+1:n,k+1:n)-A(k+1:n,k)*A(k,k+1:n);  % Eliminazione gaussiana sulla matrice
  end
  
  L=tril(A,-1)+eye(n); % Estrae i moltiplicatori 
  U=triu(A);           % Estrae la parte triangolare superiore + diagonale
  