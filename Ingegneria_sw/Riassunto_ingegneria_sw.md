# Ingegneria del Software

### Schema esercizi esame

1. Diagramma dei casi d'uso + Diagramma delle classi (tutti e due sempre)
2. Diagramma di stati o Diagramma di attività o Diagramma di sequenza
3. Progettazione delle associazioni 
4. Teoria

### [Esercizi](https://gitlab.com/p2f/software-engineering/course)

### Parte 1

- Ciclo di vita di un software
    - Tipi
        - Waterfall: parto dall'alto e non torno più su
        - Fontana: a waterfall ma "ricircola"
        - ...
    - Fasi
        1. Definizione strategica
            - Vengono prese decisioni sullarea aziendale che deve essere oggetto di automazione
        2. Pianificazione
            - Vengono definiti gli obiettivi, evidenziati i fabbisogni e viene condotto uno studio di fattibilità per individuare possibili strategie di attuazione e avere una prima idea dei costi, dei benefici e dei tempi
                - Tempi
                    - Solari: quanti giorni impiegherò a concludere il software
                    - Uomo: dipende da quante persone lavorano e a quanto ammonta il turno di lavoro
        3. Controllo di qualità (spalmato su tutto il ciclo di vita)
            - Viene predisposto un piano di controllo di qualità per il progetto, allo scopo di garantire il rispetto delle specifiche e di controllare che il sistema realizzato si comporti come previsto
        4. Analisi dei requisiti
            - Formalizza i requisiti avvalendosi di tecniche di modellazione della realtà e produce macro-specifiche per la fase di progettazione
            - In pratica un analista va dal cliente e capisce cosa il software deve fare, poi genera una specifica dei requisiti che diventerà l'input per le successive fasi
                - Specifica dei requisiti: accordo tra il produttore di un servizio e il suo consumatore (contratto)
                    - La difficoltà per questo tipo di specifica è data dalla diversità dei linguaggi usata dalle due parti
                    - Qualità
                        - Chiarezza: ogni specifica deve indicare quanto più chiaramente possibile le operazioni e i soggetti del processo che descrive 
                        - Non ambiguità: il processo descritto dalla specifica deve essere definito in modo completo e dettagliato
                        - Consistenza: le specifiche non devono contenere punti contraddittori 
                    - Linguaggio di stesura
                        - Tipi
                            - Informale: il linguaggio naturale, alla base della comunicazione durante le interviste tra analista e utente, non può essere adottato come unico mezzo per produrre documenti di specifica per le innumerevoli ambiguità di significato
                            - Semiformale (quello da noi scelto): notazione grafica, che presenta una semantica sfumata, accoppiata con descrizioni in linguaggio naturale (es: ER)
                            - Formale
                                - Linguaggi di specifica basati sulla logica dei predicati
                                - Linguaggi di specifica algebrici
                                - Linguaggi concettuali per basi di dati
                        - Formalismo
                            - Operazionale: definisce  il sistema descrivendone il comportamento, normalmente mediante un modello (es: Ellisse = "E è il percorso che si ottiene muovendosi in modo che la somma delle distanze tra due punti fissi p1 e p2 rimanga invariata")
                            - Dichiarativo: definise il sistema dichiarando le proprietà che esso deve avere (es: Ellisse = ax2+by2+c=0)
            - Importanza: più tardi viene scoperto un errore nel ciclo di sviluppo del software, maggiore è il costo di riparazione
            - Il processo di analisi è incrementale e porta per passi successivi alla stesura di un insieme di documenti in grado di rappresentare un modello dell'organizzazione e comunicare, in modo non ambiguo, una descrizione esauriente, coerente e realizzabile dei vari aspetti statici, dinamici e funzionali di un sistema informatico
                - Aspetti
                    - Statici: definisco quello che c'è (uno studente, un prof..)
                    - Dinamici: come variano nel tempo gli oggetti in base a degli avvenimenti
                    - Funzionali: definisco funzioni (lo studente frequenta lezioni, il prof mette voti..)
            - Metodi
                - Analisi orientata agli oggetti (aspetto statico, tipo ER)
                    - Identificazione degli oggetti
                    - Identificazione delle interrelazioni fra oggetti
                - Analisi orientata alle funzioni (aspetto funzionale)
                    - Rappresentare il sistema come
                        - Un insieme di flussi informativi
                        - Una rete di processi che trasformano flussi informativi
                - Analisi orientata agli stati (aspetto dinamico, tipo FSM)
                    - Stati operativi in cui si può trovare il sistema
                    - Transizioni di stato
            - Uso dei metodi: la tendenza attuale è integrare metodi dei tre tipi, tenendo però conto della tipologia di applicazione
                - Applicazioni orientate agli oggetti: l'aspetto più significativo è costituito dalle informazioni, le funzioni svolte sono relativamente semplici
                - Applicazioni orientate alle funzioni: la complessità risiede nel tipo di trasformazione input-output operata
                - Applicazioni orientate al controllo: l'aspetto più significativo da modellare è la sincronizzazione fra diverse attività cooperanti nel sistema
            - Astrazione
                - Interessa sia gli oggetti che le funzioni che gli stati
                - Meccanismi
                    - Classificazione: raggruppare in classi oggetti, funzioni, o stati in base alle loro proprietà (instance of)
                    - Generalizzazione: cattura le relazioni è-un (is a) ovvero permette di astrarre le caratteristiche comuni fra più classi definendo superclassi
                    - Aggregazione: esprime le relazioni parte-di (part of) che sussistono tra oggetti, tra funzioni, tra stati
                    - Associazioni
        5. Progettazione 
            - Riguarda tutte quelle attività che permettono di passare dalla raccolta ed elaborazione dei requisiti di un sistema software alla sua effettiva realizzazione
            - Il sistema complessivo viene suddiviso in più sottosistemi (modularità, divide et impera)
            - Tipi
                - Del sistema
                    - Interpreta i requisiti in una soluzione architetturale di massima. Produce specifiche indipendenti dai particolari strumenti che saranno usati per la costruzione del sistema
                - Esecutiva
                    - Vengono descritti struttura e comportamento dei componenti dell'architettura, producendo specifiche che possano dar luogo, attraverso il ricorso a strumenti di sviluppo opportuni, a un prodotto funzionante
                    - Se eseguita bene il lavoro del programmatore è ridotto alla traduzione della programmazione esecutiva in codice
            - Esigenze (contrastanti)
                - Progetto sufficientemente astratto per poter essere agevolmente confrontato con le specifiche da cui viene derivato
                - Progetto sufficientemente dettagliato in modo tale che la codifica possa avvenire senza ulteriori necessità di chiarire le operazioni che devono essere realizzate
            - Obiettivi (della progettazione e quindi del software)
                - Affidabilità
                - Modificabilità
                - Comprensibilità
                - Riusabilità
        6. Realizzazione e collaudo in fabbrica
            - Il sistema viene implementato sulla piattaforma prescelta e viene testato internamente (α-test) sulla base dei casi prova definiti durante la fase di analisi
        7. Certificazione
            - L'attività di certificazione del software ha lo scopo di verificare che esso sia stato sviluppato secondo i criteri previsti dal metodo tecnico di progetto, in conformità alle specifiche di sistema e a tutta la documentazione di progetto
            - Se una software house è molto conosciuta allora probabilmente avrà una certificazione che garantisce la qualità dei suoi prodotti
            - E' possibile chiedere ad un ente terzo di certificare il mio codice in base a certi standard di qualità
        8. Installazione
            - Il sistema viene installato e configurato, e vengono recuperati gli eventuali dati pregressi
        9. Collaudo del sistema installato
            - Gli utenti testano "in vitro" il prodotto installato (β-test). Si possono evidenziare errori bloccanti (malfunzionamenti che pregiudicano l'attività di collaudo), errori non bloccanti (malfunzionamenti che non pregiudicano l'attività di collaudo), problemi di operatività (una funzionalità richiesta non viene attuata adeguatamente) e funzionali (una funzionalità richiesta non è implementata)
            - La classificazione delle mancanze va fatta molto bene in quanto per alcune (quelle presenti nel contratto) è possibile richiedere il miglioramento/reimplementazione gratuitamente, per altre (non presenti o non chiare nel contratto) sarà necessario pagare una somma supplementare
        10. Esercizio
            - Quando il collaudo dà esito positivo il sistema viene avviato ("messo in produzione"), inizialmente affiancando e poi sostituendo gradualmente l'eventuale sistema preesistente
        11. Diagnosi
            - Durante l'esercizio gli utenti rilevano eventuali errori che in base alla gravità devono essere fixati più o meno tempestivamente 
        12. Manutenzione
            - Gli errori che si manifestano durante il funzionamento vengono segnalati e corretti (manutenzione correttiva). Può inoltre essere necessario intervenire sul software per adattarlo ai cambiamenti del dominio applicativo (manutenzione adattativa)
        13. Evoluzione
            - Si valutano le possibilità di far evolvere il sistema incorporando nuove funzionalità o migliorandone l'operatività (manutenzione evolutiva o perfettiva)
        14. Pagamento della manutenzione
            - Viene stipulato un contratto in cui si paga un tot all'anno e questo tot comprende i costi di manutenzione (licenza)
            - Se ci sono variazioni troppo grandi ricompro il software
        15. Messa fuori servizio
        
### Parte 2

- Oggetto
    - Definizione: un oggetto è un individuo sostanziale che possiede un identità e un insieme   di   proprietà,   che   ne   rappresentano   lo   stato   e   il comportamento
    - Ogni oggetto è caratterizzato da: 
        - Una identità (OID, Object IDentifier) che gli viene associata all’atto della creazione, non può essere modificata ed è indipendente dallo stato corrente dell’oggetto
        - Uno stato definito come l’insieme dei valori assunti a un certo istante da un insieme di attributi
        - Un comportamento definito da un insieme di operazioni caratterizzate da un nome, dei parametri e il valore restituito (signature); l'insieme di tutte le operazioni definiscono l'interfaccia dell'oggetto
    - Poiché  un  oggetto  può  anche  includere  riferimenti  ad  altri  oggetti, risulta possibile creare oggetti complessi (part of)
- Astrazione
    - E' una rappresentazione di un insieme di oggetti “simili”, caratterizzato da una struttura per i dati e da un’interfaccia che definisce quali sono le operazioni associate agli oggetti, ovvero l’insieme dei servizi implementati
    - Un tipo è sottotipo di un supertipo se la sua interfaccia contiene quella del supertipo
        - Un sottotipo eredita l’interfaccia del suo supertipo
        - L’interfaccia non vincola l’implementazione del servizio offerto ovvero il comportamento effettivo
        - Oggetti con la stessa interfaccia possono avere implementazioni completamente diverse 
- Classe
    - Fornisce una realizzazione di un tipo di dati astratto, specifica cioè un’implementazione per i metodi a esso associati
    - Un oggetto è sempre istanza di esattamente una classe 
    - Tutti gli oggetti di una classe hanno gli stessi attributi e metodi
        - Esistono metodi di due tipi
            - Restituiscono astrazioni significative sullo stato dell’oggetto cui sono applicati (es: estrapolo età da data di nascita)
            - Alterano lo stato (es: setto se un oggetto persona è sposato o no)
- Incapsulamento 
    - Il principio di incapsulamento sancisce che gli attributi di un oggetto possono essere letti e manipolati solo attraverso l’interfaccia che l’oggetto stesso mette a disposizione
        - Per l’utilizzo di una classe è sufficiente conoscerne l’interfaccia  pubblica; i dettagli implementativi sono nascosti all’interno (scatola nera)
        - La   modifica   dell’implementazione di una classe non si ripercuote sull’applicazione, a patto che non ne venga variata l’interfaccia
        - Poiché  la  manipolazione  diretta  degli  attributi  della  classe avviene   esclusivamente   tramite   i   suoi   metodi,   viene fortemente  ridotta  la  possibilità  di  commettere errori nella gestione dello stato degli oggetti
        - Il debugging delle applicazioni è velocizzato, poiché l’incapsulamento rende più semplice identificare la sorgente di un errore 
- Metodo
    - Un metodo cattura l’implementazione di una operazione
    - I metodi possono essere classificati in:
        - Costruttori: per costruire oggetti a partire da parametri di ingresso restituendo l’OID dell’oggetto costruito
        - Distruttori: per cancellare gli oggetti ed eventuali altri oggetti ad essi collegati
        - Accessori: per restituire informazioni sul contenuto degli oggetti (proprietà derivate)
        - Trasformatori: per modificare lo stato degli oggetti e di eventuali altri oggetti ad essi collegati
    - I metodi possono essere:
        - Pubblici
        - Protetti
        - Privati
- Ereditarietà
    - Il  meccanismo  di  ereditarietà  permette  di  basare  la  definizione  e implementazione di una classe su quelle di altre classi
    - E’  possibile  definire  relazioni  di  specializzazione/generalizzazione tra  classi:  la  classe  generalizzante  viene  detta superclasse,  la classe specializzante viene detta sottoclasse o classe derivata
    - Ciascuna  sottoclasse  eredita  dalla  sua  superclasse  la  struttura  ed  i comportamenti, ovvero gli attributi, i metodi e l’interfaccia; può però specializzare le caratteristiche ereditate e aggiungere caratteristiche specifiche non presenti nella superclasse
    - Si  parla  di ereditarietà  multipla quando  una  sottoclasse  può essere derivata contemporaneamente da più superclassi
    - Date due classi A e B di cui B è una sottoclasse di A, esiste di fatto la relazione B is-a A (B è un A)
        - Gli oggetti istanze di B possano a tutti gli effetti essere utilizzati al posto  di  oggetti  istanze  di  A
        - Non  è  vero  il  contrario
- Polimorfismo
    - Possibilità di creare metodi con lo stesso nome ma implementazioni differenti
    - Tipi
        - Overload: possibilità di definire, all’interno di una stessa classe, più metodi con lo stesso nome ma signature (insieme dei parametri) differenti
        - Override: possibilità di ridefinire, all’interno di una sottoclasse, l’implementazione di un metodo ereditato
    - Late binding
        - Permette a ciascun oggetto di rispondere a uno stesso messaggio in modo appropriato a seconda della classe da cui deriva (es: il metodo ruota chiamato su figura geometrica si comporterà diversamente se la figura geometrica è un rettangolo o un cerchio)
- Delegazione
    - Si  parla  di  delegazione  quando  un  oggetto  A contiene  al  suo  interno  un  riferimento  a  un altro oggetto B, cosicché A (che risulta essere in  questo  caso  un oggetto  complesso)  può delegare   alcune   funzioni   alla   classe   a   cui appartiene B
- Lo sviluppo di sistemi a oggetti
    - Imparare una nuova tecnica di progettazione è molto più difficile che imparare un nuovo linguaggio, poiché richiede di modificare sostanzialmente il nostro modo di pensare
    - Dall'approccio funzionale a quello ad oggetti
        - Approccio funzionale: la decomposizione funzionale è un’analisi di tipo top-down tradizionalmente impiegata nel paradigma procedurale, basata sui concetti di procedura e flusso di dati
            - La domanda fondamentale è: cosa fa il sistema, qual è la sua funzione?
            - Ad alto livello di astrazione, il sistema viene caratterizzato tramiteun’unica funzionalità
            - I blocchi di base dell’applicazione sono i task (compiti), che durante l’implementazione daranno luogo a procedure, e sono legati alla specifica soluzione proposta 
            - Problemi
                - Nessun modello unificante per integrare le diverse fasi
                - Mancanza di iterazione nella progettazione: si adotta il modello a cascata, in cui le attività sono viste come una progressione lineare 
                - Mancanza di estendibilità: non si considerano le possibili evoluzioni del sistema
                - Poca attenzione al problema della riusabilità: ogni sistema viene ricostruito a partire da zero, per cui i costi di manutenzione sono alti 
                - La progettazione dei dati viene trascurata, poiché le strutture dati sono determinate dalle strutture procedurali
        - Approccio a oggetti
            - Analisi: va dall’inizio del progetto fino all’analisi delle specifiche utente e allo studio di fattibilità (cosa il sistema deve fare)
            - Design: progettazione logica e fisica del sistema (come lo deve fare) 
            - Implementazione: scrittura del codice, test di verifica, validazione, manutenzione
                - I confini tra le fasi non sono più distinti
                - Il processo di sviluppo è iterativo: si adotta il modello a fontana, in cui lo sviluppo raggiunge un alto livello per poi ritornare a un livello precedente e risalire di nuovo
                - L’ereditarietà permette di aggiungere nuove caratteristiche a un sistema riducendo i costi di manutenzione (estendibilità), e di costruire nuove funzionalità a partire dall’esistente (riusabilità) riscrivendo solo quella parte di codice inadeguato e solo per gli oggetti che ne hanno bisogno
            - Benefici
                - La decomposizione è orientata alla modellazione
                    - I blocchi di base dell’applicazione sono entità che interagiscono, modellate come classi di oggetti, e sono legate alla formulazione originale del problema
                    - I risultati dell’analisi non sono un semplice input del design, ma ne sono parte integrante: analisi e design lavorano insieme per sviluppare un modello del dominio del problema
                - Il progetto dettagliato è rimandato nel tempo e nascosto all’interno di ciascuna classe
                    - Algoritmi e strutture dati non sono più “congelati” a un alto livello del progetto
                    - Si ha più flessibilità, poiché un cambiamento nell’implementazione non implica variazioni consistenti alla struttura del sistema
                - I sistemi sviluppati a oggetti risultano più stabili nel tempo di quelli progettati per decomposizione funzionale
                    - Le caratteristiche dei domini applicativi variano più lentamente nel tempo rispetto alle funzionalità richieste ai sistemi
                - La produttività è alta
                    - Fasi diverse dell’analisi dei requisiti e del ciclo di vita possono essere svolte contemporaneamente
                - C’è la possibilità di sviluppare rapidamente prototipi che possono risultare di valido ausilio per la certificazione dell’analisi dei requisiti
                - E’ possibile che il design e l’implementazione a classi richiedano tempi elevati, volendo provvedere generalità e riusabilità; a fronte di ciò si ha però una drastica riduzione dei costi di manutenzione
    - Analisi
        - Determinare la funzionalità del sistema (ci sono poche assunzioni riguardo al linguaggio di programmazione da usare)
        - Creare una lista delle classi che sono parte del sistema (le classi sono relativamente “piccole” e molte sono abbastanza generali da poter essere riusate in futuri progetti)
        - Distribuire le funzionalità del sistema attraverso le classi individuate (il progetto non ha un “centro” esplicito)
    - Design
        - Determinare metodi e attributi di ciascuna classe (le classi sono raggruppate in moduli)
        - Progettare algoritmi per implementare le operazioni (i percorsi di accesso ai dati sono ottimizzati)
        - Progettare le associazioni

### Parte 3

- UML (linguaggio)
    - Caratteristiche
        - Definisce una notazione standard, basata su un metamodello (modello che modella altri modelli) integrato degli “oggetti” che compongono un sistema software
        - Non prescrive una sequenza di processo, cioè non dice “prima bisogna fare questa attività, poi quest’altra”
        - E' indipendente dai metodi e dal fine
        - E' un linguaggio non proprietario, standard
    - Generalità
        - UML fornisce i costrutti per
            - Analisi dei requisiti tramite i casi d’uso
            - Analisi e progetto OO
            - Modellazione dei componenti
            - Modellazione della struttura e della configurazione
        - Alcuni modelli sono espressi tramite l'uso di diagrammi grafici
        - Ogni entità del modello può comparire in uno o più diagrammi, che ne rappresentano una proiezione
        - Nei vari diagrammi, tutti i concetti e le entità che presentano similitudini sono espressi con la medesima notazione
        - Importante: modello != diagramma, un certo elemento può comparire in più diagrammi ma è univoca la sua definizione all'interno del modello
            - Modello: contiene elementi di informazione circa il sistema sotto osservazione
            - Diagramma: particolare visualizzazione di alcuni tipi di elementi di un modello
    - Struttura
        - Costituenti fondamentali: gli elementi di base
            - Entità: elementi di modellazione

                ![alt text](./res/entita.png "Entità")

            - Relazioni: legano tra loro le entità

                ![alt text](./res/relazioni.png "Relazioni")

            - Diagrammi: viste sul modello UML (un modello deve almeno avere un diagramma dei casi d'uso e uno delle classi/oggetti)
                - Statici
                    - Diagramma delle classi: descrive la struttura dati degli oggetti del  sistema e le loro relazioni; è il diagramma  più importante, da cui si può generare il codice
                        - Notazione
                            - Attributi: `visibilità nome molteplicità : tipo = valoreDefault`
                                - Visibilità
                                    - pubblica +
                                    - privata -
                                    - protetta #
                                    - package ~
                                - Molteplicità: es: String [5], Real [2..*], Boolean [0..1]
                                - Tipo
                                    - Integer, UnlimitedNatural, Real
                                    - Boolean
                                    - String
                                - Ambito
                                    - istanza
                                    - $`\underline{classe}`$
                            - Operazioni: `visibilità nome (parametro, ...): tipoRestituito`
                                - Parametri: `direzione nomeParametro: tipoParametro=valoreDefault`
                                    - Direzione
                                        - in
                                        - out
                                        - inout
                                        - return (si usa quando l’operazione restituisce più valori)
                        - Astrazione
                            
                            ![alt text](./res/astraz.png "Livelli astrazione")

                            - Nello schema delle classi centrale uso il livello più dettagliato, invece negli schemi di contorno posso usare livelli meno dettagliati
                        - Relazioni tra classi
                            - Associazione: connessione fra classi tipicamente bidirezionale, possiede sempre un nome
                                - Molteplicità: (si mettono al contrario rispetto all'ER)
                                    - Esattamente 1: 1
                                    - Opzionale 1: 0..1
                                    - Da x a y inclusi: x..y
                                    - Solo i valori a,b,c: a,b,c
                                    - 1 o più: 1..*
                                    - 0 o più: *

                                    ![alt text](./res/es-molte.png "Esempio di molteplicità")

                                - Verso (principalmente estetico, non starci a perdere troppo la testa)

                                    ![alt text](./res/vero-verso.png "Esempio di verso")

                                - Monodirezionalità (non viene particolarmente applicata nell'analisi, più che altro nella progettazione)

                                    ![alt text](./res/verso.png "Esempio di monodirezionalità")

                                - Ruoli: possono definire attributi (in esempio: datore, impiegato, dirigente, sottoposto, moglie, marito)

                                    ![alt text](./res/ruoli.png "Esempio di ruoli")

                                - Vincoli

                                    ![alt text](./res/vincoli.png "Esempi di vincoli")

                                    1. Un conto può essere intestato ad un'azienda o ad un privato
                                    2. Il datore di lavoro di una persona deve essere equivalente al datore di lavoro del capo della persona
                                        - Ecco a cosa servono i post-it; ricordati di collegarli alle associazioni interessate e puoi evitare di usare la dotted notation, usa linguaggio naturale
                                - Classi associative: un'associazione diventa una classe
                                    - Un'associazione senza classe associativa può avere duplicati, altrimenti no
                                    
                                    ![alt text](./res/class-as.png "Esempio di classe associativa")

                                    ![alt text](./res/classi-assoc.png "Note classe associativa")

                                - Associazioni qualificate (rare): riducono un’associazione molti-a-molti a una del tipo uno-a-uno, specificando un attributo che permette di selezionare un unico oggetto destinazione svolgendo il ruolo di identificatore o chiave di ricerca

                                    ![alt text](./res/as-qual.png "Esempio di associazioni qualificate")
                                
                                - Associazioni n-arie

                                    ![alt text](./res/asnar.png "Esempio di associazioni n-arie")

                                    - Definire associazione n-aria con dipendenze funzionali
                                        1. Fai le cardinalità di "andata", cioè da un elemento agli altri due (distinti), che deve essere per forza da molti a molti
                                            - Nell'esempio
                                                - Un'aula può avere più corsi e più giorno/ora
                                                - Un corso può avere più aule e giorno/ora
                                                - Un giorno/ora può avere più aule e corsi
                                        2. Fai le cardinalità di "ritorno", cioè da due elementi (insieme) all'altro, che può determinare dei vincoli e scrivi le molteplicità
                                            - Nell'esempio
                                                - Data un'aula e un corso ci sono tanti giorno/ora
                                                - Dato un corso e un giorno/ora c'è una sola aula
                                                - Dato un giorno/ora e un'aula c'è solo un corso
                                    - Non possono esistere false ternarie (in sostanza tutti i rami, all'andata, devono avere cardinalità n)
                                - Elemento derivato: non aggiunge niente, chiarisce soltanto dei vincoli 

                                    ![alt text](./res/elder.png "Esempio di elemento derivato")

                                    - Warning: hai creato un ciclo; prova a togliere un'associazione alla volta e vedi se è veramente importante

                            - Aggregazione (part-of): sia il tutto che le parti esistono indipendentemente

                                ![alt text](./res/aggre.png "Esempio di aggregazione")

                            - Composizione (part-of): il tutto possiede le sue parti

                                ![alt text](./res/comp.png "Esempio di composizione")

                            - Generalizzazione (o specializzazione): tutti gli attributi, le operazioni e le relazioni della superclasse vengono ereditati dalle sottoclassi
                                - Supportata ereditarietà multipla
                                - Vincoli
                                    - Overlapping (sovrapposte)
                                    - Disjoint (esclusiva)
                                    - Complete (completa)
                                    - Incomplete (incompleta)

                                ![alt text](./res/general.png "Esempio di generalizzazione")

                                - Classi astratte: non possono essere istanziate da oggetti

                                    ![alt text](./res/astratt.png "Esempio classe astratta")

                                - Powetyping: (meta)classe le cui istanze sono classi che specializzano un’altra classe

                                    ![alt text](./res/powert.png "Esempio di powertyping")

                                    - "Guai a voi": Articolo si collega a tipo che si specializza in HiFi, Telefonia e PC

                            - Dipendenza (non le useremo negli esercizi): A dipende da B quando una variazione in B può comportare una variazione in A
                                - Comunemente
                                    - Warning: use in UML 1 si usava al posto di include

                                    ![alt text](./res/dipuse.png "Esempio di dipendenza")

                                - Un’operazione della classe A ha argomenti che appartengono al tipo di un’altra classe (B)

                                    ![alt text](./res/dippar.png "Esempio di dipendenza con parametri")

                                - Template (in analisi/compito non lo useremo, irl si): descrivere una classe in cui uno o più parametri formali non sono istanziati
                                    - Definisce  una  famiglia  di  classi  in  cui  ogni  classe  è specificata istanziando i parametri con i valori attuali
                                    - Un template non è utilizzabile direttamente
                                    - Bound element: classe che istanzia i parametri di un template

                                    ![alt text](./res/templ.png "Esempio di template")
                                    - In questo caso
                                        - T: tipo elemento
                                        - k: quantità elemento
                            - Raffinamento (è tipo un "mi spiego meglio", la freccia è al contrario)
                                - Tra un tipo astratto e una classe che lo realizza (realizzazione)
                                - Tra una classe di analisi e una di progetto
                                - Tra una implementazione semplice e una complessa della stessa cosa

                                ![alt text](./res/raffinamento.png "Esempio di raffinamento")

                                - In questo caso
                                    - Un ArrayStack realizza, implementa, un'interfaccia Stack
                                - Interfaccia: insieme di funzionalità pubbliche identificate da un nome
                                    - Specifica  le  operazioni  pubbliche separandone   le   specifiche dall’implementazione 

                                    ![alt text](./res/interfac.png "Esempio di interfaccia")

                                    - In questo caso (e in generale)
                                        - La notazione a pallino può essere usata solo se nel progetto la notazione estesa è gia stata usata
                                        - Sempre meglio usare un'interfaccia invece di una gerarchia di ereditarietà

                                            ![alt text](./res/intvsered.png "Interfaccia vs Ereditarietà")

                        - Analisi vs Progettazione
                            - Analisi
                                - Classi
                                    - Informazioni
                                        - Rappresentano un’astrazione nel dominio del problema
                                        - Escludono tutti i dettagli implementativi
                                        - Indicano gli attributi che saranno probabilmente inclusi nelle classi di progettazione
                                        - Le loro operazioni specificano i principali servizi offerti dalla classe
                                    - Identificazione
                                        - Corrispondono  a entità  fisiche  e  a concetti  del dominio applicativo 
                                        - Una  classe  è  associata  a  un  piccolo  e  ben  definito  insieme  di responsabilità (normalmente tra 3 e 5)
                                        - Non possono essere isolate
                                        - I  nomi  delle  classi  devono  riflettere  la  loro  natura  intrinseca  e non il ruolo giocato nelle associazioni
                                        - Evitare
                                            - Classi troppo piccole
                                            - Classi troppo grandi
                                            - Classi onnipotenti
                                            - Classi "implementative"
                                            - Gerarchie di specializzazione profonde (massimo 3 livelli)
                                - Associazioni
                                    - Identificazione
                                        - Le associazioni sono tipicamente indicate da verbi che esprimono collocazione fisica (contenuto in), azioni (gestisce), comunicazioni (parla a), proprietà (possiede), soddisfacimento di condizioni (sposato a)
                                        - Un’associazione deve descrivere una proprietà strutturale del dominio, non un evento transitorio
                                        - Molte associazioni ternarie possono essere scomposte in due associazioni binarie
                                        - Evidenziare le associazioni derivate, che cioè possono essere espresse in termini di altre associazioni
                                - Attributi
                                    - Identificazione
                                        - Gli attributi spesso corrispondono a nomi seguiti da possessivi (ad esempio, il colore della macchina)
                                        - Omettere o evidenziare gli attributi derivati (tipo età da data di nascita)
                                        - 
                            - Progettazione ([How to](https://gitlab.com/p2f/software-engineering/course/tree/master/note/association-project))
                                - Classi
                                    - Informazioni
                                        - Le loro specifiche sono complete per cui possono essere direttamente implementate
                                        - Nascono dal domino del problema per raffinamento delle classi di analisi
                                    - Identificazione
                                        - Ogni classe deve essere
                                            - Completa: fornire ai suoi clienti tutti i servizi che essi si aspettano
                                            - Sufficiente: i suoi metodi devono essere esclusivamente finalizzati allo scopo della classe
                                            - Essenziale: non mettere a disposizione più di un modo per effettuare la stessa operazione
                                            - Massimamente coesa: modellare un unico concetto astratto
                                            - Minimamente interdipendente: associata all’insieme minimo di classi che le consente di realizzare le proprie responsabilità
                                - Associazioni
                                    - Identificazione
                                        - Non esistono associazioni bidirezionali e classi associative
                                        - La trasformazione analisi -> progettazione si basa sul carico di lavoro cui ciascuna associazione è sottoposta (qualitativamente)
                                        - Devono specificare
                                            - Nome
                                            - Verso di navigabilità
                                            - Molteplicità a entrambi gli estremi
                                            - Nome del ruolo destinazione
                                        - Regole
                                            - Il rombo bianco si usa quando ho un riferimento, quello nero quando ho "un oggetto dentro l'altro"
                                        - Esempi esplicativi
                                            
                                            ![alt text](./res/esasprog.png "Esempio di associazione in progettazione")

                                            - In persona ho un attributo "datore" che si riferisce all'azienda, quindi data una persona so in che azienda lavora ma data un'azienda devo scorrere tutte le persone per trovare i dipendenti dell'azienda in questione
                                            - `<<trace>>` si usa per riferirsi all'analisi (negli esercizi non lo usiamo)
                                            
                                            ![alt text](./res/esasprog1.png "Esempio di associazione in progettazione")

                                            - In azienda ho un attributo "impiegati" che si riferisce a tutte le persone che lavorano nell'azienda
                                            - Quella fra parentesi non la useremo mai, è solo una specificazione

                                            ![alt text](./res/esasprog2.png "Esempio di associazione in progettazione")

                                            - In caso di associazioni bidirezionali metto due frecce; possono essere ridondanti e infatti si tende ad analizzarne prima l'utilità (quantità di operazioni) e in base a questa si decide se adottarle o "fare il giro più lungo" passando dalle altre associazioni
                                            - Il problema dell'inconsistenza è risolto dall'incapsulamento 

                                            ![alt text](./res/esasprog3.png "Esempio di associazione in progettazione")
                                            
                                            - Ho utilizzato il rombo nero perchè la tessera è in associazione 1 a 1 con iscritto, quindi la metto "dentro" ad iscritto

                                            ![alt text](./res/esasprog4.png "Esempio di associazione in progettazione")
                                            
                                            - Potrei anche accorparle in una sola classe

                                            ![alt text](./res/esasprog5.png "Esempio di associazione in progettazione")

                                            - Le ternarie (e tutte le n-arie) impongono l'obbligo di reificare; i puntatori vanno da una classe a quella reificata, e da quella reificata alle altre (SEMPRE)
                                            - Se ho necessità di accesso particolari posso applicare la ridondanza che può essere parziale (come in figura) o totale (i puntatori collegano tutto da tutti i versi)
                                            - Tutte le molteplicità "a fianco" della classe reificata sono "a molti", quelle "a fianco" delle classi collegate invece sono "a uno"
                                            - Le dipendenze funzionali non sono esplicitate (le vedo dal diagramma di analisi o tramite un post-it)

                                            ![alt text](./res/esasprog6.png "Esempio di associazione in progettazione")

                                            - In caso di classe associativa questa si trasforma in una vera e propria classe ed è sempre in mezzo alle altre due
                                            - Nel primo esempio dato un Fornitore vedo tutto, nella seconda data una Parte vedo Parte e Fornitura, ma non Fornitore
                                            - NB: in questo caso per esempio non posso fare un'associazione che va da Fornitore a Parte
                    - Diagramma degli oggetti: mostra un insieme di oggetti di interesse e le loro relazioni (principalmente serve a  mostrare  esempi  di  strutture dati)
                        - Rappresentazione
                            - Un oggetto rappresenta una particolare istanza di una classe (`: Poligono`, in questo caso, si chiama prototipo, si usa per prendere un oggetto della classe poligono generico)

                                ![alt text](./res/ogg.png "Esempio di oggetto in UML")

                            - Un oggetto composto è un oggetto di alto livello che contiene altri oggetti

                                ![alt text](./res/oggcom.png "Esempio di oggetto composto in UML")

                        - Esempio
                            - Diagramma delle classi
                                
                                ![alt text](./res/diagclassiog.png "Esempio di diagramma delle classi")

                            - Relativo diagramma degli oggetti

                                ![alt text](./res/diagoges.png "Esempio di diagramma degli oggetti")

                    - Diagramma dei package: mostra i package e le loro relazioni di dipendenza, contenimento e specializzazione
                        - Package: raggruppamento di elementi del modello semanticamente correlati
                        - Relazioni
                            - Normali

                                ![alt text](./res/packdiag1.png "Esempio di relazione fra package")

                            - Di contenimento
                                - Mostrare massimo due livelli
                                -  I package annidati vedono lo spazio dei nomi dei package che li contengono

                                ![alt text](./res/packdiag2.png "Esempio di relazione di contenimento fra package")

                        - Dipendenze
                            - «use» (default): quando un elemento del package cliente usa in qualche modo un elemento del package fornitore
                            - «import»: quando gli elementi pubblici dello spazio dei nomi del package fornitore vengono aggiunti come elementi pubblici allo spazio dei nomi del package cliente
                            - «access»: quando gli elementi privati dello spazio dei nomi del package fornitore vengono aggiunti come elementi privati allo spazio dei nomi del package cliente
                            - «trace»: rappresenta l’evoluzione di un elemento in un altro elemento più dettagliato

                            ![alt text](./res/diagpackk.png "Esempio di dipendenze fra package")

                        - Generalizzazione: quando il package specifico si deve conformare all’interfaccia del package generale

                            ![alt text](./res/esgenpack.png "Esempio di generalizzazione di package")

                        - Individuare i package
                            - Fonte 1: diagramma delle classi
                            - I migliori candidati per essere raggruppati nello stesso package sono
                                - Le classi appartenenti a gerarchie di composizione
                                - Le classi appartenenti a gerarchie di specializzazione
                            - Fonte 2: diagramma dei casi d’uso
                                - Uno o più casi d’uso che supportano un processo aziendale o un attore potrebbero indicare un package
                            - Si possono poi spostare classi tra package, aggiungere package, eliminare package
                            - Numero ideale di classi per package: tra 4 e 10
                            - Evitare la dipendenze circolari
                    - Diagramma dei componenti: descrive l’architettura  software del sistema 
                    - Diagramma di deployment: descrive la struttura del sistema  hardware e l’allocazione  dei vari moduli software
                - Dinamici
                    - Diagramma dei casi d’uso: elenca i casi d’uso del  sistema e le loro relazioni 
                        - In dettaglio
                            - Rappresentano i ruoli di utilizzo del sistema da parte di uno o più utilizzatori (attori = esseri umani, enti..)
                            - Descrivono l’interazione tra attori e sistema
                            - Sono comprensibili a tutti
                            - Vanno definiti dal punto di vista dell'utente
                        - Attore: identifica il ruolo che un’entità esterna assume quando interagisce direttamente con il sistema
                            - Esterno al sistema
                            - Scambia informazioni con il sistema
                            - Esegue i casi d'uso
                            - E' modellato con una classe
                        - Caso d'uso: è la specifica di una sequenza di azioni
                            - L'attore la percepisce come una funzionalità
                            - Produce un risultato utile all'attore
                            - Viene attivato da un attore
                            - E' completo
                        - Elementi
                            
                            ![alt text](./res/info-casi-uso.png "Elementi casi d'uso")
                        
                        - Ruolo 
                            - Nelle fasi iniziali della progettazione servono per chiarire cosa dovrà fare il sistema (sono sempre il primo diagramma che si disegna)
                                - Ragionare sui casi d'uso con il committente permette di analizzare i requisiti ai quali il sistema dovrà fornire un'implementazione
                                - Raggiungere un accordo con il committente 
                            - I casi d'uso guidano l'intero progetto di sviluppo
                                - Sono il punto di partenza per la progettazione del sistema
                                - Sono il riferimento per i test di verifica di quanto prodotto 
                        - Identificare casi d'uso
                            1. Identificare tutte le tipologie di utilizzatori del sistema (attori)
                            2. Per ogni tipologia di attore rilevare in quale modo utilizzerà il sistema; a ogni modalità di utilizzo corrisponde un caso d'uso
                            3. (CORE) Per ogni caso d'uso descrivere lo scenario base (la sequenza di passi che conduce al successo del caso d'uso) e le principali varianti a tale scenario, così da far emergere eventuali attori/casi d'uso non individuati
                        - Scenari: ogni specifica esecuzione (istanza) di un caso d'uso
                            - Scenario base: scenario più semplice possibile che porta al successo del caso d'uso
                            - Varianti: possono portare al successo o al fallimento del caso d'uso       
                        - Specifiche

                            ![alt text](./res/spec-cas-us.png "Specifiche caso d'uso")

                        - Esempio

                            ![alt text](./res/casi-uso-bank.png "Esempio casi d'uso") 

                    - Diagramma degli stati: usa la notazione degli automi  di Harel per descrivere gli  stati degli oggetti di una classe
                        - Ogni classe può avere associato un diagramma di stato
                        - Stato: lo stato di un oggetto in un certo istante è un'astrazione dell'insieme dei valori dei suoi attributi e dei suoi collegamenti 
                            - Un evento provoca la transizione tra uno stato e l’altro; un oggetto rimane in uno stato per un tempo finito non istantaneo corrispondente all'intervallo tra due eventi
                            - Uno stato può contenere
                                - Azioni: operazioni istantanee, atomiche e non interrompibili associate a transizioni attivate da eventi
                                    - Se tante azioni uguali entrano in uno stato posso scrivere direttamente nello stato: `entry/azione`; per l'uscita `exit/azione`
                                - Attività: operazioni che richiedono un certo tempo per essere completate e possono quindi essere interrotte da un evento

                                ![alt text](./res/esstato.png "Esempio di uno stato")

                            - Stato composito: contiene  altri  stati annidati, organizzati in uno o più automi; ogni  stato  annidato  eredita  tutte  le  transizioni  dello  stato  che lo contiene e lo  pseudo-stato  finale  di  un  automa  viene  applicato  solo  a quell’automa

                                ![alt text](./res/esstatcomp.png "Esempio di stato composito")

                                - In questo caso, per esempio, se io sono in `Inizio` e mi triggerano `riaggancia` esco direttamente dallo stato composto senza eseguire `NumeroParziale` 

                                ![alt text](./res/esstatcomp1.png "Wireshark flow diagram")

                                - In questo caso, per esempio, il successo avviene se tutti i sottostati concludono il loro percorso e il fallimento invece solo se `ProvaFinale` fallisce

                        - Transizione (freccia): marca il passaggio di un oggetto da uno stato a un altro, ed è associata a uno o più eventi e, opzionalmente, a condizioni e azioni
                            - Un evento avviene a un preciso istante di tempo, e si assume che abbia durata nulla
                            - Una condizione è un’espressione booleana che deve risultare vera affinché la transizione possa avvenire
                            - Un’azione è un’operazione istantanea, atomica e non interrompibile che viene eseguita all’atto della transizione

                                ![alt text](./res/estrans.png "Esempio di transizione")

                                - Nel caso di un autoanello le "scritte" dell'immagine soprastante sono incorporate direttamente nello stato

                            - Una transizione che esce da uno stato e non riporta alcun evento indica che la transizione avviene al termine dell'attività

                                ![alt text](./res/estransv.png "Esempio di transizione senza evento")

                        - IF

                            ![alt text](./res/statoif.png "Esempio di blocco condizionale in diagramma a stati")

                        - Eventi
                            - Evento di variazione: si verifica nel momento in cui una condizione diventa vera; è denotato da un’espressione booleana (es. bilancio < 0)
                            - Evento di segnale (NON IMPORTANTE): si verifica nel momento in cui un oggetto riceve un oggetto segnale da un altro oggetto
                            - Evento di chiamata (NON IMPORTANTE): è l’invocazione di una specifica operazione nell’istanza del classificatore che fa da contesto al diagramma 
                            - Evento temporale: si verifica allo scadere di un periodo di tempo 
                                - `when(data=01/01/2008)`: specifica il momento della transizione
                                - `after(10 seconds)`: specifica che la transizione deve avvenire dopo 10 secondi dall’entrata dell’automa nello stato attuale; è anche possibile specificare il momento in cui inizia a decorrere il periodo aggiungendo una frase del tipo `since...` 
                        - Esempio: la linea telefonica

                            ![alt text](./res/esdiagstati.png "Esempio di diagramma a stati")

                    - Diagramma di attività: descrive le sequenze  eventi-azioni-transizioni  di una funzione

                        ![alt text](./res/activdiag.png "Esempio di diagramma di attività")

                        - Può essere associato a qualunque elemento di modellazione, che ne diviene il contesto
                            - Caso d’uso
                            - Operazione
                            - Classe 
                            - Interfaccia
                            - Componente
                            - Collaborazione
                        - Elementi
                            - Nodi
                                - Azione: rappresentano compiti atomici
                                    - Di chiamata
                                        - Chiama un comportamento

                                            ![alt text](./res/chcom.png "Azione di chiamata ad un comportamento")

                                        - Chiama un'attività

                                            ![alt text](./res/chat.png "Azione di chiamata ad un'attività")

                                        - Chiama un'operazione

                                            ![alt text](./res/chop.png "Azione di chiamata ad un'operazione")

                                    - Di accettazione di un evento temporale
                                        - Produce un evento temporale ogni volta  che la condizione temporale diventa vera

                                            ![alt text](./res/acc1.png "Azione di accettazione di un evento temporale eachtime")

                                        - Diventa attivo solo quando si attiva l’arco

                                            ![alt text](./res/acc2.png "Azione di accettazione di un evento temporale onetime")

                                - Controllo: controllano il flusso

                                    ![alt text](./res/nodicontrollo.png "Lista delle tipologie dei nodi di controllo")

                                - Oggetto: indicano che sono disponibili istanze di una data classe in un punto specifico dell’attività con un determinato stato

                                    ![alt text](./res/esnodog.png "Esempio di utilizzo di nodi oggetto")

                            - Archi
                                - Flussi di controllo
                                - Flussi di oggetti
                            - Corsie: raggruppano insiemi di azioni correlate 
                                
                                ![alt text](./res/escors.png "Esempio di utilizzo di corsie")
                                
                    - Diagramma di interazione: mostra le interazioni tra gli  oggetti durante scenari di  funzionamento del sistema
                        - Tipi
                            - Diagramma di sequenza: enfatizza  la  sequenza  temporale  degli scambi di messaggi
                                - Dimensioni
                                    - Verticale: tempo
                                    - Orizzontale: linee di vita
                                - Attivazione: rappresenta sia la durata dell’azione nel tempo sia la relazione di controllo tra l’attivazione e i suoi chiamanti
                                - Si possono specificare nodi decisionali, iterazioni, attivazioni annidate
                                - È consigliato descrivere il flusso tramite un’insieme di note poste accanto agli elementi 
                                
                                ![alt text](./res/richiestaprestito.png "Esempio di diagramma di sequenza")

                                - Stati: quando un’istanza riceve un messaggio il suo stato può cambiare; lo stato delle istanze può essere mostrato sulla linea di vita (oppure in un altro diagramma)
                                - Vincolo: posto sulla linea di vita indica una condizione sulle istanze che deve essere vera da lì in avanti 

                                ![alt text](./res/vininstat.png "Esempio di vincolo e variazione di stato")

                                - Frammenti combinati: utili per gestire if, switch e loop
                                    - Op = optional (if, then senza else)
                                    - Alt = alternatives (if, then, else)
                                    - Switch = switch

                                    ![alt text](./res/framcomb.png "Esempio di frammenti combinati")

                            - Diagramma di comunicazione: enfatizza le relazioni strutturali tra gli oggetti che interagiscono
                            - Diagramma di sintesi dell’interazione: illustra come un comportamento complesso viene realizzato da un insieme di interazioni più semplici
                            - Diagramma di temporizzazione: enfatizza gli aspetti real-time di un’interazione
                        - Terminologia
                            - Interazione: unità di comportamento di un classificatore che ne costituisce il contesto; comprende un insieme di messaggi scambiati tra linee di vita all’interno del contesto per ottenere un obiettivo
                            - Contesto: può essere dato dall’intero sistema, da un sottosistema, da un caso d’uso, da un’operazione, da una classe
                            - Linea di vita: rappresenta come un’istanza di un classificatore partecipa all’interazione

                                ![alt text](./res/linvita.png "Esempio di linee di vita")

                            - Messaggio: rappresenta un tipo specifico di comunicazione istantanea tra due linee di vita in un’interazione, e trasporta informazione nella prospettiva che seguirà una attività
                                - Di chiamata: per ogni messaggio di chiamata ricevuto da una linea di vita, deve esistere un’operazione corrispondente nel classificatore di quella linea di vita
                                    - Sincrono: il mittente aspetta che il destinatario ritorni

                                        ![alt text](./res/msgsin.png "Esempio di messaggio sincrono")
                                        
                                    - Asincrono: il mittente continua l’esecuzione

                                        ![alt text](./res/msgasin.png "Esempio di messaggio asincrono")

                                - Di creazione: si crea un’istanza del classificatore destinatario

                                    ![alt text](./res/msgcr.png "Esempio di messaggio di creazione")

                                - Di distruzione: il mittente distrugge il destinatario

                                    ![alt text](./res/msgdes.png "Esempio di messaggio di distruzione")

                                - Invio di segnali
                                    - Di ritorno: il destinatario restituisce il controllo al mittente 

                                        ![alt text](./res/msgret.png "Esempio di messaggio di ritorno")

        - Meccanismi comuni: tecniche comuni per raggiungere specifici obiettivi
            - Specifiche: descrizione testuale della semantica di un elemento

                ![alt text](./res/specifiche-esempio.png "Specifiche esempio")

            - Ornamenti: rendono visibili gli aspetti particolari della specifica dell’elemento

                ![alt text](./res/ornamenti-esempio.png "Ornamenti esempio")

            - Distinzioni comuni 
                - Classificatore/istanza

                    ![alt text](./res/clas-ist.png "Classificatore/istanza")
                
                - Interfaccia/implementazione

                    ![alt text](./res/int-imp.png "Interfaccia/implementazione")

            - Meccanismi di estendibilità
                - Stereotipo: variazione di un elemento di modellazione esistente, con lo stesso scopo ma forma diversa

                    ![alt text](./res/stereotipo.png "Stereotipo")
                
                - Proprietà: valore associato a un elemento del modello
                    - Esempio: `{ author = “Joe Smith”, status = analysis }  { abstract }`
                - Vincolo: frase di testo che definisce una condizione o una regola che riguarda un elemento del modello 
                    - Esempio: `{ disjoint, complete }  { subset }`
                - Profilo: insieme di stereotipi, proprietà e vincoli (personalizzazione)

### Parte 4 (discorsiva, non è necessario sapere proprio tutto)

- Ingegneria del software: tratta  la  realizzazione  di  sistemi  software  di dimensioni e complessità talmente elevate da richiedere uno o più team di persone per la loro costruzione
    - Definizioni
        - Approccio sistematico allo sviluppo, all’operatività, alla manutenzione e al ritiro del software. 
        - Disciplina tecnologica e manageriale che riguarda la produzione sistematica e la manutenzione dei prodotti software che vengono sviluppati e modificati entro i tempi e i costi preventivati. 
        - Corpus di teorie, metodi e strumenti, sia di tipo tecnologico che organizzativo, che consentono di produrre applicazioni con le desiderate caratteristiche di qualità.
    - Qualità del software
        - Classi
            - Interne: riguardano  le  caratteristiche  legate  allo  sviluppo  del  software;  non  sono visibili agli utenti
            - Esterne:  riguardano  le  funzionalità  fornite  dal  prodotto;  sono  visibili  agli utenti 
            - Relative  al  prodotto:  riguardano  le  caratteristiche  stesse  del  software
            - Relative  al  processo:  riguardano  i  metodi  utilizzati  durante  lo  sviluppo  del software
        - Lista di qualità
            - Correttezza: un software è corretto se rispetta le specifiche di progetto 
            - Affidabilità: un software è affidabile se l’utente può dipendere da esso 
            - Robustezza:  un  software  è  robusto  se  si  comporta  in  modo  ragionevole  anche  in circostanze  non  previste  dalle  specifiche  di  progetto 
            - Efficienza: un software è efficiente se usa intelligentemente le risorse di calcolo 
            - Facilità  d’uso:  un  software  è  facile  da  usare  se  l’interfaccia  che  presenta  all’utente  gli permette di esprimersi in modo naturale
            - Verificabilità: un software è verificabile se le sue caratteristiche (correttezza, performance, ecc.) sono facilmente valutabili 
            - Riusabilità:  un  software  è  riusabile  se  può  essere  usato,  in  tutto  o  in  parte,  per  costruire nuovi sistemi
            - Portabilità: un software è portabile se può funzionare su più piattaforme
            - Facilità di manutenzione: un software è facile da manutenere non solo se è strutturato in modo tale da facilitare la ricerca degli errori (modifiche correttive) ma anche se la sua struttura permette di aggiungere nuove funzionalità al sistema (modifiche perfettive) o di adattarlo ai cambiamenti del dominio applicativo (modifiche adattative)
            - Interoperabilità: fa riferimento all’abilità di un sistema di coesistere e cooperare con altri sistemi
            - Produttività: misura l’efficienza del processo di produzione del software in termini di velocità di consegna del software
            - Tempestività: misura la capacità del processo di produzione del software di valutare e rispettare i tempi di consegna del prodotto
            - Trasparenza: un processo di produzione del software si dice trasparente se permette di capire il suo stato attuale e tutti i suoi passi 
    - Principi di progettazione
        - Software design: processo che trasforma, attraverso numerosi passi intermedi, le specifiche dell’utente in un insieme di specifiche direttamente utilizzabili dai programmatori
            - Risultato: architettura del software, ossia l’insieme dei moduli che compongono il sistema, la descrizione della loro funzione, e delle relazioni esistenti tra di essi
        - Lista dei principi
            - Formalità: l’utilizzo di formalismi e di metodologie standardizzate nelle fasi di progettazione, implementazione e documentazione del sistema permette di ridurre fortemente gli errori di progetto
            - Anticipazione dei cambiamenti: la progettazione di un sistema informatico non deve mirare a soddisfare solo le specifiche attuali ma deve prevedere anche quelle future, poiché la capacità di prevedere i cambiamenti a cui il software sarà sottoposto durante il suo ciclo di vita determina la sua semplicità di manutenzione e la sua riusabilità
                - Tipi
                    - Noti a priori: ogni software segue un cammino evolutivo rispetto alla sua prima release. Anche i servizi che non verranno inizialmente implementati devono comunque essere presi in considerazione durante la fase progettuale
                    - NON noti a priori: al fine di poter affrontare anche modifiche non prevedibili durante la fase di design, la progettazione deve cercare di rendere il progetto facilmente modificabile 
            - Separazione degli argomenti (divide et impera): individuare i diversi aspetti di un problema complesso e trattarli separatamente al fine di semplificare la soluzione
                - Suddivisione in base a
                    - Tempo 
                    - Livello di qualità: dapprima si progetta il software in modo corretto quindi lo si ristruttura parzialmente al fine di aumentarne l’efficienza
                    - Vista: nella fase di analisi dei requisiti può essere conveniente analizzare distintamente i flussi di dati tra le diverse attività e il flusso di controllo che le governa
                    - Livello di astrazione: le specifiche vengono progressivamente raffinate
                    - Dimensione: modularizzazione
            - Modularità: divisione in sottoparti di un sistema
                - Caratteristiche fondamentali
                    - Tutti i servizi strettamente connessi devono appartenere allo stesso modulo
                    - Ogni modulo deve essere realizzato in modo indipendente da ogni altro 
                    - I programmatori devono essere in grado di operare su un modulo avendo una conoscenza minima del contenuto degli altri
                - Interfaccia
                    - Information hiding: l’interfaccia deve cioè contenere tutte le informazioni necessarie ad un corretto utilizzo del modulo evitando di mostrarne i dettagli implementativi; questo principio permette ai progettisti di modificare l’implementazione del modulo senza che ciò incida sulle altre componenti del sistema
                    - Funzionalità a disposizione: deve essere ben chiaro quali servizi sono realizzati dal modulo
                    - Modalità di fruizione di un servizio: per ogni servizio è necessario indicare la sequenza di routine da chiamare
                    - Definizione dei parametri di input: il tipo, il numero e la semantica dei parametri di input devono essere specificati in modo chiaro 
                    - Descrizione dell’output: semantica e tipologia dei valori restituiti dalle routine devono essere completamente specificati; in particolare, per ogni routine deve essere presente una tabella dei codici di errore, oltre al tipo dell’errore verificatosi e i motivi che lo hanno provocato
                - Tracking delle interazioni fra moduli (mediante grafi)
                    - Di utilizzo (USES)
                    - Di composizione (IS PART OF)
                    - Temporale: descrive la sequenza con cui devono essere realizzati i diversi moduli
            - Astrazione: consente di identificare gli aspetti fondamentali di un fenomeno e ignorare i suoi dettagli
            - Generalità: ogni volta che si deve risolvere un problema, si cerca di capire qual è il problema più generale che gli si nasconde dietro il quale può essere
                - Più semplice di quello specifico
                - La sua soluzione può essere più riusabile
                - Può essere già risolto in un’applicazione commerciale
    - Misurazione: prevedere o stimare tempi di consegna, costo di lavorazione, qualità del prodotto; si misura per prendere decisioni ed agire
        - Scopi
            - Previsione delle caratteristiche che avrà il software in una fase del ciclo di vita diversa da quella in cui si effettua la valutazione
            - Stima delle caratteristiche possedute dal software, nella fase e nello stadio di sviluppo in cui si effettua la valutazione
        - Presenza del processo in fasi  
            - di progettazione: per prevedere la manutenibilità e prevenire problemi nel software rilasciato in esercizio
            - di collaudo e/o test: per confrontare quanto fornito con le specifiche date
            - dopo il rilascio in esercizio: per misurare l'impatto del prodotto sulla efficienza ed efficacia del lavoro svolto, individuare aree di possibile miglioramento, decidere il momento del ritiro dalla produzione
        - Ambiti
            - Stima dei costi
                - Fonti
                    - Personale tecnico 
                    - Personale di supporto 
                    - Risorse informatiche
                    - Materiali di consumo
                    - Struttura
                - Fattori
                    - Numero di istruzioni da codificare 
                    - Capacità, motivazione e coordinamento degli addetti allo sviluppo 
                    - Complessità del programma 
                    - Stabilità dei requisiti 
                    - Caratteristiche dell’ambiente di sviluppo
            - Dimensione del software
                - Metriche dimensionali: si basano sulle LOC (Lines of code)
                    - Legenda
                        - $`M`$ = mesi uomo totali
                        - $`E`$ = numero totale di errori
                        - $`T`$ = costo totale
                        - $`PD`$ = pagine documentazione
                    - Indici di qualità
                        - Produttività: $`P = LOC/M`$ 
                        - Qualità: $`Q = E/LOC`$ 
                        - Costo unitario: $`C =  T/LOC`$ 
                        - Documentazione: $`D = PD/LOC`$
                - Metriche funzionali
                    - Metodo Function Points: misura la dimensione di un software in termini delle funzionalità offerte all'utente restituendo un parametro adimensionale
                        - Utilità
                            - Aiutare gli utenti a determinare il beneficio per le loro organizzazioni
                            - Misurare un prodotto, a sostegno di analisi sulla qualità e sulla produttività
                            - Stimare costi e risorse necessarie per lo sviluppo e la manutenzione del software
                            - Effettuare confronti sul software 
                        - Conteggio dei function point
                            - Funzioni
                                - Tipo dati
                                    - File interni logici (ILF): gruppo di dati o informazioni di controllo logicamente collegate e riconoscibili dall'utente che sono mantenute all'interno dei confini dell'applicazione; serve a contenere dati ottenuti attraverso uno o più processi elementari dell’applicazione
                                    - File esterni di interfaccia (EIF): gruppo di dati o informazioni di controllo logicamente collegate e riconoscibili dall'utente che sono referenziate dall’applicazione ma sono mantenute all’interno dei confini di un’altra applicazione; serve a contenere dati referenziati da uno o più processi elementari dell’applicazione, questo significa che un EIF contato per un’applicazione deve essere un ILF in un’altra applicazione 
                                - Tipo transazione
                                    - Input esterno (EI): processo elementare dell'applicazione che elabora dati o informazioni di controllo provenienti dall'esterno del confine dell'applicazione; serve a mantenere uno o più ILFs e/o modificare il comportamento del sistema
                                    - Output esterno (EO): processo elementare dell'applicazione che manda dati o informazioni di controllo all’esterno del confine dell’applicazione, serve a presentare informazioni all’utente; la logica di processo deve contenere almeno una formula matematica o calcolo, creare dati derivati, mantenere uno o più ILFs o modificare il comportamento del sistema
                                    - Interrogazioni esterne (EQ): processo elementare che manda dati o informazioni di controllo fuori dal confine dell’applicazione; serve a presentare informazioni all’utente attraverso il recupero di dati o informazioni di controllo da un ILF o EIF (la logica di processo non contiene formule matematiche o calcoli e non crea dati derivati)
                            - How to?
                                1. Identifico le funzioni
                                2. Assegno un peso, calcolato sulla base della quantità di dati e sulla complessità delle relazioni tra loro
                                3. La somma dei pesi di tutte le funzioni costituisce il Numero di Function Points Non Pesato
                                4. Questo numero è moltiplicato per un fattore di aggiustamento ottenuto considerando un insieme di 14 Caratteristiche Generali del Sistema
                            
                                ![alt text](./res/contfupo.png "Conteggio di function point")

                                - Tipi di conteggio
                                    - Per progetti di sviluppo: calcolo dei FP di un software da realizzare ex novo più eventuale conversione dei dati dalla vecchia applicazione
                                    - Per progetti di manutenzione evolutiva: misura le modifiche a un software esistente, comprendendo funzioni aggiunte, modificate, cancellate e di conversione
                                    - Per un'applicazione esistente
                                        - Calcolo dei FP iniziali
                                        - Aggiornamento dei FP dopo ogni manutenzione evolutiva: i punti delle funzioni cancellate sono sottratti
                                - Ambito del conteggio e confine delle applicazioni: significa identificare le funzionalità che devono essere considerate in un conteggio; il confine (determinato basandosi sul punto di vista dell'utente) è la linea di separazione tra le applicazioni che si stanno misurando e le applicazioni esterne o l’utente
                                - Fattore di aggiustamento: serve a tenere conto di quelle funzionalità generali del sistema non sufficientemente rappresentate dalle funzioni dati e transazionali; varia fra 0.65 e 1.35 e viene calcolato sulla base del grado di influenza di ciascuna delle 14 Caratteristiche Generali del Sistema
                                    - `fattore di aggiustamento = 0.65 + (TDI*0.01) ` dove `TDI` = somma dei gradi di influenza delle caratteristiche
                                    - Caratteristiche Generali del Sistema
                                        - Il grado di influenza di una caratteristica è compreso tra 0 (nessuna influenza) e 5 (forte influenza) 
                                        - Caratteristiche
                                            - Comunicazione dati 
                                            - Distribuzione dell’elaborazione
                                            - Prestazioni
                                            - Utilizzo estensivo della configurazione
                                            - Frequenza delle transazioni
                                            - Inserimento dati interattivo 
                                            - Efficienza per l’utente finale
                                            - Aggiornamento interattivo 
                                            - Complessità elaborativa
                                            - Riusabilità
                                            - Facilità di installazione
                                            - Facilità di gestione operativa
                                            - Molteplicità di siti
                                            - Facilità di modifica
                    - Numero ciclomatico
                        - Definizioni
                            - Generica: definizione operativa di complessità del flusso di controllo di un programma, è legato all’identificazione di tutti i cammini che permettono di raggiungere una copertura accettabile del programma
                            - Grafi: il numero ciclomatico di un grafo fortemente connesso è il numero minimo di archi che occorre eliminare per trasformarlo in un albero
                                - Formula: `numero ciclomatico = e-n+1`
                                    - `e` = archi
                                    - `n` = nodi
                                - Formula bis (se non ho l'arco che congiunge la fine all'inizio e quindi ho un "grafo modificato"): `numero ciclomatico = e-n+2`
                        - Teorema di Mills
                            - `v(G) = d + 1`
                                - `d` = numero dei punti di decisione del programma (assumendo che un punto di decisione a k uscite contribuisca come k-1 punti di decisione a 2 uscite)
                            - Se il programma ha procedure al suo interno, il numero ciclomatico dell'intero grafo è dato dalla somma dei numeri ciclomatici dei singoli grafi indipendenti: `v(G) = e - n + 2p`
                                - `p` = numero di grafi (procedure) indipendenti
                        - NB: la complessità ciclomatica di un modulo non dovrebbe superare il valore 10
                    - Constructive Cost Model (COCOMO): si  calcola  una  stima  iniziale  dei  costi  di  sviluppo  in  base  alla  dimensione  del software da produrre, poi la si migliora sulla base di un insieme di parametri
                        1. Stima della dimensione del software (numero di linee = KDSI)

                            ![alt text](./res/linee.png "Stima delle linee di un software")

                        2. Determinazione  della  classe  del  software: i  sw  sono  suddivisi  in  tre  categorie  con caratteristiche  di  difficoltà  crescente
                            - Categorie
                                - Organic
                                    - $`Costo (mesi/uomo) = 3.2 \times KDSI^{1.05}`$
                                - Semi-detached
                                    - $`Costo (mesi/uomo) = 3.0 \times KDSI^{1.12}`$
                                - Embedded
                                    - $`Costo (mesi/uomo) = 2.8 \times KDSI^{1.2}`$
                            - Tabella di scelta

                                ![alt text](./res/cocom.png "Tabella di scelta del cocomo")

                        3. Applicazione degli stimatori di costo
                            - Formula: $`Costo totale (M) = Costo (mesi/uomo) \times \prod_{i=1}^{15} c_{i}`$
                            - Tabella di $`c`$

                                ![alt text](./res/stimco.png "Tabella stimatori di costo")

    - Produzione: sequenza di operazioni che viene seguita per costruire, consegnare e modificare un prodotto
        - Tipi di modelli
            - Prescrittivi: definiscono un insieme distinto di attività, azioni, compiti, risultati e prodotti che sono necessari per ingegnerizzare un software di alta qualità 
                - Attività strutturali generiche
                    - Comunicazione (comprende la raccolta dei requisiti)
                    - Pianificazione
                    - Modellazione
                    - Costruzione (comprende il testing)
                    - Deployment
                - Tipi
                    - A cascata: suggerisce un approccio sistematico e sequenziale lineare, in cui l’output di ogni fase rappresenta l’input della successiva 
                        - E’ inadeguato quando i requisiti sono incerti o non noti durante le fasi iniziali del progetto
                        - Non permette di modificare i risultati delle fasi precedenti alla luce di errori riscontrati a posteriori
                        - Solo al termine del progetto si genera una versione funzionante dei programma

                        ![alt text](./res/waterfall.png "Modello a cascata")

                    - Incrementale: modello che combina aspetti del modello a cascata applicati a sottosistemi del prodotto finale, producendo il software a incrementi (ogni versione aggiunge nuove funzionalità o sottosistemi)
                        - Consiste nell’applicare più sequenze lineari, scalate nel tempo, ognuna delle quali produce uno stadio operativo del software
                            - Primo stadio: “prodotto base”, ossia un prodotto che soddisfa i requisiti fondamentali tralasciando alcune caratteristiche supplementari
                            - Preparazione secondo stadio: in seguito a una valutazione dell’utente, si stende un piano per lo stadio successivo, che preveda l’aggiunta di nuove funzionalità
                        - E’ adatto a progetti in cui i requisiti iniziali sono ben definiti ma la dimensione del sistema scoraggia l’adozione di un processo puramente lineare

                        ![alt text](./res/incre.png "Modello incrementale")

                        - Sottotipi
                            - RAD (Rapid Application Development): ogni applicazione modularizzabile in modo che ciascuna funzionalità principale possa essere completata in meno di 3 mesi è candidata al RAD; ogni funzionalità viene affrontata da un team distinto e poi integrata a formare un unico prodotto

                                ![alt text](./res/RAD.png "Modello RAD")

                    - Iterativo (da subito sono presenti le funzionalità/sottosistemi di base che vengono successivamente raffinate e migliorate)
                        - Evolutivo: si produce una versione limitata, sulla base di requisiti ben noti, e successivamente si realizzano delle estensioni
                            - Prototipazione
                                - Prototipo: versione approssimata, parziale (funzionante), dell’applicazione che deve essere sviluppata
                                - A cosa serve?
                                    - Un prototipo software permette di animare e dimostrare i requisiti
                                    - Equivoci fra gli utenti e gli sviluppatori sono messi in evidenza 
                                    - Possono essere evidenziate funzionalità mancanti o confuse  
                                
                                ![alt text](./res/prototipaz.png "Prototipazione")

                                - Tipi
                                    - Evolutiva: fornire un sistema funzionante all'utente finale (il prototipo viene fatto evolvere nel prodotto finale, senza gettarlo)

                                        ![alt text](./res/evo.png "Prototipazione evolutiva")

                                        - Problema: cambiamenti continui tendono a corrompere il sistema, per cui il mantenimento a lungo termine diviene costoso

                                    - Usa e getta: validare o derivare i requisiti del sistema

                                        ![alt text](./res/usagetta.png "Prototipazione usa e getta")

                                        - Usata soltanto per "testare" certe parti non completamente comprese del sistema le quali verranno poi integrate nel software ufficiale
                            - Sottotipi
                                - A spirale

                                    ![alt text](./res/modspi.png "Modello a spirale")

                                - Model-driven development: si creano modelli formali del software che vengono poi fatti evolvere mentre il sistema viene progettato e implementato; i modelli diventano la guida del processo di sviluppo, infatti, è previsto l’uso di strumenti per la generazione automatica del codice e dei test case a partire dai modelli 

            - Agili
                - Dettagli
                    - Incoraggiano la soddisfazione del cliente e una consegna incrementale anticipata del software
                    - Impiegano team di progettazione compatti e molto motivati 
                    - Impiegano metodi informali
                    - Producono un livello minimo di prodotti di ingegneria del software
                    - Incoraggiano semplicità di sviluppo
                    - Richiedono comunicazione continua tra sviluppatori e utenti 
                - Tipi
                    - Extreme programming (OO)
                        - Pianificazione
                            - Definisce un insieme di user storyche descrivono le funzionalità del software
                            - A ogni user story il cliente assegna un valore che ne definisce la priorità
                            - I progettisti assegnano a ogni user story un costo (in settimane di sviluppo)
                            - Se una user story richiede più di 3 settimane di sviluppo, si chiede al cliente di frammentarla 
                            - Il cliente e i progettisti decidono quali user story inserire nella prossima release, e le ordinano per valore o per rischio decrescenti 
                        - Design
                            - Massima semplicità
                            - Scoraggiata la progettazione di funzionalità aggiuntive 
                            - Se viene individuato un problema di design, si crea immediatamente un prototipo operativo che viene poi valutato 
                            - Incoraggia il refactoring (ripulitura)
                        - Programmazione
                            - Pair programming
                        - Testing
                            - Test driven developement
                            - Test di regressione (se faccio una funzione che peggiora l'andamento dei test regredisco)
                        - "Misurazione": dopo il primo rilascio del progetto il team calcola il numero di user story implementate; questo serve a
                            - Stimare le date di consegna e le pianificazioni per le successive release 
                            - Determinare se le user story sono state sottovalutate e prendere provvedimenti
            - Unified Process
                - Concetti base
                    - `CHI`: Una risorsa o ruolo definisce il comportamento e le responsabilità di un individuo o un gruppo 
                    - `COSA`: Il comportamento è espresso in termini di attività e manufatti
                    - `QUANDO`: Si modellano flussi di lavoro, ossia sequenze di attività correlate eseguite da ruoli che producono manufatti
                - Manufatti
                    - Set di gestione
                        - Elaborati di pianificazione (software development plan, studio economico, ...) 
                        - Elaborati operazionali (stato di avanzamento, descrizione versione, ...)
                    - Set dei requisiti
                        - Modello dei casi d'uso 
                        - Modello di business
                    - Set di progettazione
                        - Modello di design
                        - Modello architetturale 
                        - Modello di test
                    - Set di implementazione
                        - Codice sorgente ed eseguibili
                        - File di dati 
                    - Set di rilascio agli utenti
                        - Script di installazione
                        - Documentazione utente
                - Flussi di lavoro: sequenziali
                    - Requisiti: fissa ciò che il sistema deve fare 
                    - Analisi: mette a punto i requisiti e li struttura
                    - Progettazione: concretizza i requisiti in un'architettura del sistema
                    - Implementazione: costruisce il software 
                    - Test: verifica che l'implementazione rispetti i requisiti
                    - Deployment: descrive la configurazione del sistema
                    - Gestione configurazione: mantiene le versioni del sistema
                    - Gestione progetto: descrive le strategie per gestire un processo iterativo
                    - Ambiente: descrive le infrastrutture di sviluppo
                - Fasi: sequenziali, corrispondono a milestone significativi per committenti, utenti, management; ogni fase può essere composta da più iterazioni
                    - Inception (avvio): definisce gli obiettivi del progetto, ne investiga la fattibilità, ne stima i costi, il potenziale di mercato e i rischi, analizza i prodotti concorrenti
                        - Milestone: documenti di fattibilità
                    - Elaboration: pianifica il progetto e ne definisce le caratteristiche funzionali, strutturali e architetturali
                        - Milestone: specifica dei requisiti software e architettura consolidata
                    - Construction: sviluppa il prodotto attraverso una serie di iterazioni, effettua il testing, prepara la documentazione
                        - Milestone: beta del sistema
                    - Transition: consegna il sistema agli utenti finali (include marketing, installazione, configurazione, formazione, supporto, mantenimento) 
                        - Milestone: release del sistema

                ![alt text](./res/fasieflussi.png "Schema riassuntivo di fasi e flussi di lavoro")

    - Verifica del software:  controllare  se  il  sistema  realizzato risponde alle specifiche di progetto (viene iterata in varie fasi del progetto)
        - Tipi
            - Dinamiche (di testing): verificare il comportamento del sistema in un insieme di casi sufficientemente ampio da rendere plausibile che il suo comportamento sia analogo anche nelle restanti situazioni
                - Testing in the small: riguardano parti specifiche del sistema (white box)
                    - Criterio di copertura dei programmi (statement test): selezionare  un  insieme  di  test  T  tali  che,  a  seguito  dell’esecuzione  del  programma  P  su tutti i casi di T, ogni istruzione elementare di P venga eseguita almeno una volta; questo viene fatto poichè un errore non può essere scoperto se la parte di codice che lo contiene non viene eseguita almeno una volta
                    - Criterio di copertura delle decisioni (branch test): selezionare un insieme di test T tali che, a seguito dell’esecuzione del programma P su tutti i casi di T, ogni arco del grafo di controllo di P sia attraversato almeno una volta
                    - Criterio di copertura delle decisioni e delle condizioni: selezionare un insieme di test T tali che, a seguito dell’esecuzione del programma P su tutti i casi di T, ogni arco del grafo di controllo di P sia attraversato e tutti i possibili valori delle condizioni composte siano valutati almeno una volta (in pratica valuto tutti i valori che possono uscire fuori dall'if o entrarvi)
                - Testing in the large: riguardano il sistema in generale (black box)
                    - Test di modulo: verifica se un modulo è stato implementato correttamente in base al suo comportamento esterno 
                    - Test  d’integrazione:  verifica  il  comportamento  di sottoparti  del  sistema  sulla base  del  loro  comportamento  esterno
                    - Test di sistema: verifica il comportamento dell’intero sistema sulla base del suo comportamento esterno
            - Statiche (di analisi): ispezionare il codice del software  per   capirne   le caratteristiche e le funzionalità
                - Code walk-through: analisi eseguita  da  un  team  di  persone (3/5) che  dopo  aver selezionato opportune porzioni del codice e opportuni valori di input ne simulano su carta il comportamento
                - Code inspection: come la precedente solo che si limita a ricercare classi specifiche di errori
                    - Classi errori maggiormente cercate
                        - Uso di variabili non inizializzate
                        - Loop infiniti 
                        - Letture di dati non allocati 
                        - Deallocazioni improprie di memoria
                    - Tipi
                        - Analisi del flusso dati: valori assunti dalle variabili
                            - Legenda
                                - `a` = Annullamento
                                - `d` = Definizione
                                - `u` = Uso
                            - Esempio
                                ```
                                procedure swap (x1, x2: real)
                                var x: real;
                                begin
                                x2 := x;
                                x2 := x1;
                                x1 := x;
                                end;
                                ```
                                - `x`: `auu` --> Problematica!
                                    - Un annullamento (il valore associato alla variabile x non è infatti definito al momento dell'attivazione del sottoprogramma)
                                    - Un uso (`x2 := x;`) 
                                    - Un secondo uso (`x1 := x;`)
                                - `x1`: `dud`
                                - `x2`: `ddd`
                            - Campanelli d'allarme generali
                                - Ogni sequenza contenente un uso non preceduto da una definizione/annullamento e riassegnazione
                                    - Esempio eccezione: generatore di numeri casuali che legge il contenuto non inizializzato di una cella di memoria per determinare il seme della generazione
                                - Ogni sequenza contenente due definizioni consecutive
                                    - Esempio eccezione
                                        ```
                                        ......
                                        x := .....
                                        if .... then x := .....
                                        ... := ...x...
                                        .......
                                        ```
    - Certificazione: atto mediante il quale un organismo di certificazione accreditato a livello nazionale o internazionale dichiara che un determinato prodotto, processo, servizio o sistema  aziendale  è  conforme  a  una  specifica  norma  o  documento normativo a essa applicabile
        - Accreditamento: processo  di  riconoscimento  delle  competenze  degli organismi di certificazione
        - Classi
            - Regole tecniche: sono emesse dalla pubblica amministrazione e dagli organi dello Stato;  la  loro osservanza ha carattere obbligatorio
            - Norme   tecniche   consensuali: sono elaborate e pubblicate dagli organismi di normazione  riconosciuti; la  loro  applicazione  non  è  obbligatoria,  ma  può  essere  imposta  da  opportune  direttive, leggi o regolamenti 
                - ISO 9000
                    - Normativa
                        - Gestione  per  la  qualità:  offrono  una  guida  alle  aziende  che  desiderano  progettare  e attuare  un   sistema di  qualità  nella  loro  organizzazione  o  migliorare  il  sistema esistente
                        - Assicurazione della qualità: definiscono i requisiti generali a fronte dei quali un cliente valuta   l’adeguatezza   del   sistema  del   fornitore
                    - Processo di certificazione (può essere generalizzato a altri tipi di certificazione)

                        ![alt text](./res/iso9000.png "Processo di certificazione di ISO9000")

                        - Manuale  qualità
                            - Specifica  di  tutti  i  processi  su  cui  è  applicato  il  sistema  qualità
                            - Descrizione  di  tutta  la  documentazione  che  viene  redatta  a  supporto  di tale sistema
                        - Verifica ispettiva: vengono visitati i reparti in cui si svolgono le attività ed effettuate le verifiche relative alla corretta applicazione delle procedure aziendali
                        - Visite  di  sorveglianza: la  frequenza varia da 1 a 4 all’anno
                        - La certificazione può  essere  rilasciata  anche  su  sotto-porzioni  dell’azienda  oggetto  di valutazione
                        - Non conformità: scostamento o assenza di una o più caratteristiche di qualità o di elementi del sistema qualità rispetto ai requisiti specificati
                            - Rispetto ai requisiti della norma
                            - Sulla documentazione: attività  non  procedurate  o  formalizzate  in  documenti, oppure attività che non seguano le prescrizioni delle procedure
                            - Sull'attuazione delle procedure
                        - Documentazione

                            ![alt text](./res/docprog.png "Sottocategorie di documentazione")

    - Manutenzione: ciò che rende un sistema manutenibile è la sua architettura originale
        - Correttiva: rimedia ai malfunzionamenti provocati dai difetti derivanti da errori di analisi, progettazione, codifica, test
        - Adattiva: mantiene inalterato il livello di servizio del sistema al mutare delle condizioni operative
        - Perfettiva: migliora qualitativamente le caratteristiche funzionali o tecniche del sistema 
        - Evolutiva: migliora qualitativamente e quantitativamente le caratteristiche del sistema

### Parte 5 (discorsiva, non è necessario sapere proprio tutto)

- Interfaccia: permette il dialogo tra due entità
    - Scelta della tecnologia
        - In funzione degli obiettivi: rapidità o efficacia
        - In funzione degli utenti
            - Numero d’utenti
            - Esperienza nell’utilizzo della tecnologia 
            - Età media
            - Motivazione 
    - Tipi
        - Code-based: interazione attraverso comandi (CMD)
            - Ottimale per moli di lavoro elevate che richiedono attenzione in punti lontani dal video (es. Check-In in aeroporto)
        - 3270: interazione per "compilazione" (quelle che i supermercati usano per le fatture)
            - Ottimale per data-entry ed editing di dati altamente strutturati
        - Pseudo-gui: interfaccia grafica che richiama il tipo 3270
            - Ottimale per applicazioni che debbano gestire dati fortemente strutturati garantendo una buona flessibilità
        - Standard-GUI: ambiente grafico
            - Esaltate le potenzialità di manipolazione diretta
        - Special-GUI: enfasi massima alla presentazione grafica (tipo SONAR)
            - Obiettivo prioritario è l’autoesplicazione
    - Progettazione
        1. Prima del termine dello studio di fattibilità
            1. Definire le attività legate alla realizzazione dell’interfaccia 
            2. Definire i parametri di riferimento ed i criteri di usabilità 
            3. Pianificare le attività di valutazione dell’usabilità 
            4. Realizzare il modello concettuale dell’interfaccia
        2. Precocemente nella fase di analisi e progettazione
            1. Definire e realizzare le strutture base (dialogo, look & feel) 
            2. Stabilire gli standard di progetto per l’interfaccia 
            3. Prototipare le parti ritenute critiche 
            4. Verificare l’allineamento con modello concettuale e standard 
        3. Nella fase di sviluppo
            1. Ultimare l’interfaccia in dettaglio legandola alla logica applicativa
    - Struttura
        - Alta e stretta

            ![alt text](./res/as.png "Modello di GUI alto e stretto")

            - Multi-window: flessibilità alta ma navigazione complessa

                ![alt text](./res/mw.png "Multi-window")
                
        - Bassa e larga

            ![alt text](./res/bl.png "Modello di GUI basso e largo")

            - Multi-paned: adatta a special-GUI ma non flessibile

                ![alt text](./res/mp.png "Multi-paned")

        - "Una via di mezzo"
            - Multi-Document: perfetta per utenti inesperti, flessibilità minore rispetto alla Multi-window

                ![alt text](./res/mdi.png "Entità")

    - Project standard
        - Definizione degli standard per:
            - Terminologia
            - Metafore, icone
            - Caratteristiche delle finestre (menu, bottoni, dimensioni, posizione, ecc.)
        - Obiettivo: agevolare l’utilizzo da parte dell’utente
    - Test con l'utente
        - Tipi
            - Simulatore (l’utente è passivo) 
            - Dimostratore (l’utente agisce sulle parti critiche) 
            - Prototipo (l’utente agisce sull’intero sistema in beta-release) 
        - Da verificare
            - Il modello concettuale è sufficientemente rappresentato
            - Rispetto al progetto l’interfaccia è adatta e gli standard sono rispettati
            - Possibilità d’utilizzo alternativo tra mouse e tastiera
            - Livello d’integrazione dell’utente con l’interfaccia
            - E’ utilizzata la terminologia utente
    - Comunicazione visiva
        - Ergonomia: lo studio del costo delle operazioni mentali per un utilizzo adeguato dell’interfaccia
        - Affordance: enfatizza gli aspetti di un oggetto che invitano a manipolarlo in un certo modo

            ![alt text](./res/esaff.png "Esempio di affordance")

        - Metafora: una parola, una frase o una figura che dipinge un oggetto o un concetto attraverso una somiglianza o un’analogia con un altro oggetto o concetto del mondo reale
            - Colori: utili per focalizzare l’attenzione o per creare associazioni 
                - Non abusarne
                - Se il colore è usato come codice: solo 3-5 colori, ricordarsi la semantica
                - Colori vivaci per aree piccole (poichè affaticano la vista) e neutri per aree grandi
                - Ricercare un contrasto efficace tra testo e sfondo
            - Icone: disegni piccoli, semplici e metaforici
                - Dektop icon: per applicazioni collegate per l’utente, icone simili graficamente
                - Menu icon - Palette Icon: sempre visibili accanto ai menu
                - Button-icon: in aggiunta al testo di un bottone
        - Layout: è determinato dalla posizione del testo, dei disegni e dei controlli all’interno di un’area considerata
            - Le distanze devono essere scelte in relazione al grado di associazione tra gli elementi
        - Font: leggibilità in relazione al tipo e alle caratteristiche del carattere
            - Sans Serif (senza grazie) per singole righe
            - Serif (con grazie) per testi articolati su molte righe, aiuta a seguire il testo
            - Non solo maiuscolo
            - Spaziatura proporzionale
        - Usabilità: l’efficacia, efficienza e soddisfazione con cui determinati utenti eseguono determinati compiti in particolari ambienti

            ![alt text](./res/usab.png "Criteri di usabilità")