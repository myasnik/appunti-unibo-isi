### Parte 8 - SQL

- Database di riferimento

    ![alt text](./res/db-ref.png "Database di esempio.")

- **Caratteristiche generali**
    * Non specifica la sequenza di operazioni da compiere per ottenere il risultato.
    * Relazionalmente completo: ogni espressione dell'algebra relazionale può essere tradotta in SQL.
    * Utilizza tabelle invece che relazioni.
    * Adotta la logica a 3 valori (V, F, ?).
        + In generale quando si effettua una espressione su un valore nullo questa non viene ritornata.
        + Per verificare se un valore è NULL si usa l'operatore IS:
            - `...WHERE Stipendio IS NULL`
- **Funzionalità**
    * **Data definition language** (DDL)
        + Creare schemi di relazioni (tabelle), modificarli ed eliminarli.
            - **Creare tabella**
                
                ![alt text](./res/cr-tab.png "Esempio di creazione tabella SQL.")
            
            - **Eliminare tabella**
                * Istruzione: **DROP TABLE** [table-name]
            - **Modificare tabella**
                
                ![alt text](./res/add-tab.png "Esempio di aggiunta di valori ad una tabella preesistente.")

        + Specificare vincoli
            - **Vietare la presenza di NULL**
                * `Cognome varchar(60) NOT NULL`
            - **Specificare valore di default**
                * `Ruolo char(20) DEFAULT 'Programmatore'`
            - **Definire chiave**
                * **Singolo attributo**
                    + `CF char(16) UNIQUE`
                * **Multipli attributi**
                    + `UNIQUE(Cognome, Nome)`
                * **Chiave primaria**
                    + Non obbligatoria
                    + Massimo 1 chiave primaria per tabella
                    + Non è necessario specificare `NOT NULL`
                    + Istruzione: `CodImp char(4) PRIMARY KEY`
                * **Foreign key**
                    + `FOREIGN KEY (Sede) REFERENCES Sedi(Sede)`
            - **Esprimere vincoli arbitrari**
                * `Stipendio int CHECK(Stipendio > 0)`
                * Se check viene espresso a livello di tabella è possibile fare riferimento a più attributi della tabella stessa.
            - **Politiche di reazione**

                ![alt text](./res/pol-rea.png "Esempio di politiche di reazione.")

        + Definire nuovi domini
            - Tipi:
                * **Elementari**
                    + **Carattere**: singoli caratteri o stringhe, anche di lunghezza variabile
                    + **Bit**: singoli booleani o stringhe
                    + **Numerici**: esatti e approssimati
                    + **Data, ora, intervalli di tempo**
                * **Definiti dall'utente**
                    
                    ![alt text](./res/dom-ut.png "Esempio di dominio definito dall'utente.")

        + Definire viste e indici
    * **Data manipulation language** (DML)
        + **Istruzioni principali**
            - **SELECT**: esegue query.

                ![alt text](./res/select.png "Definizione di select.")

                * **SELECT list**: che cosa si vuole come risultato (NULL ignorati)
                    + `*`: selezionare tutto
                    + **DISTINCT**: elimina le righe duplicate
                    + **TOP**(numero|percentuale) **WITH TIES**: quante righe deve restituire (sintassi dipendente da DBMS) e se deve/non deve contare due volte le tuple uguali
                    + Può contenere anche espressioni: `SELECT CodImp, Stipendio*12`
                    + **Ridenominazione colonne**: `SELECT CodImp AS Codice, Stipendio*12 AS StipendioAnnuo`
                    + **Pseudonimi**:
                         ```
                         Esempio 1 (quello con stipendio più basso):
                            SELECT Impiegati.CodImp AS Codice,
                                   Impiegati.Stipendio*12 AS StipendioAnnuo
                            FROM   Impiegati
                            WHERE  Impiegati.Sede= 'S01‘
                         Esempio 2 (quelli con stipendio più basso):
                            SELECT I.CodImp AS Codice,
                                   I.Stipendio*12 AS StipendioAnnuo
                            FROM   Impiegati I  -- oppure Impiegati AS I 
                            WHERE  I.Sede= 'S01'
                         ```
                * **FROM**: da dove si prende
                * **WHERE**: quali condizioni deve soddisfare
                    + **AND, OR, NOT**
                    + **BETWEEN**: `...WHERE Stipendio BETWEEN 1300 AND 2000`
                        - Alternativa: `...WHERE Stipendio >= 1300 AND Stipendio <= 2000`
                    + **IN**: condizioni di appartenenza ad un insieme (`...WHERE Sede IN ('S02','S03')`)
                        - Alternative: 
                            * **ANY**: `...WHERE Sede = ANY ('S02','S03')`
                            * **OR**: `...WHERE Sede = 'S02'OR Sede = 'S03'`
                    + **LIKE**: pattern su stringhe tramite wildard
                        - Wildcard: 
                            * `_`: carattere
                            * `%`: stringa
                        - Esempio: `...WHERE Nome LIKE '_i%i'`
                * **GROUP BY**: colonne su cui raggruppare
                * **HAVING**: condizioni relative ai gruppi
                * **ORDER BY**: ordinamento
                    + Sintassi: `...ORDER BY <colonna/attributo> DESC/ASC`
                    + Con TOP: 
                        ```
                        Esempio 1:
                            SELECT TOP(1) Nome, Stipendio
                            FROM Impiegati
                            WHERE Ruolo = 'Programmatore'
                            ORDER BY Stipendio
                        Esempio 2:
                            SELECT TOP(1) WITH TIES Nome, Stipendio
                            FROM Impiegati
                            WHERE Ruolo = 'Programmatore'
                            ORDER BY Stipendio
                        ```

            - Possono fare uso di condizioni per specificare le tuple da modificare.
                * **INSERT**: inserisce nuove tuple.
                    + Può usare il risultato di una query per eseguire inserimenti multipli.
                    + Caso singolo
                        ```
                            INSERT INTO Sedi(Sede, Responsabile, Città)
                            VALUES      ('S04', 'Bruni', 'Firenze')
                        ```
                        - Se la lista non include tutti gli attributi, i restanti assumono valore NULL (se ammesso) o il valore di default (se specificato).
                    + Caso multiplo (usare risultati query)
                        ```
                            INSERT INTO     SediBologna(SedeBO,Resp)
                            SELECT          Sede, Responsabile
                            FROM            Sedi
                            WHERE           Città = 'Bologna‘
                        ```
                * **DELETE**: cancella tuple.
                    ```
                        DELETE FROM     Sedi -- elimina le sedi di Bologna
                        WHERE           Città = 'Bologna'
                    ```
                * **UPDATE**: modifica tuple.
                    ```
                    Esempio 1:
                        UPDATE  Sedi
                        SET     Responsabile = 'Bruni', Città = 'Firenze'
                        WHERE   Sede = 'S01'
                    Esempio 2:
                        UPDATE  Impiegati
                        SET     Stipendio = 1.1 * Stipendio
                        WHERE   Ruolo = 'Programmatore'
                    ```
        + **Join**
            - **Interrogazioni su più tabelle**
                ```
                    SELECT I.Nome, I.Sede, S.Città
                    FROM   Impiegati I, Sedi S
                    WHERE  I.Sede = S.Sede 
                    AND    I.Ruolo = 'Programmatore'
                ```

                ![alt text](./res/int-p-tab.png "Esempio di interrogazione su più tabelle.")

            - **Self join**
                ```
                    SELECT  G1.Genitore AS Nonno
                    FROM    Genitori G1, Genitori G2
                    WHERE   G1.Figlio = G2.Genitore
                    AND     G2.Figlio = 'Anna'
                ```

                ![alt text](./res/sel-jo.png "Esempio di self join.")

            - **Join espliciti**
                * Tipi:
                    + LEFT [OUTER] JOIN
                    + RIGHT [OUTER] JOIN
                    + FULL [OUTER] JOIN
                    + NATURAL JOIN
                * Esempio:
                    ```
                        SELECT  I.Nome, I.Sede, S.Città
                        FROM    Impiegati I JOIN Sedi S ON (I.Sede = S.Sede)
                        WHERE   I.Ruolo = 'Programmatore'
                    ```
            - **Outer join**
                ```
                    SELECT   S.Sede, S.Responsabile, COUNT(I.CodImp)
                    FROM     Sedi S LEFT OUTER JOIN Impiegati I 
                    ON       (S.Sede = I.Sede)
                    AND      (I.Ruolo = 'Analista')
                    GROUP BY S.Sede, S.Responsabile
                ```
        + **Operatori insiemistici**
            - No duplicati:
                * **UNION**
                * **INTERSECT**
                * **EXCEPT**
            - Con duplicati: aggiungi **ALL**

            ![alt text](./res/op-in-1.png "Esempio di operatori insiemistici 1.")

            ![alt text](./res/op-in-2.png "Esempio di operatori insiemistici 2.")

    * **Data control language** (DCL)
        + Gestisce i permessi relativi agli utenti del Database.
    * **Funzioni aggregate**
        + L'argomento è una qualunque espressione che può figurare nella SELECT, ma NON UN'ALTRA FUNZIONE AGGREGATA!
        + Tutte le funzioni, eccetto COUNT, ignorano i valori nulli.
        + Il risultato è NULL se tutti i valori sono NULL.
        + Se si usano funzioni aggregate, la SELECT non può includere altri elementi che non siano funzioni aggregate.
        + **Raggruppamento**
            ```
                SELECT   Sede, COUNT(*) AS NumProg
                FROM     Impiegati
                WHERE    Ruolo = 'Programmatore'
                GROUP BY Sede
            ```

            ![alt text](./res/groupby.png "Esempio di COUNT con il GROUP BY.")

            - La SELECT può includere solo le colonne di raggruppamento, ma non altre (per il punto precedente).
            - **Condizioni sui gruppi**
                * **HAVING** simile a WHERE solo che per i gruppi.
                ```
                    SELECT   Sede, COUNT(*) AS NumImp
                    FROM     Impiegati
                    GROUP BY Sede
                    HAVING   COUNT(*) > 2
                    Risultato:
                    Sede    NumImp
                    S01     4
                    S02     3
                ```

            ![alt text](./res/es-gr-by-com.png "Esempio di GROUP BY completo.")

        + **DISTINCT**: valori distinti
            ```
                SELECT  SUM(DISTINCT Stipendio)
                FROM    Impiegati
                WHERE   Sede= 'S01'
            ```
        + Tipi:
            - **MIN**
            - **MAX**
            - **SUM**
                ```
                    SELECT  SUM(Stipendio * 12) AS ToTStipS01
                    FROM    Impiegati
                    WHERE   Sede= 'S01'
                ```
            - **AVG**
                ```
                Esempio 1:
                    SELECT  AVG(Stipendio) AS AvgStip
                    FROM    Impiegati   -- valore esatto 1412.5  
                    Risultato: AvgStip = 1412
                Esempio 2:
                    SELECT  AVG(CAST(Stipendio AS Decimal(6,2))) AS AvgStip
                    FROM    Impiegati
                    Risultato: AvgStip = 1412.50
                ```
            - **STDEV**: deviazione standard
            - **VARIANCE**: varianza
            - **COUNT**

                ![alt text](./res/count-null.png "Esempio di tabella impiegati.")

                ```
                Esempio 1:
                    SELECT  COUNT(*) AS NumImpS01 
                    FROM    Impiegati
                    WHERE   Sede= 'S01'
                    Risultato: NumImpS01 = 4
                Esempio 2:
                    SELECT  COUNT(Stipendio) AS NumStipS01
                    FROM    Impiegati
                    WHERE   Sede= 'S01'
                    Risultato: NumStipS01 = 3
                ```
    * **Subquery**: condizioni che si basano sul risultato di altre interrogazioni.
        + Non si possono usare operatori insiemistici (UNION, INTERSECT e EXCEPT).
        + Può comparire solo come operando destro in un predicato.
        ```
            SELECT  CodImp  -- impiegati delle sedi di  Milano
            FROM    Impiegati
            WHERE   Sede IN (SELECT Sede
                             FROM   Sedi
                             WHERE  Città = 'Milano')
        ```
        + **Scalari**: restituiscono non più di una tupla.
            ```
                SELECT  CodImp  -- impiegati con stipendio minimo
                FROM    Impiegati
                WHERE   Stipendio = (SELECT MIN(Stipendio)
                                     FROM   Impiegati)
            ```
        + **Generali**: restituiscono più di un valore.
            - \<op> ANY: la relazione \<op> vale per almeno uno dei valori.
                ```
                    SELECT  Responsabile
                    FROM    Sedi
                    WHERE   Sede = ANY(SELECT   Sede
                                       FROM     Impiegati
                                       WHERE    Stipendio > 1500)
                ```
            - \<op> ALL: la relazione \<op> vale per tuttii valori.
                ```
                    SELECT  CodImp  -- impiegati con stipendio minimo
                    FROM    Impiegati
                    WHERE   Stipendio <= ALL(SELECT Stipendio
                                             FROM   Impiegati)
                ```
        + **Livelli multipli di innestamento**
            - Una subquery può fare uso a sua volta di altre subquery. Il risultato si può ottenere risolvendo a partire dal blocco più interno.
        + **EXISTS**: verifica se il risultato di una subquery restituisce almeno una tupla.
            ```
                SELECT  Sede
                FROM    Sedi S
                WHERE   EXISTS(SELECT   *
                               FROM     Impiegati
                               WHERE    Ruolo = 'Programmatore')
            ```
        + **Correlate**: fa riferimento a "variabili" definite in un blocco esterno.
            ```
                SELECT  Sede    --sedi con almeno un programmatore
                FROM    Sedi S
                WHERE   EXISTS(SELECT *
                               FROM   Impiegati
                               WHERE  Ruolo = 'Programmatore'
                               AND    Sede = S.Sede)
            ```
        + **Unnesting**: ridurre una query innestata ad un unica query.
            - La query precedente può essere scritta:
                ```
                    SELECT  DISTINCT Sede
                    FROM    Sedi S, Impiegati I
                    WHERE   S.Sede = I.Sede
                    AND     I.Ruolo = 'Programmatore'
                ```
            - Esempio complesso: sede che ha il numero maggiore di impiegati con ruolo 'Programmatore'.
                ```
                Nested:
                    SELECT      Sede, COUNT(CodImp) AS NumProg
                    FROM        Impiegati I1
                    WHERE       I1.Ruolo = 'Programmatore'
                    GROUP BY    I1.Sede
                    HAVING      COUNT(CodImp) >= ALL
                                (SELECT     COUNT(CodImp)
                                 FROM       Impiegati I2
                                 WHERE      I2.Ruolo = 'Programmatore'
                                 GROUP BY   I2.Sede)
                Unnested:        
                    SELECT      TOP(1) WITH TIES Sede, COUNT(CodImp)
                    FROM        Impiegati
                    WHERE       Ruolo = 'Programmatore'
                    GROUP BY    Sede
                    ORDER BY    COUNT(CodImp) DESC
                ```
        + **Divisione**
            ```
                “Sedi in cui sono presenti tutti i ruoli” equivale a “Sedi in cui non esiste un ruolo non presente”
                
                SELECT  Sede
                FROM    Sedi S
                WHERE   NOT EXISTS(SELECT   Ruolo
                                   FROM     Impiegati I1
                                   WHERE    NOT EXISTS
                                   (SELECT  *
                                    FROM    Impiegati I2
                                    WHERE   S.Sede = I2.Sede
                                    AND     I1.Ruolo = I2.Ruolo))

                oppure “Sedi per cui il numero di ruoli distinti è uguale al numero totale di ruoli presenti”

                SELECT   Sede
                FROM     Impiegati I
                GROUP BY Sede
                HAVING   COUNT(DISTINCT Ruolo) = (SELECT   COUNT(DISTINCT Ruolo)
                                                  FROM     Impiegati)
            ```
        + **Aggiornamento dei dati** di una tabella sulla base di criteri che dipendono dal contenuto di altre tabelle.
            ```
                DELETE   FROM Impiegati  -- elimina gli impiegati di Bologna
                WHERE    Sede IN (SELECT Sede
                                  FROM   Sedi
                                  WHERE  Città = 'Bologna')
                                  
                UPDATE   Impiegati  -- aumenta lo stipendio agli impiegati 
                SET      Stipendio = 1.1 * Stipendio
                WHERE    Sede IN (SELECT S.Sede     --  delle sedi di 'P02'
                                  FROM   Sede S, Prog P
                                  WHERE  S.Città = P.Città
                                  AND    P.CodProg = 'P02')
            ```
        + **CHECK**: esprimere vincoli arbitrariamente complessi.
            - Ogni sede deve avere almeno due programmatori
                ```
                -- quando si crea la TABLE Sedi
                    CHECK(2 <= (SELECT  COUNT(*)
                                FROM    Impiegati I
                                WHERE   I.Sede = Sede   -- correlazione
                                AND     I.Ruolo = 'Programmatore'))
                ```
            - Supponendo di avere due tabelle ImpBO e ImpMI e di volere che uno stesso codice (CodImp) non sia presente in entrambe le tabelle
                ```
                -- quando si crea la TABLE ImpBO
                    CHECK(NOT EXISTS(SELECT * 
                                     FROM   ImpMI
                                     WHERE  ImpMI.CodImp = CodImp))
                ```
    * **Definizione di viste**
        + **CREATE VIEW**: definisce una vista, una "tabella virtuale"; le tuple della vista sono il risultato di una query che viene valutata dinamicamente ogni volta che si fa riferimento alla vista.
        ```
            CREATE VIEW ProgSedi(CodProg, CodSede)
            AS  SELECT P.CodProg, S.Sede
                FROM   Prog P, Sedi S
                WHERE  P.Città = S.Città

            SELECT  *
            FROM    ProgSedi
            WHERE   CodProg = 'P01'
        ```

        ![alt text](./res/es-vista.png "Esempio di tabella virtuale.")
        ![alt text](./res/res-vista.png "Operazione su tabella virtuale.")

        + **Aggiornabilità**
            - **NON** si può fare
                ```
                    CREATE VIEW NumImp(Sede, NImp)
                    AS  SELECT      Sede, COUNT(*)
                        FROM        Impiegati
                        GROUP BY    Sede
                            
                    UPDATE  NumImp
                    SET     NImp = NImp + 1
                    WHERE   Sede= 'S03'
                ```

                ![alt text](./res/ag-vis.png "Esempio di aggiornamento di viste (errato).")

            - **Restrizioni**
                * Il blocco più esterno non può contenere:
                    + **GROUP BY**
                    + **Funzioni aggregate**
                    + **DISTINCT**
                    + **Join** (espliciti o impliciti)
                ```
                Non aggiornabile:
                    CREATE VIEW ImpBO(CodImp, Nome, Sede, Ruolo, Stipendio)
                    AS  SELECT I.*
                        FROM   Impiegati I JOIN Sedi S ON (I.Sede = S.Sede)
                        WHERE  S.Città = 'Bologna'
                Equivalente ma aggiornabile:
                    CREATE VIEW ImpBO(CodImp, Nome, Sede, Ruolo, Stipendio)
                    AS  SELECT I.*
                        FROM   Impiegati I
                        WHERE  I.Sede IN (SELECT S.Sede
                                          FROM   Sedi S
                                          WHERE  S.Città = 'Bologna')
                ```
        + **CHECK OPTION**
            - Riferendoci a ImpBO sopra:
                ```
                    INSERT INTO ImpBO(CodImp, Nome, Sede, Ruolo, Stipendio)
                    VALUES('E009', 'Azzurri', 'S03', 'Analista', 1800)
                ```
                * Il valore di Sede `S03` non rispetta la specifica della vista, una successiva query su ImpBO non restituirebbe la tupla appena inserita.
                * **WITH CHECK OPTION** ci garantisce che ogni tupla inserita nella vista sia anche restituita dalla vista stessa.
                    + Se la vista V1 è definita in termini di un'altra vista V2, e si specifica la clausola WITH CHECK OPTION, il DBMS verifica che la nuova tupla t inserita soddisfi sia la definizione di V1 sia quella di V2, indipendentemente dal fatto che V2 sia stata a sua volta definita WITH CHECK OPTION.
                    + Ciò si può alterare definendo V1 `WITH LOCAL CHECK OPTION`, così da definire che il DBMS verifichi solo che t soddisfi la specifica di V1 e quelle di tutte e sole le viste da cui V1 dipende per cui è stata specificata la clausola WITH CHECK OPTION.
    * **Table expressions**: usare all'interno della SELECT o della clausola FROM una subquery che definisce "dinamicamente" una tabella derivata.
        + Per ogni sede, lo stipendio massimo e quanti impiegati lo percepiscono
            ```
                SELECT  SM.Sede, SM.MaxStip, COUNT(*) AS NumImpMaxStip
                FROM    Impiegati I, (SELECT    Sede, MAX(Stipendio)
                                    FROM      Impiegati
                                    GROUP BY  Sede) AS SM(Sede,MaxStip)
                WHERE   I.Sede = SM.Sede
                AND     I.Stipendio = SM.MaxStip
                GROUP BY SM.Sede, SM.MaxStip
            ```

            ![alt text](./res/t-e-es.png "Esempio table expression.")

        + **Correlazione** (SELECT e FROM)
            - Per ogni sede, la somma degli stipendi pagati agli analisti
                ```
                    SELECT  S.Sede,(SELECT      SUM(Stipendio)
                                    FROM        Impiegati I
                                    WHERE       I.Sede = S.Sede
                                    AND         I.Ruolo = 'Analista') AS TotStip
                    FROM    Sedi S 

                    oppure

                    SELECT  S.Sede, Stipendi.TotStip
                    FROM    Sedi S, (SELECT     I.Sede, SUM(Stipendio)
                                     FROM       Impiegati I 
                                     WHERE      I.Ruolo = 'Analista'
                                     GROUP BY   Sede) AS Stipendi(Sede, TotStip)
                    WHERE   S.Sede = Stipendi.Sede
                ```
        + **Common table expression**: "vista temporanea" che può essere usata in una query come se fosse una VIEW.
            ```
                WITH    SediStip(Sede, TotStip)
                AS      (SELECT     Sede, SUM(Stipendio)
                         FROM       Impiegati
                         GROUP BY   Sede)
                
                SELECT  Sede
                FROM    SediStip
                WHERE   TotStip = (SELECT MAX(TotStip)
                                   FROM   SediStip)
            ```
            - **Interrogazioni ricorsive**
                * Si consideri la tabella Genitori(Figlio, Genitore) e la query ricorsiva: trova tutti gli antenati (genitori, nonni, bisnonni,...) di Anna.
                * La formulazione mediante common table expressions definisce la vista temporanea (ricorsiva) Antenati(Persona, Avo) facendo l'unione di:
                    + una "subquery base" non ricorsiva (che inizializza Antenati con le tuple di Genitori)
                    + una "subquery ricorsiva" che a ogni iterazione aggiunge ad Antenati le tuple che risultano dal join tra Genitori e Antenati
                ```
                    WITH Antenati(Persona, Avo)
                    AS (
                        (SELECT Figlio, Genitore    -- subquery base 
                         FROM   Genitori)
                        UNION ALL                   -- sempre UNION ALL!
                        (SELECT G.Figlio, A.Avo     -- subquery ricorsiva
                         FROM   Genitori G, Antenati A
                         WHERE  G.Genitore = A.Persona)
                       )
                       
                    SELECT  Avo
                    FROM    Antenati
                    WHERE   Persona= ‘Anna’
                ```

            - **Limitazioni**
                * Si può usare solo nel blocco più esterno di un SELECT
                * Subquery ricorsive: non si possono usare funzioni aggregate, GROUP BY e SELECT DISTINCT.