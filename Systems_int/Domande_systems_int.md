# Domande Systems Integration

## Modulo 1

1. Un container in ambiente linux in cui esegue un web server, può essere considerato un "micro-servizio"?. Motivare la risposta.
    > **A**: Si. I container possono essere pensate come componenti per creare applicazioni. Per esempio, possiamo utilizzare un container per far girare una database ed un altro container per eseguire un web server.
2. Cos’è lo schema del Directory Service?
    > **A**: Uno schema è la definizione degli attributi e delle classi che fanno parte di un Directory Service. Uno schema include le regole che definiscono il tipo ed il formato dei dati che possono essere aggiunti al database. La classe User è un esempio di classe, questa può contenere attributi quali il nome, il cognome, il numero di telefono...
3. Che cosa caratterizza il modello publish-subscribe? Fate un esempio di software che utilizza questo modello.
    > **A**: Il modello publish-subscribe permette di gestire le comunicazioni locali "molti a molti" a run-time, tramite un bus di messaggi e l'utilizzo di topic. Ogni entità che vuole ricevere messaggi, si registra e dichiara a quali topic è interessata. Le entità che vogliono invece inviare informazioni, devono pubblicare messaggi indicandone un subject. I messaggi vengono consegnati alle entità che hanno indicato tra i topic di loro interesse il subject del messaggio. RabbitMQ e Microsoft Azure Service Bus Messaging usano questo modello.
4. Consideriamo dei computer all'interno di una rete che non possa comunicare in nessun modo con l'esterno, può avere senso sincronizzare tra di loro gli orologi? Motivare la risposta.
    > **A**: Si, risulta necessario sincronizzare gli orologi per fornire a processi fisicamente lontani (ossia su computer diversi) la stessa nozione del tempo. Per esempio, servizi come Kerberos, hanno dei meccanismi che si basano sul tempo (durata dei ticket), richiedendo quindi che le entità coinvolte siano sincronizzate temporalmente. Inoltre, a lungo andare, potrebbero presentarsi accelleramenti o rallentamenti che potrebbero rendere le macchine non sincronizzate e quindi inutilizzabili in contesti ove le marche temporali siano necessarie.
5. Nel contesto del protocollo NTP, che cos'è lo stratum?
    > **A**: Lo stratum è il concetto con cui vengono categorizzati i server NTP in vari livelli (tramite l'utilizzo di un indice crescente che parte da 1). I server di stratum 1 sono direttamente collegati ai server WWV e sincronizzano esclusivamente server di stratum 2 (questo per non sovraccaricare i pochi server presenti nello stratum 1). All'aumentare dello stratum, aumenta il numero di server ma diminuisce la precisione a causa dei ritardi di propagazione introdotti nella sincronizzazione. I client possono sincronizzarsi solo tramite server di stratum 2 o maggiori.
6. Nel contesto della sicurezza informatica, definire le diverse forme del concetto di Integrità.
    > **A**: Integrità: le informazioni o le risorse "sensibili" non devono subire alterazioni non autorizzate.
    > 1. Integrità dei dati (e delle comunicazioni): le informazioni devono rimanere come il legittimo proprietario le ha costruite.
    > 2. Integrità della sorgente (o mittente); in una comunicazione, il ricevitore del messaggio deve vedere non modificato il mittente del messaggio.
7. Il GDPR (General Data Protection Regulation) protegge dati quali l'ammontare dello stipendio di un dipendente? Motivare la risposta.
    > **A**: Si, in quanto il GDPR definisce responsabilità e procedure operative da attuare per chi deve usare, conservare questo tipo di informazioni personali (come le ore lavorative, lo stipendio, il recapito telefonico o la mail)
8. Spiegare la differenza tra Authentication e Authorization.
    > **A**: 
    > - Per Authentication (AuthN) si intende la verifica dell'identità di un utente del sistema quaqndo questo richiede il servizio. Richiede un'operazione preliminare di identificazione (Identification) dell'utente che consente di inserire l'utente nel sistema (Provisioning) e consegna all'utente le credenziali per le successive operazioni.
    > - Per Authorization (AuthZ) si intente, nel momento in cui un utente già autenticato richiede un servizio, il controllo effettuato dal sistema per capire se l'utente ha i permessi di accesso a quel servizio e, nel caso, autorizza l'utente ad accedere al servizio. Sarebbe il meccanismo di controllo di accesso alle risosrse, ed è necessaria un'operazione preliminare di assegnamento dei diritti (Granting), effettuata durante il provisioning, che stabilisce quali sono i diritti sui servizi per l'utente e li salva come attributi in struttura dati di un apposito DB.
9. Indicare alcuni vantaggi di un IAM centralizzato.
    > **A**: 
    > 1. Autenticazione di dominio: a tutti gli utenti di un determinato dominio
    > 2. Credenziali di dominio: valide per tutti gli host del dominio
    > 3. Single-Sign-On: permette di autenticarsi una sola volta per sessione
    > 4. Una sola credenziale per ciascun utente
    > 5. Una sola autenticazione per sessione di lavoro
    > 6. Utilizzo di tutte le risorse del dominio
    > 7. Stesse risorse disponibili da PC diversi del dominio
10. In uno scenario d'uso di un IAM centralizzato, spiegare il ruolo dell'Identiry Provider, e il ruolo del Service Provider.
    > **A**: 
    > - Identity Provider: riconosce l'utente e autorizza l'uso del servizio (lo IAM system vero e proprio).
    > - Service Provider: chi fornisce il servizio richiesto dall'utente.
11. Qual è lo scopo di NIS?
    > **A**: NIS (Nwetwork Information Service) consente che più host condiviando le informazioni contenute in un insieme di files di configurazione di sistema (Mappa) contenuti nella cartella Linux `/etc`. Infatti consiste nel primo esempio di amministrazione centralizzata per sistemi Linux.
12. Cos'è SAML 2.0 e cos'è Shibboleth.
    > **A**: SAML (Security Assertion Markup Language) è uno standard per realizzare SSO (single sign-on) in ambito web e permette di delegare l'autenticazione e l'autorizzazione di un'applicazione web ad un Identity Provider centrale. Consiste nella base di Shibboleth ("Parola d'ordine"), ossia un sistema di SSO per reti informatiche.
13. Perché se spedisco un messaggio usando MAC o firma digitale (e niente altro) non ottengo la proprietà di segretezza?
    > **A**: La segretezza (o confidenzialità) si ottiene nascondendo ai non autorizzati le informazioni o le risorse "sensibili". MACe la FD compiono una codifica sul solo digest di un documento, non sul documento per intero, quindi chiunque abbia accesso al documento può carpirne i contenuti.
14. Qualunque applicazione, presa così come è fatta, può usare kerberos per consentire agli utenti l'autenticazione e autorizzazione ad utilizzare dei servizi? Motivare la risposta.
    > **A**: No, Kerberos usa uno specifico protocollo che lo rende poco portabile, infatti per essere usato da un'applicazione essa dev'essere "Kerberizzata" (Kerberized).
15. Indicare, possibilmente con uno schema grafico, quali chiavi segrete statiche sono condivise tra le varie entità coinvolte nel protocollo kerberos.
    > **A**: Static Kerberos Shared Secret (SKSS) utilizza chiavi private condivise contenute (anche) nel DB, e sono:
    > 1. La chiave $`K_{c}`$ fra il _Client_ ed l'_Authentication Server_
    > 2. La chiave $`K_{tgs}`$ fra l'_Authentication Server_ ed il _Ticket Granting Server_
    > 3. La chiave $`K_{s}`$ fra il _Ticket Granting Server_ ed il _Service Server_
16. A cosa serve lo standard Simple Authentication and Security Layer (SASL)?
    > **A**: SASL è un meccanismo di autenticazione, sicurezza e/o di privacy nei protocolli Internet, usato da LDAP sopra SSL/TLS.
17. Nell'ambito dello standard LDAP, cosa si intende con "modello dei nomi della DIT" o "LDAP naming"?
    > **A (FORSE)**:
    > - Standard dei nomi per i primi livelli della DIT
    >   - Nei primi due livelli della DIT si usa come RDN un attributo che è adatto per distinguere tra le organizzazioni mediante il loro dominio di rete (domain component, dc)
    >   - I successivi due livelli tipicamente usano come RDN un attributo che è adatto per descrivere l'organizzazione del personale o dei servizi o delle sedi (organization unit, ou)
    >   - Un ulteriore livello identifica le singole persone (user id, uid)
18. Nell'ambito di Active Directory, che cos'è una foresta? Cosa la distingue da un albero?
    > **A**: Nell'ambito di Active Directory una foresta sono uno o più alberi con ognuno avete proprio name space univoco, mentre un albero è uno o più domini con name space contiguo. Nello specifico, una foresta è una collezione di domini che condividono lo stesso schema Active Directory, mentre un albero è una collezione di domini contenuti in una foresta che condividono un DNS namespace comune.
19. Nell'ambito del VoIP, qual è lo scopo del protocollo SIP?
    > **A**: Il protocollo SIP è utilizzato nell'ambito di una comunicazione VoIP per eseguire lo scambio di indirizzi fra i due host che vogliono comunicare, questo è ottenuto mediante un terzo host detto appunto server SIP.
20. Quale è la principale differenza che distingue le connessioni stabilite mediante l'ausilio di un server STUN rispetto a quelle connessioni che hanno richiesto l'utilizzo di un server TURN?
    > **A**: Se una connessione, per essere stabilita, ha dovuto usare solo un server STUN, questo vuol dire che non erano presenti 2 firewall simmetrici (ma al massimo 1), permettendo quindi di stabilire una connessione diretta tra gli endpoints. Se viene richiesto l'utilizzo del server TURN, questo significa che non è stato possibile stabilire una connessione diretta (quindi erano presenti 2 firewall simmetrici), dovendo quindi utilizzare il server TURN come intermediario (relay).
21. A cosa serve il protocollo TURN?
    > **A**: Il protocollo TURN (assieme al protocollo STUN) serve a capire che tipo di firewall ci sia davanti a due sistemi che vogliono comunicare fra loro. Un server TURN svolge il ruolo di relay qualora non vi possa essere una communicazione diretta (come quando si ha due firewall simmetrici).
22. Citare i due modi mediante i quali si risolve il problema delle istruzioni privilegiate nella full-virtualization.
    > **A**:
    > 1. Binary Translation: consiste nel riscrivere dinamicamente a run-time, man mano che vengono eseguite, le parti del codice del s.o. guest che dovrebbero essere eseguite in modo privilegiato.
    > 2. Hardware Assisted: 
    >     1. Aggiunge un nuovo ring all'architettura standard x86, chiamato `Ring -1` su cui esegue il s.o. host e l'hypervisor, mentre il s.o. guest gira sul `Ring 0`. 
    >     2. Vengono messe a disposizione diverse istruzioni, come VMPTRLD, VMPTRST, VMCLEAR, VMREAD, VMWRITE, VMCALL, VMLAUNCH, VMRESUME, VMXOFF e VMON, istruzioni permettono di accedere ed uscire da un'esecuzione virtuale dove il s.o. guest si percepisce come in esecuzione con tutti i privilegi (Ring 0), ma l'host rimane protetto.
    >     3. Vengono poi aggiunte delle funzionalità che aggiungono un ulteriore livello di mappatura delle pagine in memoria: Intel EPT (Extended Page Tables).
    >     4. Infine vengono aggiunte delle tabelle con le informazioni su ciascuna macchina virtuale (prima ne esisteva una sola, quindi non si potevano annidare le VM, una dentro l'altra): VMCS Shadowing.
23. In una macchina fisica in cui è installato Docker, a cosa serve il docker registry?
    > **A**: Il docker registry è un archivio contenente le immagini Docker in differenti versioni (in base ai tag). 
24. Posso mettere in esecuzione due container, partendo dalla stessa immagine, in modo che i due container eseguano simultaneamente? Potrebbero generarsi interferenza tra i due container? Motivare la risposta.
    > **A**: Si, in quanto i container possono essere generati dalla stessa immagine, ma risultano di fatto due elementi differenti con ambiente di lavoro diversi, e quindi indipendenti gli uni dagli altri.
25. Supponiamo che un immagine di un container contenga tutto il necessario per eseguire un applicativo, ad esempio il database documentale mongo. Però in quell'immagine non è installato un altro applicativo, ad esempio il magnifico editor testuale vim. L'applicativo vim è però installato nel sistema operativo Linux della macchina fisica. Supponiamo di mettere in esecuzione un container partendo da quell'immagine. Posso eseguire, all'interno del container, l'applicativo vi? Motivare la risposta.
    > **A**: No. I container sono entità isolate fra loro e rispetto dalla macchina host, per questo il concetto di container stesso cadrebbe se gli fosse consentita l'esecuzione di programmi sulla macchina fisica.
26. A cosa serve il comando `docker build`?
    > **A**: Il comando `docker build` serve a costruire una nuova immagine sulla base di un preesistente Dockerfile.
27. Posso salvare l'immagine di un container in esecuzione, dopo averlo stoppato? Se ritenete che si possa, quale comando dovreste usare?
    > **A**: Si, salvando i cambiamenti in un commit tramite il comando `$ docker commit -m "$commitmessage" -a "$author" $container_id $username/$destination_dockerimage `.
28. Che tipo di servizio offre il docker hub (https://hub.docker.com/)? 
    > **A**: Docker hub fornisce un catalogo di immagini create dagli utenti della comunità.
29. Un container offre un servizio sulla propria porta TCP 80. Nella macchina fisica in cui voglio eseguire il container, quella porta 80 è usata da un'altra applicazione. Senza modificare il container, posso eseguire quel container in modo da esporre il servizio offerto dal container su una diversa porta nella macchina fisica, ad esempio la porta 60000? Motivare la risposta.
    > **A**: Basta aggiungere l'opzione `-p -p $hostport:$containerport/$protocol` per pubblicare porte su un container, la sintassi sarebbe: `docker run -p 60000:80 --name $container_name`.
30. A cosa serve un Dockerfile?
    > **A**: Un Dockerfile è uno script contenente una collezione di istruzioni che saranno automaticamente eseguite in una sequenza nell'ambienete docker per costruire docker images.
31. Quale è la principale funzionalità che viene messa a disposizione dal DNS embedded che docker automaticamente configura per i container che usano una stessa user-defined bridge network?
    > **A**: Il DNS server di una determinata network assegna a tutti i container connessi ad essa un nome simbolico (di default il nome del container, se non definito l'id del container, customizzabile mediante l'opzione `--hostname`) che potrà essere poi utilizzato dai container stessi per comunicare.
32. È possibile attaccare un container a più di una rete?
    > **A**: Si, in docker c'è la possibilità di connettere un container ad una o più reti usanto il comando `docker network connect`.
33. Che cos'è iptables/netfilter?
    > **A**:
    >   - Netfilter: componente base incluso nel kernel delle macchine linux; viene utilizzato per definire l'instradamento dei pacchetti di rete.
    >   - Iptables: CLI che permette di interfacciarsi e utilizzare netfilter.
34. Che cos'è docker-compose e a cosa serve?
    > **A**: Possiamo considerare docker-compose come l'orchestratore/amministratore di un ambiente costituito da N docker containers, M network e O volumi definiti all'interno di un file di configurazione `docker-compose.yml` che interagiscono andando a comporre un'applicazione complessa.
35. Nel contesto di docker swarm, qual'è la differenza tra un nodo manager ed un nodo worker?
    > **A**: Un nodo manager (non ridondato) è il nodo che si occupa di distribuire il carico delle X richieste che si stanno ricevendo agli Y nodi worker. Il nodo manager può essere anche utilizzato come un nodo worker.
36. Nel contesto di docker swarm, che cos'è uno stack?
    > **A**: Lo stack è un gruppo di servizi (container) che realizzano un'applicazione.
37. Nel contesto di kubernetes, che cosa sono i pods?
    > **A**: I pods sono gruppi di container (anche composti da un unico container) che devono lavorare cooperando (si trovano sullo stesso host, anche più contemporaneamente sullo stesso nodo)
    > - I container che fanno parte di un pod
    >   - Condividono uno stesso indirizzo IP
    >   - Hanno lo stesso spazio delle porte di protocollo (due container in uno stesso pod non possono attestarsi sulla stessa porta di protocollo)
    >   - Possono comunicare tra di loro mediante il localhost (invece due container contenuti in pods diversi per comunicare tra di loro devono specificare l'indirizzo IP del pod a cui appartiene l'altro container)
    >   - Possono comunicare tra di loro mediante le Inter Process Communications (IPC)
38. Nel contesto di kubernetes, qual'è la differenza tra deployments e services?
    > **A**: 
    > - DEPLOYMENTS: gruppo di pods replicati a cui può essere richiesto uno stesso servizio (ai fini di essere sfruttati mediante il bilanciamento di carico)
    > - SERVICES: interfaccia verso l'esterno di un gruppo di deployments, si occupa lui del bilanciamento di carico (in sostanza è il load balancer di un pod X)

## Modulo 2 

1. Qual è il comando in ambiente linux con cui è possibile identificare il default gateway?
    > **A**: `ip route` oppure semplicemente `route`.
2. Quale comando devo usare per aggiungere un utente al gruppo sudo? Specificare la riga di comando da usare.
    > **A**: `$ usermod -G groupname username # Aggiungiamo l'utente username al gruppo groupname`.
3. In quale file di log vengono registrati i comandi eseguiti con sudo?
    > **A**: `/var/log/auth.log`.
4. Come effettua la risoluzione dei nomi un DNS configurato in modalita' forwarding?
    > **A**: I risultati delle richieste inviate al DNS server vengono prima cercati in cache, se non sono disponibili qui la richiesta viene inoltrata ad un DNS server esterno (google, ISP...).
5. A cosa serve il protocollo smb?
    > **A**: SMB è un protocollo di condivisione di file in rete usato da Samba, un progetto libero che fornisce servizi di condivisione di file e stampanti ai client usando il protocollo SMB/CIFS.
    > - Svolge le funzionalità dì:
    >   - Ricerca ei server in rete (Browsing)
    >   - Stampa su rete
    >   - Autenticazione e accesso a share di rete, file e cartelle
    >   - File lock
    >   - Supporto unicode
    > - Viene spesso usato come livello Applicativo (a livello di Trasporto viene usato NetBIOS - NBT)
    > - Può essere anche senza NBT
    > - Usa IPC (Inter Process Communication) come meccanismo di comunicazione per i servizi tra computer
6. Cosa sono le ACL e a cosa servono?
    > **A**: Windows ha una gestione dei permessi dei file più complessa rispetto a quella di Linux; le ACL (Access Control Lists, chi può avere accesso al file X) sono quella parte di permessi che sono presenti su Windows ma che su Linux non esistono e che vanno perciò aggiunti se si vuole eseguire il join di una macchina Linux ad un servizio di directory Windows.
7. A cosa serve il servizio apparmor in un ambiente linux?
    > **A**: Il servizio Apparmor serve per definire cosa un processo X può fare.
    > - `r`: lettura
    > - `w`: scrittura
    > - `m`: mappa in memoria come eseguibile
    > - `k`: mutua esclusione su dei file
    > - ...
8. A cosa serve winbind?
    > **A**: Winbind è il componente di SAMBA che permette il join ad un dominio con Active Directory di una macchina Linux.
9. A cosa serve PAM?
    > **A**: PAM (Pluggable Authentication Modules) è un sistema di autenticazione degli utenti per l'uso di applicazioni che operano su un host, e che delega l'autenticazione all'host. Fornisce API per l'autenticazione e, se un programma ha bisogno di far autenticare un utente, chiama la routine pam_authenticate che accede ad un file di configurazione nella cartella `/etc/pam_a` avente lo stesso nome del programma. 
    > PAM è caratterizzato da:
    > 1. Sempliciticà d'uso
    > 2. Configurabilità
    > 3. Estensibilità
10. Quali sono i ruoli FSMO?
    > **A**: I ruoli FSMO (Flexible Single Master Operation) si suddividono in per-domains roles e per-forest roles.
    >
    > - Per-domains roles:
    >   - PDC Emulator - Importante per i PC pre-Windows 2000, è il master per la sincronizzazione degli orologi, gestisce i cambi di password degli utenti del dominio
    >   - RID Master – Gestisce i Relative ID degli oggetti creati ed è responsabile dello spostamento di un oggetto da un dominio ad un altro
    >   - Infrastructure Master – Si preoccupa che il riferimento di oggetti fra domini sia consistente
    >
    > - Per-forest roles:
    >   - Schema Master – Gestisce i cambiamento allo schema della foresta e la propagazione delle modifiche agli altri DC
    >   - Domain Naming Master - Questo ruolo si occupa dell’aggiunta o rimozione di domini da una foresta.
11. A cosa serve l'Infrastructure Master?
    > **A**: L'Infrastructure Master è il responsabile degli aggiornamenti degli oggetti SID e DN.
12. Perché si installano i Remote Server Administrator Tools in ambiente Windows?
    > **A**: Remote Server Administration Tools (RSAT) serve a gestire da remoto Active Directory, DNS e DHCP di un dominio Active Directory.
13. Cos'è il servizio Radius?
    > **A**: RADIUS (Remote Authentication Dial-In User Service) è un protocollo AAA (Authentication, Authorization, Accounting) utilizzato in applicazioni di accesso alle reti.
    >
    > Le parti che costituiscono l'architettura RADIUS sono:
    >
    > - Un server di accesso alla rete (NAS - Network Access Server)
    > - Un server che si occupa di effettuare l'autenticazione dell'utente al server RADIUS (che si occupa di controllare il pacchetto contenente le credenziali di accesso dell'utente)
    >
    > Le credenziali vengono confrontate con quelle esistenti in un DB
    >
    > - In caso di matching. l'utente può accedere ai servizi della rete
    > - In caso di fallimento, il protocollo notifica con un messaggio di errore
    >
    > La comunicazione fra il NAS ed il server RADIUS è crittografata usando una chiave segreta condivisa da entrambi (secret key) che dev'essere opportunamente creata sia sul NAS che sul server RADIUS e mantenuta riservata
14. Gli script di startup di un PC del dominio possono essere definite in una policy di computer o in una policy di utente?
    > **A**: In una policy di computer poichè queste vengono applicate al PC indipendentemente dall’utente collegato.
15. A cosa server il comando `nsupdate`?
    > **A**: `nsupdate` è un comando usato dall'amministratore di rete che sollecita il name_server du una zona DNS ad aggiornare il suo database.
16. A cosa serve il comando `klist`?
    > **A**: `klist` è un comando usato per visualizzare un elenco dei ticket Kerberos attualmente memorizzati nella cache.

## Modulo 3

1. Cosa si intende per Dominio Windows?
    > **A**: Un dominio Windows è un gruppo logico di computer che condividono un database di directory centralizzato detto, in linguaggio Microsoft, Active Directory.
3. Quanti tipi di DC c'erano prima di Active Directory (Pre-Windows 2000)
    > **A**:
    > - Esisteva un PrimaryDomain Controller (PDC) che manteneva il database degli utenti del dominio.
    > - Esistevano zero o più Backup Domain Controller (BDC) che mantenevano una copia in sola lettura del database degli utenti. Quindi le modifiche al DB avvenivano solo sul PDC che poi le propagava ai vari BDC (se esistevano).
7. Come viene individuata una entry all'interno del Directory Service?
    > **A**: Mediante il DN (distinguished name), composto dal RDN (relative distinguished name) e dal BDN (base distinguished name).
10. Da quali protocolli è formato Active directory?
    > **A**: NTP (sincronizzazione degli orologi), DNS (risoluzione dei nomi degli elementi del dominio), Kerberos (servizio di autenticazione basato sui ticket) e LDAP (protocollo di directory che implementa in maniera effettiva lo scambio di risorse fra gli host di un directory service).
11. Come realizza il SSO Active Directory?
    > **A**: Tramite l'utilizzo di kerberos. (Se necessario vedi spiegazione Kerberos di Computerphile/slide)
14. Quanti e quali tipi di DNS server possono esserci?
    > **A**: 
    > - Forwarding: tutte le richieste DNS vengono rigirate verso DNS esterni tipo google. E’ il compito che normalmente effettua il router di casa. Per esempio se noi cerchiamo di risolvere www.example.com il server DNS prima cerca in cache e se non c’è la richiesta viene forwardata al DNS del provider (telecom, fastweb …).
    > - Recursion: il server DNS risolve i domini andando ad effettuare delle query ricorsive. Per esempio per risolvere il dominio foo.example.com il server DNS prima effettua una query per sapere i server DNS che gestiscono il dominio .com, poi recupera i server DNS che gestiscono il dominio example.com ed infine interroga i server DNS che gestiscono il dominio example.com per sapere l’indirizzo IP di foo.example.com.
15. Cosa rappresenta un record SRV all'interno del DNS
    > **A**: Un Service record (SRV record) definisce la location (hostname e porta) dei server per specifici servizi; viene molto utilizzato per i server SIP.
    > - Esempio: `_sip._tcp.example.com. 3600	IN	SRV	10 20 5000 sip-server.example.com.`
17. Posso avere gruppi con lo stesso nome nello stesso dominio in OU diverse? Perché?
    > **A**: Si, poichè il BDN sarà diverso in quanto l'OU è differente.
18. Cos'è un sito Active Directory?
    > **A**: Raggruppamento fisico e non logico di computer, spesso legati da una stessa sottorete IP. La definizione dei siti è indipendente dalla struttura dei domini e delle OU (un sito può apparire in diversi domini e diversi domini possono apparire in un sito).
19. Cosa è memorizzato all'interno del Global Catalog?
    > **A**: Un Domain Controller configurato come Global Catalog conserva tutte le informazioni relative al suo dominio più le informazioni relative agli altri domini della foresta. Per ogni Sito della Foresta è preferibile avere almeno un Global Catalog in modo tale che le richieste vengano fatte a questo, senza la necessità di interrogare Domain Controller al di fuori del proprio sito.
21. A cosa servono i ruoli FSMO?
    > **A**: In Active Directroy i DC sono paritetici, quindi le modifiche agli oggetti possono essere fatte su uno qualunque dei DC, questa funzionalità viene detta multimaster update. Gli eventuali conflitti vengono gestiti attraverso un processo che viene chiamato conflict resolution, che in pratica fa "vincere" l’ultima modifica fatta all'oggetto. Ci sono dei casi invece in cui è meglio prevenire eventuali conflitti, e qui entra in gioco l’operation master, cioè l’unico DC del dominio o della foresta che può effettuare determinate modifiche (questo si dice che ha un ruolo FSMO X che gli permette di essere il master per una certa categoria di modifiche, vedi Modulo 2 n. 10)
24. Come viene gestita la concorrenza sulla modifica di oggetti in AD?
    > **A**: Vedi risposta sopra.
25. È possibile far amministrare ad un utente non amministratore una parte di AD? Motivare la risposta.
    > **A**: Si, mediante le group policy è possibile delegare dei permessi ad altri utenti che non siano gli amministratori. Questo per motivi amministrativi e gestionali.
27. Con VMware Workstation, posso far comunicare una VM con le macchina sulla rete e viceversa? Se si come?
    > **A**: Si, questo può essere ottenuto connettendo la VM e la machina host ad una rete virtuale in modalità bridge.
30. Come decido quante risorse (CPU e memoria) assegnare alla VM?
    > **A**: Le risorse vanno assegnate in base ai requisiti della macchina virtuale e alla capacità della macchina host su cui si sanno facendo girare le VM.
31. Posso collegare ad una VM un disco di un'altra VM?
    > **A**: Ci sono diverse interpretazioni della domanda (chiedere chiarimenti se la domanda è posta in un esame). Condividere un disco su cui è presente un'installazione di un SO non è possibile, lo è invece condividere un disco virtuale di soli dati.
32. Quali sono le differenze fra un disco thin e uno thick?
    > **A**: Un disco thin usa solo lo spazio che viene utilizzato e cresce dinamicamente, un disco thick invece alloca una dimensione fissa di spazio utilizzabile.
36. Quando è meglio clonare un VM?
    > **A**: È meglio clonare una VM quando questa è in uno stato stabile, creando così un backup.
37. Cos'è una snapshot di una VM? Quando è meglio farla?
    > **A**: Fare snapshot di una VM è significa congelare lo stato della macchina in un dato momento e potervi eventualmente ritornare in caso di bisogno, e spesso viene fatta prima di effettuare modifiche importanti.
38. Un DC, può avere un indirizzo assegnato via DHCP?
    > **A**: No, un DC ha bisogno di un indirizzo IP statico.
40. Una volta installato il ruolo di Active Directory Domain service, il server è già un DC?
    > **A**: Si, previa promozione del server in questione a ruolo di DC.
41. Il functional level si applica al dominio o alla foresta?
    > **A**: A tutti e due. Il functional level determina le funzionalità disponibili per il dominio o la foresta di Active Directory Domain Services (AD DS).
45. Che strumento uso per gestire le deleghe?
    > **A**: In ambiente windows server usiamo un wizard chiamato Active Directory Users and Computer dove possiamo delegare ad alcune persone diritti di creazione e modifica sugli oggetti di una determinata OU (e conseguentemente le sottostanti OUs).
