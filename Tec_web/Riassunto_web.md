# Tec Web

Progetto: [EventNation](https://gitlab.com/emrevoid/uni/web/project/src)

### Parte 1

- World Wide Web
    - The World Wide Web (abbreviated WWW or the Web) is an information space where documents and other web resources are identified by Uniform Resource Locators (URLs), interlinked by hypertext links, and can be accessed via the Internet

- Tecnologie fondamentali (da sempre)
    - HTML (HyperText Markup Language): Formato per le pagine
        - Il contenuto del documento è inframezzato da elementi di markup (tag: <>) che ne definiscono struttura e semantica
    - URI (Uniform Resource Identifier): Identificazione delle risorse
    - HTTP (HyperText Transfer Protocol): Interazione client/server tramite TCP
        - Funzionamento:
            - L'interazione client/server è iniziata esclusivamente dal client che attira a sé (pull) i contenuti richiesti
            - In certi casi il server spinge (push) l'informazione al client (es: casting e instant messaging)
            - Recentemente si è adottato un modello pseudo-push basato su polling (pool): un processo automatico, ad intervalli regolari, interroga il server (poll) per sapere se ci sono nuovi contenuti
    
- Elementi costituenti
    - Linguaggi
        - CSS (Cascading Style Sheets): Linguaggio che definisce lo stile di pagina
        - Javascript: Linguaggio di scripting orientato agli oggetti e agli eventi, comunemente utilizzato nella programmazione Web lato client
            - Interpretato, debolmente tipizzato e debolmente orientato agli oggetti
            - Funziona anche lato server
        - Java client-side
            -  Applet (deprecate): Applicazioni scritte in Java (introdotte nel 1995) che vengono incapsulate all'interno di pagine web (con \<object>) e fatte eseguire nel browser.  
        - PHP (PHP Hypertext Preprocessor): Linguaggio di scripting, interpretato, originariamente concepito per la programmazione di pagine web dinamiche lato server
            - Funzionamento
                - Solo PHP: Il browser richiede una risorsa al server, se c'è del codice PHP il server lo esegue e ritorna al client la pagina HTML con l'output del PHP
                - PHP + JS: Come prima ma il JS embeddato nella pagina è eseguito lato client
    - Enti
        - Browser (client): Visualizzatore di documenti ipertestuali e multimediali in HTML
            - Comprende:
                - Plug-in (utili per visualizzare formati speciali, es: Adobe Flash, ora però in disuso)
                - Sandbox Javascript interna
        - Server: Applicazione in grado di rispondere a richieste di risorse locali (file, record di database, ecc.) individuate da un identificatore univoco
    - Tecnologie
        - Cookies: Blocco di dati opaco (cioè non interpretabile) lasciato dal server in consegna ad un richiedente per poter ristabilire in seguito il suo diritto alla risorsa richiesta
        - AJAX (Asynchronous JavaScript and XML): Gruppo di tecnologie utilizzate per la realizzazione di RIA (Rich Internet Application) ovvero applicazioni web client-side e server-side fortemente interattive
            - Lo sviluppo AJAX si basa su uno scambio di dati in background fra web browser e server, che consente l'aggiornamento dinamico di una pagina web senza esplicito ricaricamento da parte dell'utente
            - L'uso di XML non è indispensabile, si usa per esempio JSON
        - Framework di sviluppo: Librerie che rendono più ricco, sofisticato e semplice l'uso di una tecnologia
            - Tipi
                - In base a dove girano:
                    - Server-side
                    - Client-side
                - In base all'organizzazione:
                    - Full-stack ("monolite")
                    - Glue (tanti pezzi uniti)
            - Solution stack: Insieme di componenti o sottosistemi software che sono necessari per creare una piattaforma completa in modo che nessun software aggiuntivo sia indispensabile allo sviluppo di applicazioni.
                - Per lo sviluppo Web è solitamente costituito da 4 elementi: sistema operativo, web server, database, e linguaggio di programmazione
                - Esempi:
                    - LAMP (FOSS)
                        - SO: Linux
                        - Web-server: Apache
                        - DBMS: MySQL
                        - Lang: PHP, Perl, Python..
                    - WAMP (come LAMP ma gira su Windows)
                    - WISA (Microsoft)
                        - SO: Windows
                        - Web-server: Internet Information Services
                        - DBMS: Microsoft SQL Server
                        - Lang: ASP.NET
                    - MEAN (multipiattaforma)
                        - Web-server: Node.js
                        - Framework JS client-side: Angular JS
                        - Framework JS server-side: Express.js
                        - DBMS: MongoDB

### Parte 2 (HTML)

- Markup: un linguaggio di markup è un linguaggio (con una specifica sintassi) che consente di annotare un documento  fornendone una interpretazione delle sue parti
    - Tipi
        - Procedurale: indicano le procedure di trattamento del testo, il markup specifica le istruzioni che devono essere eseguite per visualizzare la porzione di testo referenziata (es. TEX)
        - Descrittivo: identificano strutturalmente il tipo di ogni elemento dal contenuto. Invece di specificare effetti grafici come l’allineamento o l’interlinea, ne individuo il ruolo all’interno del documento (es. HTML)
    - Metamarkup: consiste nel fornire regole di interpretazione del markup e permette di definire nuovi linguaggi di markup
        - Un metalinguaggio di markup
            - Fornisce una sintassi per definire le regole da applicare nella marcatura di un determinato tipo di documenti
            - Consente di definire altri linguaggi di markup descrivendone la grammatica
        - XML (Extensible Markup Language): progettato per lo scambio e la interusabilità di documenti strutturati su Internet
            - Documenti espressi indipendentemente dalla loro destinazione finale
            - Definire e utilizzare elementi e attributi personalizzati definendo in questo modo nuovi linguaggi di markup
            - E' possibile creare un modello, chiamato Document Type Definition (DTD), che descrive la struttura e il contenuto di una classe di documenti (lo stesso XML ha un suo DTD e fino ad HTML4 c'era anche per l'HTML, poi siccome nessuno lo usava in HTML5 è stato abolito)
                - In base al DTD i documenti XML si dividono in:
                    - Well-formed: conformi alle generiche specifiche XML
                    - Valid: conformi ad una specifica DTD (che può essere interna, nel file XML, o esterna, in un altro file); un documento valid è sempre well-formed
                - Esempio:
                    ```xml
                    <!--Intestazione -->
                    <?xml version="1.0" ?>
                    <!--DTD -->
                    <!DOCTYPE tesi SYSTEM ”tesi.dtd">
                    
                    <tesi sessione=‘II’>
                        <titolotesi> Linguaggi di Markup</titolotesi>
                        <autore> Pinco Pallino </autore>
                        <relatore> Paola Salomoni</relatore>
                        <capitolo>
                            <numero>1</numero>
                            <titolo>introduzione</titolo>
                            <paragrafo>
                                . . . . .
                            </paragrafo>
                        </capitolo>
                    </tesi>
                    ```
- Storia
    - Mosaic: primo browser che si evolve in Netscape
    - Prima guerra dei browser: Explorer vs Netscape, vince Explorer
    - Conseguenze guerra: ognuno ha "migliorato" html aggiungendo robe solo per il suo browser, no standard
    - W3C (wwwconsortium, fondato da Berners-Lee): ente di standardizzazione che impone di far passare da lui le modifiche ad HTML così da standardizzarle
    - W3C vuole eliminare HTML4 e creare XHTML (bleah!)
    - Quindi nasce il WHAT WG che crea HTML5 che verrà poi riadottato da W3C e reso standard
    - Il WHAT WG aggiunge ogni giorno cose nuove ad HTML5, per questo il linguaggio è definito "living standard"

- Stato dell'arte
    - Scrivere codice well-formed
        - Elementi tutti in minuscolo
        - Elementi con contenuto aperti e chiusi, ed elementi vuoti (senza contenuto) scritti correttamente
    - Seguire la semantica degli elementi
        - La pagina deve quindi essere divisa in parti a seconda del ruolo che queste hanno e questi ruoli (header, footer, ecc.) devono essere specificati
    - Separare la presentazione (CSS3) dal contenuto (HTML5)
    - Scrivere codice accessibile
    - Scrivere codice portabile 
    - Scrivere codice velocemente

- HTML5
    - Contenuto
        - Interactive: interazione con gli utenti
        - Metadati (in head): identifica la presentazione o il comportamento del resto del contenuto, o le relazioni del documento con altri documenti
            - \<title>: titolo o nome del documento
                - Potrebbe essere usato fuori dal contesto (come per esempio accade nei bookmark) ovvero senza avere a corredo il contenuto del documento
                - Ogni documento ha al massimo un title
                - Es: `<title>Materiale dell’insegnamento di Tecnologie Web</title>`
            - \<base>: indica il path base del documento che servirà per risolvere gli URL relativi
                - Può contenere:
                    - href
                    - target (_blank apre una nuova finestra) 
                - Deve essere unico, se non lo è viene considerato solo il primo
                - Es: `<base href="http://www.w3schools.com/images/" target="_blank“/>`
            - \<link>: creare relazioni tra il documento e altri documenti o risorse (principalmente CSS)
                - Es: `<link rel="stylesheet" type="text/css" href="theme.css">`
            - \<meta>: aggiungere altri metadati al documento (spesso usati dai motori di ricerca)
                - Il tipo di metadato è specificato dall'attributo name
                - Es:
                    ```html
                    <meta charset="UTF-8">
                    <meta name="description" content="Free Web tutorials"> // Google
                    <meta name="keywords" content="HTML,CSS,XML,JavaScript"/>
                    <meta name="author" content="HegeRefsnes"/>
                    ```
            - \<style>: includere stili all'interno del documento
                - Es: 
                    ```html
                    <style>
                        h1 {color:red;}
                        p {color:blue;}
                    </style>
                    ```
        - Embedded: importa un'altra risorsa inserita nel documento
    - Categorie
        - Phrasing: testo del documento e elementi che marcano il testo
        - Flow content: ingloba tutto il resto, è il contenuto del documento
        - Heading: definisce le intestazioni
        - Sectioning: dividono il documento in parti, a cui è assegnato un ruolo
    - Struttura
        - Definito dal DOCTYPE come html
        - Incluso tra \<html> e \</html>
        - Strutturato in:
            - \<head>\</head> intestazione del documento che riporta informazioni sulla pagina o sulle relazioni con altri documenti
            - \<body>\</body> corpo del documento che racchiude il vero e proprio contenuto della pagina

- Charsets
    - ASCII: 7 bit, si usa un byte di memoria ma gli ottetti da 128 a 255 non sono utilizzati
    - ISO 646: ASCII in cui alcuni caratteri sono sostituiti da lettere accentate
    - ISO 8859
        - ISO Latin 1: i primi 128 caratteri sono quelli di ASCII, gli altri 128 sono usati per introdurre i caratteri latini specifici (per lingue europee)
        - ISO Latin 15: ISO Latin 1 con migliorie
    - Unicode e ISO/IEC 10646 (pesante): 
        - UCS-2: due byte, estensione di ISO Latin
        - UCS-4: 31 bit in 4 byte
    - UTF: consente di usare tutti i caratteri definiti in UCS ma utilizzando una codifica a lunghezza variabile
        - UTF-16 = UCS-2
        - UTF-8: considera di accedere a tutti i caratteri di UCS-4, ma utilizza un numero compreso tra 1 e 4 byte per farlo
            - Struttura
                - 0-127 (ASCII a 7 bit) = 1 byte
                - Alfabeto latino e tutti gli script non-ideografici = 2 byte
                - Ideogrammi (orientali) = 3 byte
                - Altro = 4 byte
            - Le codifiche UTF-8 e Latin 1 sono compatibili ma non identiche

### Parte 3

- Web design: processo di pianificazione e creazione di un sito web
    - Web designer: progetta e crea le singole pagine web
    - Elementi
        - Testo
            - Necessario un sistema di codifica che stabilisca come rappresentare i diversi simboli
            - Ogni simbolo può essere rappresentato visivamente in modi diversi, con diversi stili tipografici, dimensioni e colori
            - Glifo: entità tipografica che realizza la rappresentazione visiva della forma del carattere
            - Font: insieme di glifi caratterizzati da un certo stile grafico che rappresentano i caratteri di un alfabeto
                - Classificazione
                    - Dimensione
                        - Proporzionale: i glifi hanno lunghezza variabile
                        - Monospace: i glifi hanno larghezza fissa
                    - Formattazione
                        - Serif: glifi con grazie
                        - Sans-serif: glifi senza grazie
                - Family
                    - Font family: insieme di stili diversi di uno stesso carattere
                    - Generic family: insieme di font family accomunati da caratteristiche simili
            - Interlinea: spazio fra due linee
        - Colori (NB: Daltonismo progetto)
            - Indicati attraverso il loro nome (in inglese) oppure attraverso il loro codice RGB espresso in forma esadecimale/intera
            - La codifica RGB descrive la corrispondenza tra colori e terne di numeri compresi tra 0 e 255
            - Red, Green and Blue poichè sono i colori primari additivi, cioè sommandoli si possono creare tutti i colori
            - Se i 3 valori sono tutti uguali si avrà una tonalità di grigio
        - Layout di pagina
    - Paradigma
        - Graceful degradation: da design ricchi per i desktop a layout semplificati per il mobile
        - Progressive enhancement (mobile first): si parte dalla condizione più vincolante e si creano design progressivamente più ricchi

### Parte 4 (HTML)

- Sectioning: gli elementi di questa categoria hanno funzione strutturale, ovvero dividono la pagina in parti con semantica (ruolo) diverso a seconda dell’elemento usato
    - \<section>
        - Definisce una sezione del documento
        - Esempio
            - Una home page potrebbe essere suddivisa in 3 sezioni: 
                - una per l’introduzione
                - una per il contenuto vero e proprio 
                - una per le informazioni sui contatti
    - \<article>
        - Un articolo dovrebbe essere un elemento con uno suo senso proprio, che potrebbe essere letto in modo indipendente dal resto della pagina Web
        - Esempio
            - Post suun blog
            - Articolodi un quotidiano
    - \<header>
        - Definisce l’intestazione di un documento o di una sua sezione
        - Possono essere presenti più \<header> in ogni pagina Web
    - \<footer>
        - Solitamente un footer contiene le informazioni sull’autore del documento, le informazioni sul copyright, link ai termini d’uso, informazioni sui contatti...
        - Possono essere presenti più \<footer>in ogni pagina Web
    - \<nav>
        - Definisce un insieme di link di navigazione (menù o toolbar o altri set di link)
        - Esempio
            ```html
            <nav>
                <a href="/html/">HTML</a> |
                <a href="/css/">CSS</a> |
                <a href="/js/">JavaScript</a> |
                <a href="/jquery/">jQuery</a>
            </nav>
            ```
    - \<aside>
        - Definisce un contenuto a latere rispetto a quelli principali
        - Può essere utilizzato per contenere i contenuti di una barra laterale e altri elementi

- Headings
    - Gli headings introducono i titoli delle diverse sezioni del documento
    - Vanno da \<h1> a \<h6> a seconda della loro rilevanza
    - E' necessario usare sezioni esplicite (sempre con elemento di sezione e titolo, mai solo il titolo)
    - Far combaciare il grado dell'intestazione con il livello di nidificazione previsto
    - Esempio
        ```html
        <section>
            <h1>Linguaggi di Markup</h1>
            <p>L'insegnamento di Tecnologie Web introduce diversi linguaggi di markup tra cui....</p>
            <section>
                <h2>XML</h2>
                <p>(ExtensibleMarkup Language) è un meta-linguaggio di markup, progettato per lo scambio e la interusabilitàdi documenti strutturati su Internet. </p>
            </section>
        </section>
        ```

- Phrasing: contenuto che rappresenta il testo del documento
    - \<p>
        - L’elemento inserisce un paragrafo testuale
        - L’andata a capo non basta
    - \<br/>
        - Interruzione di linea (line break)
        - Deve essere usato solo per interruzioni che sono effettivamente parte del contenuto, come negli indirizzi, nelle poesie o nel codice, NON per ottenere effetti grafici
    - \<div>
        - Non ha alcun significato proprio ma ha lo scopo di rappresentare gli elementi in esso annidati e specificare per loro gli attributi class, lang e title
    - \<span>
        - Opera in modo simile all’elemento \<div> ma a livello di testo
    - \<main>
        - Raggruppa gli elementi di struttura (come \<section> o \<article>) che rappresentano il contenuto principale del documento
        - Il contenuto deve essere caratterizzante di quel documento, quindi vanno esclusi i contenuti che sono ripetuti in diverse pagine (come per esempio le barre di navigazione)
        - Ogni documento deve avere un solo \<main>
    - \<a>
    - Elementi che attribuiscono ruolo al testo
        - \<i>: testo in voce alternativa (termini tecnici, frasi idiomatiche, pensieri, testo in altra lingua..)
        - \<em>: stress emphasis (un testo o una frase che si pronuncia in modo differente dal resto)
        - \<strong>: strong importance (testo importante)
        - \<b>: offset text (conventionally styled in bold, testo più visibile)
        - \<small>: note a margine
        - \<abbr>: abbreviazioni o acronimi; l’attributo title è usato per inserire la versione espansa del termine
        - \<var>: variabili o costanti usate in documenti a carattere scientifico
        - \<dfn>: definizione di un termine
            - Se ha un attributo title, questo assume il valore di definizione
            - Se non ha un attributo title, deve contenere solo un elemento \<abbr> che abbia un title che rappresenta la definizione
            - Altrimenti il testo contenuto in \<dfn>\</dfn> è la definizione
        - \<code>: porzioni di codice
        - \<sub> e \<sup>: apice e pedice
        - Citazioni
            - \<blockquote>: per parti di contenuto che vengono citate da una sorgente esterna (specificabile attraverso l’attributo opzionale cite)
            - \<q>: simile a blockquote ma agisce su un breve testo; si può usare l’attributo cite
            - \<cite>: per citare i riferimenti ad un lavoro creativo (come  una bibliografia); deve includere il titolo del lavoro, o il nome dell’autore o l’URL di riferimento
        - \<ins> e \<del>: servono a marcare le modifiche al documento, inserimenti e cancellazioni (si usa cite per specificare fonti aggiuntive e datetime per specificare data e ora della modifica)
        - \<map>
            - Una mappa è un’immagine in cui alcune aree sono interattive, attivano un link o altre azioni
            - Tipi
                - Client-side: il browser esamina la locazione del click e attiva/gestisce l’interazione
                - Server-side: il server esamina la locazione del click e attiva/gestisce l’interazione
            - Esempio client-side
                ```html
                <img src="planets.gif" width="145" height="126" alt="Planets" usemap="#planetmap">
                <map name="planetmap">
                    <area shape="rect" coords="0,0,82,126" href="sun.htm" alt="Sun">
                    <area shape="circle" coords="90,58,3" href="mercur.htm" alt="Mercury">
                    <area shape="circle" coords="124,58,8" href="venus.htm" alt="Venus">
                </map>
                ```

- Embedded: ha lo scopo di importare risorse (multimediali) o contenuto dentro il documento; alcuni elementi embedded prevedono un contenuto fallback, che viene usato quando la risorsa esterna non può essere utilizzata
    - \<figcaption> e \<figure>: definiscono la didascalia di un'immagine e la raggruppano
        - Esempio
            ```html
            <figure>
                <img src="pic_mountain.jpg" alt="PulpitRock" width="304" height="228"/>
                <figcaption>Fig.1 -PulpitRock, Norvegia.</figcaption>
            </figure>
            ```
    - \<img>: definisce le immagini inline
        - Attributi obbligatori
            - src: specifica l'URL del file contenente l'immagine
            - alt: testo alternativo, viene visualizzato in caso il browser non riesca a mostrare l’immagine e in caso di immagini disabilitate (non vedenti)
                - Serve SEMPRE nelle immagini rilevanti, altrimenti è vuoto
        - Attributi opzionali
            - usemap: specifica che l’immagine è una mappa lato client
            - ismap: specifica che l’immagine è una mappa lato server
            - width: specifica la larghezza dell’immagine in pixel
            - height: specifica l’altezza dell’immagine in pixel
            - longdesc: URL che porta alla descrizione lunga per l’accessibilità
        - Esempio
            ```html
            <img src="smiley.gif" alt="Smiley face" width="42" height="42">
            ```
    - \<video>: definisce un modo standard per includere un video in una pagina Web
        - Attributi
            - controls: aggiunge i controlli per il video (play, pausa, volume..)
            - width: definisce la larghezza del video; se non specificato il valore di height, viene calcolato mantenendo le proporzioni originali del video
            - autoplay
        - È possibile proporre il video in diversi formati, usando gli elementi \<source>; il browser utilizzerà il primo riconosciuto e supportato
        - Il testo contenuto in \<video>\</video> viene mostrato nel caso in cui il browser non supporti l’elemento
    - \<audio>: sostanzialmente equivalente a \<video> ma per l'audio
    - \<object>: definisce un oggetto incluso in un documento HTML
        - Supportato da tutti i browser
        - In modo simile a \<source>, ammette versioni alternative dell’oggetto (con  \<object> annidati); il primo formato riconosciuto e supportato è quello mandato in playout dal browser
        - Può essere utilizzato anche per includere un documento HTML in un HTML
    - \<embed/>: definisce un oggetto incluso in un documento HTML, come \<object>, ma non ammette alternative
        - Può essere utilizzato anche per includere un documento HTML in un HTML
    - \<canvas>: fornisce le API necessarie per la generazione e il rendering dinamico di grafica, diagrammi, immagini e animazioni tramite Javascript
        - La larghezza e l’altezza del canvas sono specificati tramite gli attributi  width e height
        - Le coordinate (0,0) corrispondono all’angolo in alto a sinistra
        - Gli oggetti non sono disegnati direttamente sul canvas ma all’interno del contesto, recuperato tramite un metodo Javascript dell’elemento \<canvas> chiamato `getContext()`:
            - Questo metodo è parte di una vasta libreria utile per disegnare figure, colorarle, trasformarle..
            - Altra tecnologia per la grafica vettoriale e l’animazione prevista da HTML5 è quella basata su SVG

### Parte 5 (CSS)

- CSS (Cascading Style Sheets): serve per definire come il contenuto deve essere presentato

- Prevista ed incoraggiata la presenza di fogli di stile multipli, che agiscono uno dopo l'altro, in cascata

- Modi d'uso
    - Posizionato presso il tag di riferimento (foglio di stile inline, attraverso l’attributo style)
        - Esempio: `<header style="color:blue;">`
    - Posizionato nel tag \<style> (foglio di stile interno, nell'header del documento)
        - Esempio
            ```html
            <style type="text/css">
                header { color: blue; }
            </style>
            ```
    - Importato dal tag \<style> (foglio di stile esterno importato, nell’header del documento)
        - Esempio
            ```html
            <style type="text/css">
                @import url(style.css);
            </style>
            ```
    - Indicato dal tag \<link> (foglio di stile esterno, nell’header del documento, BEST)
        - Esempio
            ```html
            <link type ="text/css" rel="stylesheet" href="style.css">
            ```

- Sintassi: Selettore { Proprietà: Valore;}
    - Selettore: consente di specificare un elemento o un insieme di elementi dell’albero HTML al fine di associarvi delle caratteristiche
        - Universale (*): fa match con qualsiasi elemento
        - Di tipo (E): fa match con gli elementi E
            - Esempio: `body{ font-family: Arial; font-size: 12 pt; }`
        - Di prossimità
            - E F: Elementi F discendenti di elementi di E
                - Esempio: `section p { font-size: 10 pt; }`
            - E>F: Elementi F figli diretti di elementi di E
                - Esempio: `p>strong { color: red; }`
            - E + F: Elementi F immediatamente seguenti a elementi di E
            - E ~ F: Elementi F fratelli successori di elementi di E
        - Di attributi (E[foo], E[foo="bar"], E[foo~="bar"], E[foo^="bar"]): fanno match con gli elementi E che possiedono l'attributo specificato o che ha un valore particolare
        - Di classe
            - E.bar: è equivalente a E[class="bar"]
                - Esempio: `h1.spiegazione { font-size: 24 px; }`
            - E#bar: identifica gli elementi il cui attributo di tipo id vale "bar"
                - Esempio: `p#note1 { font-size: 9 px; }`
        - Di pseudo-classi
            - E:link, E:visited: vero se l'elemento E è un link non ancora visitato o un link già visitato
            - E:active, E:hover, E:focus: vero se sull'elemento E passa sopra il mouse, il mouse è premuto o il controllo è selezionato per accettare input
            - E:enabled, E:checked: vero se elemento E è abilitato o "checked"
            - E:lang(c): vero se l'elemento ha selezionata la lingua c
        - Di pseudo-classi strutturali
            - E:first-child: elemento E che è il primo figlio di suo padre
            - E:nth-child(n): elemento E che è l’n-esimo figlio di suo padre
            - E:nth-last-child(n): elemento E che è l’n-esimo figlio di suo padre a partire dall’ultimo
            - E:first-of-type: elemento E che è il primo figlio di suo padre di quel tipo
            - E:nth-of-type(n): elemento E che è l’n-esimo figlio di suo padre di quel tipo
            - E:only-of-type: elemento E che è l’unico figlio di suo padre di quel tipo
            - E:empty: elemento E che è vuoto
        - Di pseudo-elementi
            - E:before, E:after: vero prima e dopo il contenuto dell'elemento E
            - E:first-line: vero per la prima riga dell'elemento E
            - E:first-letter: vero per la prima lettera di un elemento
        - `,` (raggruppamento di selettori): selettori diversi possono usare lo stesso blocco se separati da virgola
    - Proprietà: caratteristica di stile assegnabile ad un elemento
    - Valore: red, 60%, 100px...
        - Unità di misura
            - Relative
                - em: relativa alla dimensione del font in uso (es: se il font ha corpo 12pt, 1em varrà 12pt, 2em varranno 24pt, ...)
                - px: relativi al dispositivo di output e all'impostazione dell’utente
            - Assolute
                - in: pollici (1in = 2.54cm)
                - cm: centimetri
                - mm: millimetri
                - pt: punti tipografici (1/72 di pollice)
        - Percentuali: percentuale del valore che assume la proprietà stessa nell’elemento padre
        - URL
        - Stringhe
        - Colori (RGB, o keyword)
    - Esercizi: https://flukeout.github.io/

- Conflitti di stile: possono nascere dei conflitti, ovvero ad uno stesso elemento sono applicate delle regole i cui valori sono in conflitto
    - Ordinamento regole
        1. Media
        2. Importanza di una dichiarazione
            - È possibile aggiungere ad una dichiarazione la keyword !important (sconsigliato)
        3. Origine della dichiarazione
            1. Author: l'autore delle pagine fornisce i fogli di stile del documento specifico
            2. User: l'utente può fornire un ulteriore foglio di stile per indicare regole di proprio piacimento
            3. User Agent: il browser definisce le regole di default per gli elementi dei documenti
        4. Specificità del selettore
            - Data da una quadrupla xywz
                - x: 1 se la dichiarazione è nell’attributo style, 0 altrimenti
                - y: numero di id specificati nel selettore
                - w: numero di classi, attributi e pseudo-classi specificati nel selettore
                - z: numero di elementi e di pseudo-elementi specificati nel selettore
        5. Ordine delle dichiarazioni

- Box model
    - Ogni elemento è definito da una scatola (box) all'interno della quale si trova il contenuto
    
    ![alt text](./res/box-model.jpeg "Boxes")

    - Contenuto
        - È possibile definire
            - width
            - height
            - min-height
            - max-height
        - Solitamente si specifica SOLO la larghezza e NON la l’altezza; in questo modo l’altezza di un elemento viene determinata dal suo contenuto
        - Se il contenuto non fitta l'altezza e la larghezza impostate questa situazione va gestita con la proprietà overflow
            - visible: il contenuto eccedente viene mostrato
            - hidden: il contenuto eccedente viene nascosto
            - scroll: vengono mostrare le barre di scorrimento per visualizzare il contenuto eccedente
            - auto: il contenuto eccedente viene mostrato in base alle impostazioni del browser
    - Margin: permette di impostare lo spazio tra un elemento e gli altri elementi della pagina
        - Esempio
            ```css
            p{margin: 5px 7px 8px 10px} /*top right bottom left*/
            p{margin: 5px 7px 6px } /*top right-left bottom*/
            p{margin: 5px 10%} /*top-bottom right-left*/
            p{margin: 5px } /*all*/

            /* Oppure uso le proprietà: margin-top, margin-right, margin-bottom, e margin-left*/
            ```
        - Nel caso in cui due elementi siano allineati orizzontalmente, la distanza tra i due è data dalla somma dei due margini 
        - Nel caso in cui due elementi siano allineati verticalmente, si ha il cosiddetto margin collpasing: la distanza tra i due è data dal valore massimo fra il margine inferiore del primo elemento e quello superiore del secondo
    - Padding: permette di impostare lo spazio fra il contenuto e il bordo
        - A livello di sintassi è più o meno equivalente a margin
    - Border: permette di impostare lo spessore, lo stile e il colore di ognuno dei quattro bordi
        - Proprietà singole (position può essere top, right, bottom, left)
            - border-position-width
                - Valore
                    - Numerico
                    - Keyword
            - border-position-style
                - Valore
                    - none o hidden: nessun bordo
                    - solid: intero
                    - dotted: a puntini
                    - dashed: a trattini
                    - double: doppio
                    - groove, ridge, inset, outset: effetti tridimensionali
            - border-position-color
        - Proprietà sintetiche
            - border-top, border-right, border-bottom, border-left
            - border-width, border-style, border-color
            - border
    - Dimensioni
        - Larghezza complessiva: margin-left + border-left-width + padding-left + width + padding-right + border-right-width + margin-right
        - Altezza complessiva: uguale ma tengo conto del margin collapsing
    - Posizionamento
        - Comportamento elementi di blocco (h1, h2, p, div)
            - Larghezza: se non specificata occupano il 100% di quella del padre; è possibile specificare un valore con la proprietà width
            - Altezza:  dipende dal contenuto dell’elemento; è possibile specificare un valore con la proprietà height
            - Gli elementi sono disposti verticalmente
        - Comportamento elementi di linea (a, strong, em, span)
            - Larghezza: dipende dal contenuto dell’elemento; non è possibile specificare un valore con la proprietà width
            - Altezza: dipende dal contenuto dell’elemento; non è possibile specificare un valore con la proprietà height
            - È possibile specificare l’altezza della linea con la proprietà line-height
            - Gli elementi adiacenti sono disposti orizzontalmente
    - Display: determina il tipo di elemento (e il relativo comportamento)
        - Valori
            - inline
            - block
            - none: l'elemento non viene visualizzato
            - inline-block: l’elemento può assumere dimensioni esplicite (come gli elementi blocco), ma si disporrà orizzontalmente (come gli elementi inline) e non verticalmente
            - list-item: per fare in modo che un elemento si comporti come un li
            - grid: trasforma un elemento in un gridcontainer
            - flex: trasforma un elemento in un flexcontainer
        - Valori per trasformare elementi in parti di una tabella (table, inline-table, table-cell, table-row, table-row-group, table-column, table-column-group, table-header-group, table-footer-group, table-caption)
        - Layout multi colonna liquido: layout in cui la grandezza della pagina dipende dalla finestra del browser, adattandosi a tutte le risoluzioni
            - Le colonne devono essere elementi ibridi inline-block
            - Gli elementi di linea sono solitamente allineati in basso; se le colonne sono di altezze diverse è necessario specificare un allineamento a partire dall'alto usando la proprietà vertical-align con il valore top
            - Le tre colonne devono occupare in totale al massimo 100% tra width, margin e padding, altrimenti l’ultima andrà a capo
            - NON devono esserci spazi nel codice html tra una sezione e l’altra, altrimenti l’ultima colonna andrà a capo
            - E' necessario usare la proprietà box-sizing con valore border-box per fare in modo che la grandezza del bordo (E DEL PADDING!) sia inclusa nella larghezza
    - Float: consente di estrarre un elemento dal normale flusso del documento e lo sposta su un lato rispetto al suo contenitore (gli elementi appartenenti al normale flusso del documento circonderanno gli elementi floating)
        - Layout multi colonna liquido
            - Le colonne laterali devono essere float, quella centrale no
            - La colonna centrale deve avere dei margini laterali almeno delle dimensioni delle colonne laterali
            - Le colonne devono occupare in totale al massimo 100%, altrimenti ci saranno delle sovrapposizioni
        - Clear: serve a disattivare l’effetto della proprietà float sugli elementi che lo seguono, ovvero a impedire che al fianco di un elemento floatingcompaiano altri elementi
            - Valori
                - none: float consentito su entrambi i lati
                - left: impedisce il posizionamento a sinistra
                - right: impedisce il posizionamento a destra
                - both: impedisce il posizionamento su entrambi i lati
    - Position: consente di specificare il posizionamento dell'elemento rispetto al flusso del documento
        - Valori
            - static: default, l’elemento è disposto secondo il normale flusso del documento
            - fixed: usando questo valore il box dell'elemento viene sottratto al normale flusso del documento; non scorre con il resto del documento ma rimane fisso
            - relative (da evitare): l’elemento NON viene rimosso dal flusso del documento, a partire dalla posizione che avrebbe occupato, è possibile specificare lo spostamento con le proprietà top, right, bottom e left (anche valori negativi)
            - absolute (da evitare): l'elemento viene rimosso dal flusso del documento; il posizionamento avviene rispetto al primo elemento antenato che ha un posizionamento diverso da static ed è  specificato sempre attraverso top, right, bottom e left
        - In caso di elementi sovrapposti, è possibile gestire quale elemento deve essere visualizzato sopra con la proprietà z-index; verrà visualizzato l’elemento con z-index maggiore (funziona solo con elementi che non hanno static come posizione)

- Ereditarietà
    - Lo stile può essere applicato
        - Direttamente: con l’attributo style o con regole
        - Indirettamente: l’elemento eredita lo stile dal padre
    - Non tutte le proprietà sono soggette ad ereditarietà (display, background, proprietà relative al box model...)
    - È possibile forzare l’ereditarietà usando come valore inherit

### Parte 6 (HTML)

- Commenti 
    - \<!--Questo è un commento. I commenti non sono visualizzati dal browser -->

- Ancore
    ```html
    <nav>
        <ul>
            <li> <a href="/">Home</a> </li>
            <li> <a href="/news">News</a> </li>
            <li> <a href="http://www.google.it">Google</a> </li>
            <li> <a href="#articolo">Articolo</a> </li>
        </ul>
    </nav>
    
    <article id="articolo">
        <p> testo dell'articolo .... </p>
    </article>
    ```

- Risorsa: è qualunque struttura che sia oggetto di scambio tra applicazioni all’interno del Web; il concetto di risorsa è indipendente dal meccanismo di memorizzazioneeffettiva o dal tipo di contenuto

- URI (Uniform Resource Identifier): sono una sintassi usata in WWW per definire i nomi e gli indirizzi di oggetti (risorse) su Internet (NB: URI = URL + URN)
    - Componenti
        - URL (Uniform Resource Locator): una sintassi che contiene informazioni immediatamente utilizzabili per accedere alla risorsa (ad esempio, il suo indirizzo di rete)
        - URN (Uniform Resource Names): una sintassi che permetta una etichettatura permanente e non ripudiabile della risorsa, indipendentemente dal riportare informazioni sull'accesso
    - Sintassi: `schema :[//authority] path[?query] [#fragment]`, fra quadre gli opzionali
        - Schema: é identificato da una stringa arbitraria (ma registrata presso IANA, [qui](https://www.iana.org/assignments/uri-schemes/uri-schemes.xhtml) la lista di tutti gli URI) usata come prefisso
        - Authority: `[userinfo@] host[: port]` individua un’organizzazione gerarchica dello spazio dei nomi a cui sono delegati i nomi
            - Userinfo: non deve essere presente se lo schema non prevede identificazione personale
            - Host: nome di dominio o un indirizzo IP
            - Port: può essere omessa se ci si riferisce ad una well-knownport
        - Path: identificativa della risorsa all’interno dello spazio di nomi identificato dallo schema e (se esistente) dalla authority (è la path normale)
        - Query: individua un’ulteriore specificazione della risorsa all’interno dello spazio di nomi identificato dallo schema, è compresa fra "?" e "#" (Es: `nome1=valore1&nome2=valore+in+molte+parole`)
        - Fragment: individua una risorsa secondaria (una risorsa associata, dipendente o in molti casi un frammento) della risorsa primaria; è tutta la parte che sta dopo a “#”
    - Caratteri ammessi
        - Riservati: sono caratteri che hanno delle funzioni particolari in uno o più schemi di URI (`.;/?:@&=+$,..`)
            - Escape: serve per l’utilizzo di caratteri particolari nell’URI, precedendone il codice esadecimale (`%`)
            - `/ . ..`: usati nella path
            - `#`: delimita il fragment
            - `?`: delimitatore query
            - `+`: al posto dello spazio nella query
        - Non riservati: sono alfanumerici e alcuni caratteri di punteggiatura privi di ambiguità (uppercase | lowercase | digit | `-_!~*'()`)
    - Tipologie
        - Assoluto: contiene tutte le parti predefinite dal suo schema, esplicitamente precisate
        - Relativo (reference): riportare solo una parte dell'URI assoluto corrispondente, tagliando progressivamente cose da sinistra (ha sempre un URI di base)

- Flow
    - Tabelle
        - Sono realizzate attraverso l’elemento \<table>
        - Sono organizzate per righe, realizzate attraverso l’elemento \<tr>, tablerow
        - Ciascuna riga è poi divisa in celle (che possono occupare più righe o colonne mediante gli attributi rowspan e colspan)
            - Normali: \<td>, table data
            - Intestazione: \<th>, table header
        - \<caption>: viene utilizzato per inserire una caption
        - Esempio
            <table>
                <caption>Monthly savings</caption>
                <tr>
                    <th>Month</th> <th>Savings</th>
                </tr>
                <tr>
                    <td>January</td> <td>$100</td>
                </tr>
                <tr>
                    <td>February</td> <td>$80</td>
                </tr>
                <tr>
                    <td colspan="2">Sum: $180</td>
                </tr>
            </table>

            ```html
            <table>
                <caption>Monthly savings</caption>
                <tr>
                    <th>Month</th> <th>Savings</th>
                </tr>
                <tr>
                    <td>January</td> <td>$100</td>
                </tr>
                <tr>
                    <td>February</td> <td>$80</td>
                </tr>
                <tr>
                    <td colspan="2">Sum: $180</td>
                </tr>
            </table>
            ```
        - Accessibilità
            - Una cella di tipo \<td> o \<th>può fare riferimento (tramite l’attributo headers), ad altre celle, per specificare che queste rappresentano una intestazione della cella corrente
                - Headers deve avere come valore la lista degli id delle intestazioni per la cella SEPARATI DA SPAZIO
            - Esempio
                <table border=1>
                    <tr>
                        <th>Nome</th> <th>mail</th> <th>Telefono</th>
                    </tr>
                    <tr>
                        <th> Paola Salomoni </th> <td> <a href="mailto:paola.salomoni@unibo.it">paola.salomoni@unibo.it</a></td> <td> +39 051 20 99231 </td>
                    </tr>
                    <tr>
                        <th> Silvia Mirri</th> <td> <a href="mailto:silvia.mirri@unibo.it">silvia.mirri@unibo.it</a></td> <td rowspan="3“ headers=“smgdlm"> +39 0547 3 38813 </td>
                    </tr>
                    <tr>
                        <th> Giovanni Delnevo</th> <td> <a href="mailto:giovanni.delnevo2@unibo.it"> giovanni.delnevo2@unibo.it</a></td>
                    </tr>
                    <tr>
                        <th> Lorenzo Monti</th> <td> <ahref="mailto:lorenzo.monti20@unibo.it">      lorenzo.monti20@unibo.it</a></td>
                    </tr>
                </table>

                ```html
                <table border=1>
                    <tr>
                        <th>Nome</th> <th>mail</th> <th>Telefono</th>
                    </tr>
                    <tr>
                        <th> Paola Salomoni </th> <td> <a href="mailto:paola.salomoni@unibo.it">paola.salomoni@unibo.it</a></td> <td> +39 051 20 99231 </td>
                    </tr>
                    <tr>
                        <th> Silvia Mirri</th> <td> <a href="mailto:silvia.mirri@unibo.it">silvia.mirri@unibo.it</a></td> <td rowspan="3“ headers=“smgdlm"> +39 0547 3 38813 </td>
                    </tr>
                    <tr>
                        <th> Giovanni Delnevo</th> <td> <a href="mailto:giovanni.delnevo2@unibo.it"> giovanni.delnevo2@unibo.it</a></td>
                    </tr>
                    <tr>
                        <th> Lorenzo Monti</th> <td> <ahref="mailto:lorenzo.monti20@unibo.it">      lorenzo.monti20@unibo.it</a></td>
                    </tr>
                </table>
                ```
    - Liste (possono essere annidate)
        - Item: \<li>
        - Tipi
            - Non ordinate (\<ul>)
            - Ordinate (\<ol>)
                - Attributi
                    - start: valore iniziale
                    - type: tipo di numerazione
                    - reversed: numerazione inversa
            - Definizioni, associative (\<dl>)
                - Gli item sono 
                    - \<dt>: termini descrittivi
                    - \<dd>: una o più definizioni
                - Esempio
                <dl>
                    <dt>Coffee</dt>
                        <dd>Black hot drink</dd>
                    <dt>Milk</dt>
                        <dd>White colddrink</dd>
                </dl>

                ```html
                <dl>
                    <dt>Coffee</dt>
                        <dd>Black hot drink</dd>
                    <dt>Milk</dt>
                        <dd>White colddrink</dd>
                </dl>
                ```
            
### Parte 7 (CSS)

- Colori
    - Specificazione
        - Keyword: red, blue..
        - Esadecimale: RGB, #RRGGBB
        - Decimale: rgb(val, val, val) 0 < val < 255
            - Trasparenza: rgba(val, val, val, opa) 0 < opa < 1
        - HSL (Hue, Saturation e Lightness): hsl(h, s, l)
            - Trasparenza: hsla(h, s, l, a)
    - opacity: proprietà che può essere usata in combinazione con un colore definito in rgb (con uno qualsiasi dei metodi precedenti) e che gestisce la trasparenza sia dello sfondo che del testo di un elemento

- Sfondo
    - Proprietà
        - background-color: permette di specificare il colore di sfondo
        - background-image: permette di specificare l’url di un immagine di sfondo
        - background-repeat: permette di specificare se e come l’immagine deve essere ripetuta (repeat, repeat-x, repeat-y, no-repeat)
        - background-attachment: permette di specificare il meccanismo di scrolling (scroll o fixed)
        - background-position: permette di specificare la posizione dell’immagine (accetta due valori: posizione orizzontale e verticale e si specificano in lunghezza, percentuale o con una keyword; top, bottom, right, left e center)
        - background-size: permette di specificare la dimensione dell’immagine di sfondo (Es: background-size: width height)
        - background-origin: permette di posizionare l’immagine di sfondo nel content-box, nel padding-box oppure nel border-box
        - E' possibile dichiarare più immagini come sfondo, il risultato è la  sovrapposizione di tutte le immagini

- Gradienti
    - linear-gradient: permette di specificare un gradiente lineare come sfondo (linear-gradient(direction, color-stop1, color-stop2, ...))
    - repeating-linear-gradient: è possibile impostare la ripetizione del gradiente linearelungo l’elemento per il quale si imposta lo sfondo (repeating-linear-gradient(direction, color-stop1, color-stop2 dimension, ...))
    - radial-gradient: permette di specificare un gradiente radiale come sfondo (radial-gradient(shape size at position, start-color, ..., last-color))
    - repeating-radial-gradient: è possibile impostare la ripetizione del gradiente radiale lungo l’elemento per il quale si imposta lo sfondo (repeating-radial-gradient(shape size at position, start-color dimension, ..., last-color dimension))

- [Comunicazione di servizio]: da qui in poi sarò più stringato, non frega a nessuno del CSS e dell'HTML, se vuoi sapere come si usa un tag lo cerchi su iternet 

- Bordi
    - border-image: permette di specificare una immagine che viene usata come bordo
    - border-radius: permette di specificare bordi arrotondati

- Regole specifiche browser
    - -msie: Trident (usato da IE)
    - -moz: Gecko (usato da Firefox)
    - -o: Presto (usato da Opera fino al 2013)
    - -webkit: WebKit (usato da Chrome e Safari)

- Box
    - box-shadow: permette di specificare l’ombra del box
    - resize: permette all’utente di ridimensionare i box
    - box-sizing: permette di far rientrare le dimensioni di padding e bordi nel computo di widthe height (border-box)

- Caratteri
    - font-family: specifica il nome di uno o più font
    - font-style: specifica lo stile: normal, oblique, italic
    - font-variant: applica l'effetto maiuscoletto (small-caps)
    - font-weight: specifica il peso (100-900, parole chiave)
    - font-size: specifica la dimensione dei caratteri (pixel, punti, em, ex, percentuale o keyword)

- Formattazione del testo
    - color: colore del testo
    - letter-spacing: normal o valore in pixel
    - line-height: interline a espresso in lunghezza o percentuale
    - text-align: left, right, center o justify
    - text-decoration: none, underline, overlineo line-through
    - text-direction: da destra a sinistra (rtl) o viceversa (ltr)
    - text-indent: indentazione della prima riga di testo, espressa come lunghezza o in percentuale
    - text-overflow: permette di specificare il comportamento nel caso in cui porzioni di testo fuoriescano dal box che lo contiene
    - text-shadow: permette di specificare l’ombreggiatura di un testo come h-shadow v-shadow blur-radius color
    - text-transform: none, capitalize, uppercase, lowercase
    - white-space: specifica come sono gestiti spazi bianchi e andate a capo
    - word-wrap: permette di forzare l’andata a capo per le parole molto lunghe che non rispettano i bordi dell’elemento contenitore
    - word-spacing: normal o valore in pixel
    - vertical-align: allineamento degli elementi inline

- Liste
    - list-style-position: specifica la posizione del marker, se dentro al testo (inside) o fuori (outside)
    - list-style-image: specifica un’immagine come marker
    - list-style-type: specifica il tipo di marker

- Filtri
    - blur(px): consente di applicare una sfocatura
    - brightness(%): consente di regolare la luminosità
    - contrast(%): consente di regolare il contrasto
    - drop-shadow(hsvs b s c): consente di specificare un’ombreggiatura
    - grayscale(%): converte l’immagine in bianco e nero
    - hue-rotate(deg): applica una rotazione di deg gradi della tonalità rispetto al cerchio cromatico
    - invert(%): inverte i colori dell’immagine
    - opacity(%): consente di regolare il livello di opacità dell’immagine
    - saturate(%): consente di regolare la saturazione dell’immagine
    - sepia(%): converte l’immagine in seppia
    - url(): indica l’url di un file XML con un filtro SVG da applicare all’immagine

- At rules: regole precedute da una @ e servono per specificare determinati comportamenti
    - Regular: @[KEYWORD] (RULE);
    - Nested: @[KEYWORD]{ regole css }

- Transizioni: effetti che permettono di applicare passaggi graduali da uno stile all’altro per un determinato elemento (più transizioni per elemento con virgola)
    - transition-property: proprietà che viene modificata (richiesta)
    - transition-duration:  durata della transizione (richiesta)
    - transition-timing-funtion: velocità di esecuzione della transizione–
    - transition-delay: indica quando la transizione inizia

- Animazioni: con la regola @keyframe è possibile definire delle animazioni, che coinvolgono una o più proprietà
    ```css
    @keyframes nome{
        selettoreKeyFrame{ ...} //Selettore = percentuale
    }
    ```
    - animation-name: nome dell’animazione
    - animation-duration:  durata dell’animazione
    - animation-timing-funtion:  velocità di esecuzione dell’animazione
    - animation-delay: indica quando l’animazione inizia
    - animation-iteration-count: indica quante volte deve essere ripetuta l’animazione
    - animation-direction: indica se l’animazione deve essere eseguita al contrario oppure no
    - animation-play-state: indica se e quando l’animazione deve essere eseguita oppure deve essere messa in pausa
    - animation-fill-mode: indica lo stato finale dell’elemento animato, una volta terminata l’animazione

- Media query: permettono di applicare (o meno) delle regole CSS in base al tipo e alle caratteristiche del dispositivo su cui si visualizza la pagina web
    - Sintassi: `@media not|only mediatype and (mediafeature and|or|not mediafeature) { codice css}`
    - Valori
        - All: indica tutti i media type per tutti i tipi di dispositivi, default
        - Print: serve per specificare le stampanti
        - Screen: serve per specificare uno schermo generico 
        - Speech: serve per specificare gli screen reader, dispositivi che utilizzano la sintesi vocale per «leggere» il contenuto della pagina
    - Più usate
        - width: indica la larghezza della finestra del browser (accetta i prefissi min e max) 
        - orientation: indica l’orientamento del dispositivo, landscape o portrait
    - Esempi
        - `@media print{ }`
        - `@media screen and (min-width: 480px){ }`
        - `@media only screen and (orientation: landscape){ }`
    - Meta tag viewport
        - Sui telefoni ci sono dei problemi, il viewport fa casino, quindi lo settiamo alla dimensione dello schermo del device e settiamo anche lo zoom iniziale: `<metaname="viewport" content="width=device-width, initial-scale=1.0">` 

- Evitare scrolling orizzontale
    - Non usare elementi con larghezza prefissata, soprattutto se di grandi dimensioni; ad esempio, se una immagine è mostrata ad una larghezza maggiore rispetto a quella del viewport, allora ci sarà scrolling orizzontale
    - Non basarsi solo sulla larghezza di un unico viewport
    - Usare le media query per offrire layout adeguati e adatti a display di diverse dimensioni
    - Accertarsi che la somma di spazio occupata da elementi inline (o inline-block) non sia mai superiore al 100%, facendo attenzione anche agli spazi bianchi e alle andate a capo nel codice HTML

### Parte 8 (HTML)

- Interactive
    - \<form>: è una parte della pagina Web che contiene controlli di input (form control), come per esempio campi di testo, bottoni e checkbox
        - La form può essere processata lato client, lato server o entrambe le cose
        - Attributi
            - Principali
                - action: specifica l’URL dell’applicazione server‐side che riceverà i dati
                - method: specifica il metodo HTTP che deve essere usato per i dati
                    - GET: richiede di processare i dati a una specifica applicazione server
                        - Esempio: `https://www.google.com/search?q=tecnologie+web`
                    - POST: sottomette (invia) i dati da processare ad una specifica applicazione server
                        - I dati sono inviati nel body del messaggio HTTP
            - Secondari
                - name: specifica il nome del controllo
                - required: è un attributo booleano dei controlli che indica che è obbligatorio inserire un valore per quel campo
                - autofocus: mette il focus su questo controllo
                - autocomplete: è un attributo booleano di input che consente di attivare l'auto‐completamento
            - Tag
                - \<label>: descrive il controllo all'utente (fondamentale per accessibilità)
                    - Esempi
                        ```html
                        <form>
                            <p><label>Customer name: <input ....../></label></p>
                        </form>

                        <form>
                            <p><label for="CN">Customer name: </label><input .... id="CN" /></p>
                        </form>
                        ```
                - \<fieldset>: raggruppa più controlli che hanno semantica comune
                    - Esempio
                        ```html
                        <form>
                            <fieldset> <legend>dati personali:</legend>
                                <label>Nome:
                                <input type="text" name="nome"></label><br/>
                                <label>Email:
                                <input type="email" name="mail"> </label ><br/>
                                <label>Data di nascita: 
                                <input type="date" name="date"></label >
                            </fieldset><br/>
                            <input type="submit">
                        </form>
                        ```
                - \<input>: consente di inserire nella pagina molti tipi diversi di controllo 
                    - Il tipo è specificato mediante l’attributo type che può assumere i valori hidden, text, search, tel, url, email, password, date, time, number, range, color, checkbox, radio, file, submit, image, reset, button
                        - Esempi
                            ```html
                            <!--reset, submit e button-->
                            </form>
                                <input type="button" value="Bottone" onclick="..."/>
                                <input type="reset"/>
                                <input type="submit" value="Invia"/>
                            </form>

                            <!--image-->
                            <form action="demo_form.asp">
                                <label>nome: <input type="text" name="fname"/></label><br/>
                                <input type="image" src="img_submit.gif" alt="Submit" width="48" height="48" />
                            </form>

                            <!--hidden-->
                            <form action="...">
                                <input type="hidden" name="country" value="Italy" />
                                <input type="submit" />
                            </form>

                            <!--password-->
                            <form action="...">
                                <label>Password: <input type="password" name="pwd" maxlength="8" /></label>
                                <br/><br/>
                                <input type="submit"/>
                            </form>

                            <!--radio-->
                            <form action="demo_form.asp">
                                <label>
                                    <input type="radio" name="gender" value="male"/>Male</label> <br/>
                                <label>
                                    <input type="radio" name="gender" value="female"/>Female</label><br/>
                                <label>
                                    <input type="radio" name="gender" value="other"/>Other</label> <br/>
                                    
                                <input type="submit" value="Submit"/>
                            </form>

                            <!--checkbox-->
                            <form action="demo_form.asp">
                                <label><input type="checkbox" name="vehicle1"value="Bike"/>I havea bike</label><br/>
                                <label><input type="checkbox" name="vehicle2"value="Car"/> I have a car</label><br/>
                                <label><input type="checkbox" name="vehicle3"value="Boat"/> I have a boat</label><br/>
                                <input type="submit" value="Submit"/>
                            </form>

                            <!--text-->
                            <form action="demo_form.asp">
                                <label>First name: 
                                    <input type="text" name="fname"/>
                                </label><br/>
                                <label>Last name: 
                                    <input type="text" name="lname"/>
                                </label><br/>
                                <input type="submit"/>
                            </form>

                            <!--search-->
                            <form action="demo_form.asp">
                                <label>Search Google: 
                                    <input type="search" name="googlesearch"/>
                                </label><br/>
                                <input type="submit"/>
                            </form>

                            <!--color-->
                            <form action="demo_form.asp">
                                <label>Select your favorite color: 
                                    <input type="color" name="favcolor"/>
                                <label><br/>
                                <input type="submit">
                            </form>

                            <!--Data e ora-->
                            <form action="demo_form.asp">
                                <label>time: <input type="time" name="usr_time"/></label><br/><br/>
                                <label>week: <input type="week" name="week_year"></label><br/><br/>
                                <label>date: <input type="date"name="day"/></label><br/><br/>
                                <label>date and time: <input type="datetime" name="dt"/></label> <br/><br/>
                                <label>local date and time: <input type="datetime-local" name="ldt"/></label><br/><br/>
                                <input type="submit">
                            </form>

                            <!--tel, url, email-->
                            <form action="demo_form.asp">
                                <p>Inserisci i tuoi dati </p>
                                <label>e-mail: <input type="email" name="usremail"/></label><br/><br/>
                                <label>telefono: <input type="tel"  name="usrtel"/></label><br/><br/>
                                <label>homepage: <input type="url" name="homepage"/></label><br/><br/>
                                <input type="submit">
                            </form>

                            <!--number e range-->
                            <form action="demo_form.asp">
                                <label>range (0-10)
                                    <input type="range" name="points" min="0" max="10"/></label><br/><br/>
                                <label>numero (1-5): <input type="number" name="quantity" min="1" max="5"/></label><br/><br/>
                                <input type="submit"/>
                            </form>

                            <!--file-->
                            <form action="demo_form.asp">
                                <label>Selezionare file: <input type="file" name="file1"/></label><br/><br/>
                                <input type="submit"/>
                            </form>
                            ```
                - \<textarea>
                    ```html
                    <form>
                        <label>testo libero <br/><textarea rows="4" cols="50">il testo inserito tra inizio e fine elemento è il valore di default della textarea</textarea></label><br/><br/>
                        <input type="submit" value="Submit"/>
                    </form>
                    ```
                - \<select> e \<option>
                    ```html
                    <!--(ci sono anche attributi)-->
                    <form>
                        <label> scegli un insegnamento<br/>
                            <select name="insegnamento">
                                <option value="SO">Sistemi Operativi</option>
                                <option value="TW">Tecnologie Web</option>
                                <option value="SM">Sistemi Multimediali</option>
                            </select>
                        </label>
                    </form>

                    <!--<optgroup>-->
                    <form>
                        <label> scegli un insegnamento<br/>
                            <select name="insegnamento">
                                <optgroup label="2014/15"> 
                                    <option value="SO">Sistemi Operativi</option>
                                    <option value="SM">Sistemi Multimediali</option>
                                </optgroup>
                                <optgroup label="2015/16">
                                    <option value="TW">Tecnologie Web</option>
                                    <option value="SM">Sistemi Multimediali</option>
                                </optgroup>
                            </select>
                        </label>
                    </form>

                    <!--<datali<!--<output>-->
                            <form oninput="x.value=parseInt(a.value)"><label for="a">slider: <br/>0<input type="range" id="a" value="50">100 <br/>il valore dello sliderè: <output name="x" for="a"></output></form>st>-->
                    <form>
                        <label> scegli un insegnamento                            <input list="corsi" name="insegnamento"/></label>
                        <datalist id="corsi">
                            <option value="SO">Sistemi Operativi</option>
                            <option value="TW">Tecnologie Web</option>
                            <option value="SM">Sistemi Multimediali</option>
                        </datalist><br/><br/>
                        <input type="submit"/>
                    </form>
                    ```
                - \<output>
                    ```html
                    <form oninput="x.value=parseInt(a.value)">
                        <label for="a">slider: <br/>0<input type="range" id="a" value="50">100 <br/>il valore dello sliderè: <output name="x" for="a"></output>
                    </form>
                    ```

### Parte 9

- Bootstrap: free and open-source front-end web framework for designing websites and web applications (mobile-first)
    - Gli elementi di Bootstrap si dividono principalmente in 3 categorie
        - Layout: componenti ed opzioni legati agli aspetti di gestione dell’impaginazione
        - Content: componenti ed opzioni legati ai contenuti come testo, immagini, ecc...
        - Components: insieme predefinito di componenti spesso utilizzati nelle pagine web
    - Contro
        - Pesante
        - L’aspetto di presentazione non è più separato dal codice HTML
        - Il look and feeldei siti è molto simile

# Parte 10

- JavaScript (standard: ECMAScript): linguaggio di scripting interpretato dal browser (Java != JavaScript)
    - Weakly typed: non è necessario definire il tipo di una variabile
    - Prototype-based: esiste il concetto di oggetto ma non quello di classe
        - Ogni oggetto è creato direttamente, senza bisogno di definire prima una classe
        - E' possibile definire un prototipo con il quale un oggetto prende a modello un altro oggetto condividendone le caratteristiche
        - E' possibile specificare le caratteristiche di un oggetto anche aggiungendo proprietà e metodi a a runtime
        - Ogni oggetto è autonomo e si possono aggiungere tutti i metodi/proprietà che si vuole senza modificare gli altri
            - Per aggiungere proprietà/metodi a molti oggetti si deve usare l'oggetto prototype
            ```javascript
            // costruttore oggetto Persona
            function Persona(nome, cognome) {
                this.firstName = nome;
                this.lastName = cognome;
            }
            
            var paola = new Persona("Paola", "Salomoni");
            Persona.prototype.welcome = function(){
                alert("Benvenuto, " + this.firstName + "!");
            }   // Aggiunta di una funzione al prototipo Persona
            
            paola.welcome();  // Utilizzo della nuova funzione
            ```
    - Using
        - Per  inserire codice JavaScript in un documento HTML esistono tre modi
            - \<script>: `<script type="text/javascript"> Codice JS </script>`
            - Riferimento a script contenuti in file .js esterni: `<script src="script_esterno.js"></script>`
            - Direttamente nel codice HTML in risposta ad eventi
                - `<input type=“button” onclick=“alert(‘Ciao!’)”...>`
                - `<a href=“javascript:nome_funzione()”> Clicca qui!</a>`
        - Sintassi
            - Case-sensitive
            - Le istruzioni sono terminate da `;` ma il terminatore può essere omesso se si va a capo
            - Commenti come in C
            - Gli identificatori possono contenere lettere, cifre e i caratteri `_` e `$` ma non possono iniziare con una cifra
            - If, switch, while e for come in C
            - Funzioni
                ```javascript
                function funzione1(par1, par2, ...) {
                    ...  
                    return v;
                }
                ```
            - Operatori relazionali
                - Uguali al C + 2 nuovi (`===` e `!==`, che hanno sempre lo stesso significato ma non applicano le type coercion) 
                - Nella valutazione di condizioni si considera falso non solo false, ma ogni valore falsy ovvero anche null, undefined, la stringa vuota (''), il valore 0 e NaN; ogni altro oggetto, inclusa la stringa 'false', è vero
                - Se i tipi dei due operandi sono diversi, == e !=  applicano type coercion
                    - 0 == '' true, perché sono entrambi falsy values
                    - 0 == '0' true, perché  '0'  è coercibile a 0
            - Variabili
                - `var nomevariabile;`
                - `var f = 15.8;`
                - Non hanno un tipo
                - Esiste lo scope globale e quello locale (ovvero dentro una funzione)
                - Dichiarazione
                    - Implicita: `pluto= 18;`, introduce variabili globali
                    - Esplicita: `var pippo = 19;` introduce variabili locali/globali
                - `for (var i = 1; i<10; i++)` (la i è globale, no scope di blocco)
                - `for (let i = 1; i<10; i++)` (la i è locale, let garantisce scope di blocco)
                    - Le variabili dichiarate con let NON sono accessibili dall’oggetto Window e non possono essere ridichiarate
                - Hoisting: una variabile Javascript
                    - Può essere usata prima di essere dichiarata
                    - Può essere dichiarata dopo essere stata usata
                - `const` consente di definire variabili che non possono essere riassegnate (è obbligatorio assegnare un valore in fase di dichiarazione)
                - Strict mode: non è possibile assegnare un valore ad una variabile se questa non è stata dichiarata (`use strict;` all'inizio dello script)
                - Tipi
                    - Numeri (`number`): sono rappresentati in formato floating point a 8 byte
                        - Non c’è distinzione fra interi e reali
                        - Esiste il valore speciale NaN e infinite
                    - Booleani (`boolean`)
                    - Array
                        - Non c'è il vincolo di omogeneità in tipo: le celle contengono oggetti
                        - `colori = new Array("rosso", "verde", "blu")`
                        - `varie = ["ciao", 13, Math.sin]`
                        - Si aggiungono poi nuovi elementi dinamicamente: `colori[3] = "giallo"`
                    - Stringhe
                        - `string` denota stringhe di caratteri Unicode
                        - Non esiste il tipo char: un carattere è una stringa lunga 1
                        - Ogni stringa è un oggetto IMMUTABILE dotato di proprietà, tra cui length, e di metodi, tra cui substring
                        - Le costanti stringa possono essere delimitate sia da virgolette sia da apici singoli, se occorre annidare virgolette e apici, occorre alternarli
                        - Si possono concatenarecon l'operatore +
            - Classi e oggetti (sono cose analoghe)
                ```javascript
                //Es 1:
                giuseppe = {
                    nome:'giuseppe', 
                    altezza:180,
                    nascita:newDate(1995,3,12),
                    salta: function() {
                        return'hop!'
                    } 
                }
                
                giuseppe.nome
                giuseppe['nome']
                giuseppe.salta()
                (giuseppe.salta)()
                giuseppe['salta']()

                //Es 2:
                function Persona(nome, altezza, nascita) {
                    this.nome = nome;
                    this.altezza = altezza;
                    this.nascita = nascita;
                    this.salta = function() {return"hop!";};
                }

                giuseppe = new Persona(‘giuseppe’, 180, new Date(1995,3,12));

                //Es 3:
                class Persona{
                    constructor(nome, altezza, nascita) {
                        this.nome = nome;
                        this.altezza = altezza;
                        this.nascita = nascita;
                    }
                    salta {
                        return ‘hop!’;
                    }
                }
                
                giuseppe= new Persona(‘giuseppe’, 180, new Date(1995,3,12));
                ```
                - Oggetti principali
                    - window: è l’oggetto top-level con le proprietà e i metodi della finestra principale
                        - Posizione: `moveBy(x,y)`, `moveTo(x,y)`...
                        - Dimensioni: `resizeBy(x,y)`, `resizeTo(x,y)`...
                        - Altre finestre: `open("URLname","Windowname",["opt"])`
                        - Tempo e intervalli: `setTimeout(function(),millisecs,["opt"])`
                    - navigator: è l’oggetto con le proprietà del client come nome, numero di versione, plug-in installati, supporto per i cookie...
                    - location: l’URL del documento corrente; modificando questa proprietà il client accede a un nuovo URL (redirect)
                        - `window.location = "http://www.unibo.it/";`
                    - history: l'array degli URL acceduti durante la navigazione
                        - Proprietà: `length`, `current`, `next`
                        - Metodi: `back()`, `forward()`, `go(int)`
                    - document: rappresenta il DOM ovvero il contenuto e la struttura del documento
                        - Elementi
                            - `window.document.title`: titolo del documento
                            - `window.document.forms[0]`: il primo form
                            - `window.document.forms[0].checkbox[0]`: la prima checkbox del primo form
                            - `window.document.forms[0].check1`: l’oggetto con nome “check1” nel primo form
                            - `window.document.myform`: l’oggetto “myform”
                            - `window.document.images[0]`: la prima immagine
                        - Ogni oggetto nella gerarchia è caratterizzato da un insieme di proprietà, metodi ed eventi che permettono di accedervi, controllarlo, modificarlo (Javascript implementa i metodi standard per accedere al DOM del documento)
            - Eccezioni
                ```javascript
                var x = prompt("Enter a number between 0 and 9:","");
                
                try {
                    if(x=="5") throw "line"
                    var el = document.getElementById("menu"+x)
                    var address= el.attributes["href"].value
                    return address;
                } catch(er) { 
                    if(er=="line") return "Errore Linea"; 
                    else return "Errore valore non adeguato";
                }
                ```

- AJAX
    - Nella programmazione Web «tradizionale» ogni passo dell'applicazione richiede di
        - Consultare il server
        - Eseguire una funzione dell'application logic
        - Generare l'HTML finale
        - Riceverlo e visualizzarlo
    - Per velocizzare si usa AJAX (Asynchronous Javascript And Xml)
        - Aumenta la dinamicità di pagine web, grazie allo scambio di piccole quantità di dati
        - Si possono utilizzare, oltre a XML, anche testo normale o dati in formato JSON
        - Permette alle pagine web di cambiare il proprio contenuto senza effettuare refresh dell’intera pagina
        - È eseguito all’interno del browser
        - È basato sul protocollo HTTP
        - Trasferisce dati in modo asincrono tra il browser e il web server (attraverso HTTP requests, tramite l'oggetto XMLHttpRequest)
        - Le HTTP requests sono inviate tramite JavaScript senza dover effettuare submit di form
        - AJAX si usa solitamente in un framework JS-AJAX che prevede già una gestione semplice della comunicazione e della modifica conseguente alla pagina
            - Tre scopi
                1. Astrazione: gestiscono le differenze tra un browser e l'altro e forniscono un modello di programmazione unico (o quasi) che funziona MOLTO PROBABILMENTE su tutti o molti browser
                2. Struttura dell'applicazione: forniscono un modello di progetto dell'applicazione omogeneo, indicando con esattezza come e dove fornire le caratteristiche individuali dell'applicazione
                3. Libreria di widget: forniscono una (più o meno) ricca collezione di elementi di interfaccia liberamente assemblabili per creare velocemente interfacce sofisticate e modulari
            - Framework scelto: jQuery

- JSON (JavaScript Object Notation) è un formato adatto all'interscambio di dati fra applicazioni client-server
    - Rispetto a XML è più leggero ed è gia integrato in JavaScript
        - Usare JSON in Javascript è particolarmente semplice
            - L'interprete è in grado di eseguirne il parsing da stringa ad oggetto JSON tramite una semplice chiamata alla funzione `parse()` e una funzione `stringify()` per il parsing da oggetto JSON a stringa
            - È basato sul concetto di array associativo e di oggetto di Javascript; poiché un array associativo può avere al suo interno un array o anche un altro array associativo, posso creare strutture gerarchiche arbitrariamente complesse
    ```json
    {
        "name": "Paola",
        "surname": "Salomoni",
        "address": {
            "street": "Via Zamboni 33",
            "city": "Bologna",
            "country": "Italy"
        },
        "phones": [
            { "sede": "Cesena", "num": "0547 338813" },
            { "sede": "Bologna", ', "num": "051 2094880" }
        ]
    }
    ```

- DOM (Document Object Model): rappresentazione dei documenti strutturati come modello orientato agli oggetti
    - Ogni documento caricato dal browser genera un DOM che specifica sottoforma di gerarchia di oggetti, tutti gli elementi di quel documento
    - Definisce sostanzialmente un’interfaccia di programmazione (API) per documenti sia HTML sia XML
        - Definisce la struttura logica dei documenti ed il modo in cui si accede e si manipola un documento
        - Utilizzando DOM i programmatori possono costruire documenti, navigare attraverso la loro struttura, e aggiungere, modificare o cancellare elementi
        - Ogni componente di un documento HTML o XML può essere letto, modificato, cancellato o aggiunto
    - Oggetti (ognuno specifica i metodi per accedere agli elementi a loro relativi)
        - DOMNode: interfaccia principale
        - DOMDocument: il documento di cui si sta parlando
        - DOMElement: ogni singolo elemento del documento
        - DOMAttr: ogni singolo attributo del documento
        - DOMText: ogni singolo nodo di testo del documento
        - ...

### Parte 11

- jQuery: libreria Javascript creata con l'obiettivo di semplificare la selezione, la manipolazione, la gestione degli eventi e l'animazione di elementi DOM in pagine HTML, nonché implementare funzionalità AJAX
    - Principi base
        - Separazione di Javascript e HTML
        - Brevità e chiarezza
        - Eliminazione di incompatibilità cross-browser
        - Estensibilità
    - Esempio: aggiungere a tutti gli elementi con classe goodbye la classe selected
        ```javascript
        // Javascript
        const d =document.getElementsByClassName("goodbye");
        let i;
        for(i =0; i <d.length; i++) {
            d[i].className=d[i].className+" selected";
        }

        //jQuery
        $(".goodbye").addClass( "selected");
        ```
    - Includere jQuery
        - Scaricare il file .js e mettere il riferimento nel file .html
        - Linkare il file, disponibile in un repository online
    - Sintassi
        - `$(selector).action()`
            - `$`: serve per utilizzare/accedere a jQuery
            - `(selector)`: permette di selezionare uno o più elementi della pagina HTML
            - `.action()`: definisce un’azione da eseguire sugli elementi selezionati
        - Il codice jQuery è solitamente inserito all’interno dell’evento document ready (per evitare che il codice sia eseguito prima che sia finito il caricamento del documento)
            ```javascript
            // Forma completa
            $(document).ready(function(){
                // jQuery
            });

            // Forma abbreviata
            $(function() {
                //jQuery
            });
            ```
    - Funzionalità
        - [Core](http://api.jquery.com/category/core/): libreria di base
        - [Selettori](http://api.jquery.com/category/selectors/)
            - Consentono di selezionare e manipolare elementi HTML
            - I selettori jQuery sono usati per trovare elementi HTML in base a: nome, id, classe, tipo, attributi, valore degli attributi...
            - Sono basati sui selettori CSS esistenti, e in aggiunta, ha alcuni propri selettori personalizzati
        - Attributi: metodi per fare get o set di attributi degli elementi
            - Metodo per gli attributi generici: `attr()`
            - Metodi per le classi
                - Per conoscere se un elemento appartiene ad una specifica classe: `hasClass()`
                - Per impostare o rimuovere una classe: `addClass()`, `removeClass()` e `toggleClass()`
            - Metodi per il contenuto
                - Per il codice HTML: `html()`
                - Per il contenuto testuale: `text()`
                - Per il valore, solitamente per i campi di un form: `val()`
        - [CSS](http://api.jquery.com/category/css/): metodi per fare get o set di proprietà CSS degli elementi
        - [Manipolazione](http://api.jquery.com/category/manipulation/): metodi che manipolano il DOM
        - [DOM Traversing](http://api.jquery.com/category/traversing/): metodi per attraversare e scorrere il DOM
        - [Eventi](http://api.jquery.com/category/events/): metodi per registrare comportamenti che hanno effetto quando l’utente interagisce con il browser
        - [Effetti](http://api.jquery.com/category/effects/): metodi dedicati all'effettistica
        - [AJAX](http://api.jquery.com/category/ajax/)
        - [Utilità](http://api.jquery.com/category/utilities/): metodi di gestione vettori, manipolazione stringhe...

### Parte 12

- Accessibilità
    - Definizioni
        - Accessibilità: la capacità dei sistemi informatici, nelle forme e nei limiti consentiti dalle conoscenze tecnologiche, di erogare servizi e fornire informazioni fruibili, senza discriminazioni, anche da parte di coloro che a causa di disabilità necessitano di tecnologie assistiveo configurazioni particolari
        - Tecnologie assistive: gli strumenti e le soluzioni tecniche, hardware e software, che permettono alla persona disabile, superando o riducendo le condizioni di svantaggio, di accedere alle informazioni e ai servizi erogati dai sistemi informatici
            - OUTPUT: Effettuano una conversione dell’informazione, in modo che possa essere percepita da un differente organo di senso
                - Sintesi vocale:  dallo schermo del PC (vista) al parlato corrispondente (udito)
                - Barra braille: dallo schermo del PC (vista) al testo corrispondente in Braille
                - Sottotitoli o trascrizioni: da audio (udito) a testo corrispondente (vista)
            - INPUT: Prevedono una modalità diversa d’interazione
                - Mouse speciali o strumenti di riconoscimento dei gesti o eyetracking, tutti corrispondenti alle funzioni di puntamento di un mouse tradizionale
                - Tastiere speciali o altri sistemi equivalenti ad una tastiera
    - Utilità
        - Accessibilità a persone disabili
            - Visive
                - Daltonismo
                - Ipovisione (tool assitivi di ingrandimento)
                - Cecità (accesso attraverso screen reader e/o voice browser)
            - Uditive (audio, uso della lingua scritta)
            - Motorie (accesso ai sistemi di input standard: tastiera, mouse, touch)
        - Aiuto nel caso di connessione lenta
        - Aiuto nel caso di hardware non prestante
    - W3C - WAI: Web Accessibility Initiative, gruppo di lavoro sull’accessibilità del Web, che ha identificato alcune linee guida e ha individuato diversi livelli di accessibilità
        - WCAG (Web Content Accessibility Guidelines)
            - Principi: percepibile, utilizzabile, comprensibile e robusto
            - Linee guida: definiscono il quadro di riferimento e gli obiettivi generali per comprendere i criteri di successo e applicare le tecniche
            - Criteri di successo
                - Livelli di conformità
                    - A (minimo)
                    - AA
                    - AAA (massimo, quasi utopia)
                - Tecniche
                    - Tipi (livello)
                        - Sufficienti, per soddisfare il criterio di successo
                        - Consigliate, che vanno oltre ciò che viene richiesto da ciascun singolo criterio di successo e consentono di rispettare le linee guida ad un livello più elevato
                    - Tipi (specificità)
                        - Generali (in questo caso sono codificate con G)
                        - Legate a una specifica tecnologia (per esempio quelle codificate H sono tecniche per l’HTML, quelle C per i CSS e così via)

### Parte 13

- PHP (PHP Hypertext Preprocessor): linguaggio di scripting interpretato concepito per la programmazione di pagine web dinamiche lato server
    - Architettura
        - L’interprete PHP preprocessa tutti i file con estensione PHP, che sono sostanzialmente file contententi HTML, all’interno dei quali è presente del codice PHP, contenuto all’interno dei delimitatori `<?php` e `?>`
    - Sintassi
        - C-like ma..
            - Tipizzazione più debole
            - Meno tipi di dati di base 
            - Vettori di dimensione variabile
            - Non supporta i puntatori
            - Supporta array associativi

        ![alt text](./res/esphp.png "Esempio di script in PHP")

    - Uso di più file php
        - Esempio
            ```php
            // dbtw-mysql.php
            <?php
                $host = "localhost";
                $user = "paola";
                $pass = "techweb";
                $database = "TW";
                
                // connessione DBMS
                $myconn = mysql_connect($host, $user, $pass) or die('Errore...');
                // connessione DB database
                mysql_select_db($database, $myconn) or die('Errore...');
            ?>
            ```
            - All'interno di tutte le pagine o i file che operano sul database TW possiamo includere il file dell’esempio in due modi equivalenti
                - `include «dbtw-mysql.php";` = se ci sono errori produce un Warning
                - `require"dati-mysql.php";` = se ci sono errori produce un Fatal Error
        - Import e require sono utilizzati anche per includere
            - Librerie: occorre ragionare sullo scope delle variabili (il codice che esso contiene eredita lo scope delle variabili della riga in cui si verifica l'inclusione) e del ritorno (ogni return termina l'esecuzione del file incluso/richiesto e torna il controllo al file php che lo ha incluso/richiesto)
            - HTML

### Parte 14

- Sintassi PHP
    - Variabili
        - `$nomevariabile`
            - Debolmente tipizzato: possiamo associare alla stessa variabile più tipi di dato
                ```php
                $qualcosa = 3;
                $qualcosa = true;
                $qualcosa = “ciao”;
                ```
        - Regole
            - Devono iniziare con una lettera o con il carattere underscore 
            - Non possono iniziare con un numero
            - Possono solo contenere caratteri alfanumerici e l’underscore
            - Case-sensitive
        - Scope
            - Local: una variabile dichiarata in una funzione ha visibilità locale e può essere usata solo in quella funzione
            - Global: una variabile dichiara fuori da una funzione ha scopo globale e può essere usata solo fuori dalla funzione, per accederci da dentro una funzione, usare la parola chiave global (dentro la funzione)
                - Se si vuole modificare una variabile globale da dentro una funzione occorre accedere al valore attraverso l’array (dizionario) che contiene tutte le variabili globali: `$GLOBALS['nome_della_variabile']`
            - Static: variabili che non vogliamo vengano cancellate o ri-inizializzate dopo l’uso; addirittura nelle funzioni non vengono ri-inizializzate (se richiamo la funzione e ho modificato la variabile statica questa "riprenderà" da dove era rimasta)
        - Tipi  
            - Boolean (falso = 0, vero = 1)
            - Integer
            - Float
            - String
                - Dettaglio
                    ```php
                    <?php
                        $nome = "Batman";
                        $stringa = "ciao $nome, come stai?";
                        // ciao Batman, come stai?
                        $stringa = 'ciao $nome, come stai?'; 
                        // ciao $nome, come stai?
                    ?>
                    ```
                - echo
                    ```php
                    <?php
                        $txt= "PHP";
                        echo "Mi piace programmare in $txt!<br>";
                        // Il . indica concatenazione di stringhe
                        echo "Mi piace programmare in " . $txt . "!<br>";
                        // La , indica che stiamo passando più parametri a echo
                        echo "Mi piace programmare in " , $txt , "!<br>";
                    ?> 
                    // Output in tutti i casi: Mi piace programmare in PHP
                    ```
                - Manipolazione
                    - Lunghezza: `strlen("ciao");` = 4
                    - Numero di parole: `str_word_count("Hello world!");` = 2
                    - Reverse: `strrev("Hello world!");` = !dlrow olleH
                    - Cerca stringa: `strpos("Hello world!", "world");` = 6
                    - Rimpiazzare: `str_replace("world", "Batman", "Hello world!");` = Hello Batman!
                    - ...
            - Array
                - Array a indice
                    - Creazione
                        - `$gelati = array("cornetto", "ghiacciolo","ricoperto");`
                        - `$gelati[0] = "cornetto"; $gelati[1] = "ghiacciolo"; $gelati[2] = "ricoperto";`
                    - Accedere ad un elemento: `$gelati[0];`
                - Array associativi
                    - Creazione
                        - `$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");`
                        - `$age['Peter'] = "35"; $age['Ben'] = "37"; $age['Joe'] = "43";`
                        - Accedere ad un elemento: `$age['Peter'];`
                - Array multidimensionali
                    - Creazione: `$age = array(1, array(2,3,5,6), “prova”, array(“ciao”,3, True));`
                - Numero elementi nell’array: `count($gelati);`
                - Sorting
                    - `sort()`: ordine crescente
                    - `rsort()`: ordine decrescente
                    - `asort()`: ordina gli array associativi in ordine crescente in base al valore
                    - `ksort()`: ordina gli array associativi in ordine crescente in base alla chiave
                    - `arsort()`: ordina gli array associativi in ordine decrescente in base al valore
                    - `krsort()`: ordina gli array associativi in ordine decrescente in base alla chiave
            - Object
                - PHP supporta
                    - Proprietà e metodi public, private, protected e static
                    - Ereditarietà
                    - Classi astratte
                    - Interfacce
                    - Tratti
                ```php
                <?php
                    class Persona {
                        private $nome;
                        public $cognome;

                        public function __construct($nome, $cognome) {
                            $this->nome = $nome;
                            $this->cognome = $cognome;
                        }
                        
                        public function presentati() {
                            echo "Sono " . $this->nome . " " . $this->cognome;
                        }
                    }

                    $gino = new Persona("Gino", "Pino");
                    $gino->presentati(); // Sono Gino Pino
                    echo $gino->nome; // Fatal error
                    echo $gino->cognome; // Pino
                ?>
                ```
            - NULL
            - `var_dump()` restituisce il tipo della variabile
        - Variabili Superglobali: variabili accessibili ovunque
            - `$GLOBALS`: memorizza tutte le variabili globali
            - `$_SERVER`: gestisce informazioni sul server
            - `$_GET`: usato per collezionare dati inviati con metodo GET
            - `$_POST`: usato per collezionare dati inviati con metodo POST
            - `$_COOKIE`: gestisce i cookie
            - `$_REQUEST`: usato per collezionare dati inviati sia con metodo GET che con metodo POST e i cookie
            - `$_SESSION`: gestisce le sessioni
    - Costanti (automaticamente globali)
        - Creazione: `define(name, value, case-insensitive)`
            - `name` = nome
            - `value` = valore
            - `case-insensitive` (opzionale) = specifica se il nome della costante dovrà essere case-insensitive, di default è falso
    - Operatori
        - Aritmetici: classici + `**` (elevamento a potenza)
        - Di assegnamento: classici
        - Di confronto
            - Classici
            - `$x === $y`: True sesono uguali e se sono dello stesso tipo
            - `$x !==$y`: True se i valori non sono uguali o non sono dello stesso tipo
            - `$x <> $y` o `$x != $y`: True se i valori sono diversi
        - Di incremento/decremento: classici
        - Logici: classici + notazione "a parole"
        - Di stringa
            - `$txt1 . $txt2`: concatenazione 
            - `$txt1 .= $txt2`: appende txt2 a txt1
        - Di array
            - `$x + $y`: unione dei due array
            - `$x == $y`: True se x e y hanno le stesse coppie chiave/valori
            - `$x === $y`: True se x e y hanno le stesse coppie chiave/valori, nello stesso ordine e dello stesso tipo
            - `$x != $y` o `$x <> $y`: True se x è diverso da y
            - `$x !== $y`: True se x non è identico a y
    - If / else: classici
    - Switch: classici
    - Loops
        - While: classico
        - Do / while: classico
        - For: classico
        - Foreach
            ```php
            <?php
                $colori = array("rosso", "verde", "blu", "giallo");
                
                foreach($colori as $colore) {
                    echo"$colore <br>";
                }
            ?>
            ```
            ```php
            <?php
                $age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
                
                foreach($age as $key => $value) {
                    echo "Key=" . $key . ", Value=" . $value;echo "<br>";
                }
            ?>
            ```
    - Funzioni
        ```php
        <?php
            function sum($x, $y) {
                $z = $x + $y;
                return$z;
            }
            
            echo "5 + 10 = " . sum(5,10) . "<br>";
            echo "7 + 13 = " . sum(7,13) . "<br>";
        ?>
        ```
        - Valori di default
            ```php
            <?php
                function nuovaColazione($numcaffe = 1, $numpaste = 1) {
                    echo "Colazione composta da: $numcaffe caffé e $numpaste paste" . "<br>";
                }
                
                nuovaColazione(2, 0);
                nuovaColazione(); // Valori di deafult
            ?>
            ```
    - Gestione file
        - Aprire un file e leggere il suo contenuto: `readfile("dizionario.txt");`
        - Aprire un file specificando più dettagli: `fopen($myfile,$permessi);` (sostanzialmente come in Python)
        - `fread()`: legge da un file aperto; il primo parametro è il file, il secondo la grandezza massima del file
            - `fread($myfile,filesize("dizionario.txt"));`
        - `fclose`: chiude un file
        - `fgets()`: legge una singola linea del file
        - `fwrite()` o `fputs()`: scrive blocchi di dati nel file
        - `feof()`: restituisce true quando si arriva alla fine del file
    - Usare GET e POST
        - Esempio GET
            ```html
            // esempio_get.html
            <form action="process_get.php" method="get">
                <label for="idsupereroe">Supereroe</label>
                <input type="text" id=" idsupereroe" name="supereroe">
                <input type="submit">
            </form>
            ```
            ```php
            // process_get.php
            <p>GET: <?php echo$_GET['supereroe'] ?></p>
            <p>POST: <?php echo$_POST['supereroe'] ?></p>
            <p>REQ: <?php echo$_REQUEST['supereroe'] ?></p>
            ```
            ```
            Output (inserendo "batman")
            
            GET: batman
            POST: Notice: Undefined index: supereroe in path\process_get.php on line 9
            REQ: batman
            ```
        - Esempio POST
            ```html
            // esempio_post.html
            <form action="process_get.php" method="post">
                <label for="idsupereroe">Supereroe</label>
                <input type="text" id=" idsupereroe" name="supereroe">
                <input type="submit">
            </form>
            ```
            ```php
            // PHP equivalente a quello precedente
            ```
            ```
            Output (inserendo "batman")
            
            GET: Notice: Undefined index: supereroe in C:\xampp\htdocs\esempi\esempio_post\process_get.php on line 8
            POST: batman
            REQ: batman
            ```
    - Usare i cookie
        ```php
        // Contare il numero di visite per utente
        <?php
            $nome_cookie = "numero_accessi";
            if(!isset($_COOKIE[$nome_cookie])) {
                echo "Cookie '" . $nome_cookie . "' non settato! Lo setto adesso!";
                $valore_cookie = 1;
                setcookie($nome_cookie, $valore_cookie, time() + (60 * 60 * 24 * 30), "/");
            } else {
                echo "Cookie '" . $nome_cookie . "' settato!<br>";
                $num_visite = $_COOKIE[$nome_cookie]+1;
                setcookie($nome_cookie, $num_visite, time() + (60 * 60 * 24 * 30), "/");
                echo "Il sito è stato visitato: " . $num_visite . " volte!";
            }
        ?>
        ```
        - È possibile cancellare un cookie semplicemente impostando un tempo di validità passato
    - Usare le session
        - Salvare un informazione direttamente sul server
        - Al browser viene assegnato un identificatore di sessione che viene registrato in un cookie (PHPSESSID)
        - Alla successiva interazione HTTP, PHP controllerà automaticamente se un id è stato inviato con la richiesta; se l’id è stato inviato, PHP controlla se esiste una sessione con quell’id e, in caso positivo, rende accessibili le informazioni salvate aggiungendole alla variabile super globale `$_SESSION`
        - Una sessione rimane aperta fino a quando il browser non viene chiuso  
            - Client: il cookie viene eliminato quando il browser viene chiuso
            - Server: la sessione viene eliminata quando scade il suo timer
        - È possibile salvare una variabile nella sessione in due modi
            - Usando direttamente la variabile super globale `$_SESSION` : `$_SESSION[‘name’] = "William";`
            - Usando la funzione `session_register()`
                ```php
                $name = "William";
                session_register("name");
                ```
            - Rimuovere dati
                - `unset()`: per rimuovere una singola variabile (`unset($_SESSION[‘name’])`)
                - `session_unset()`: rimuovere TUTTE le variabili
                - `session_destroy()`: rimuovere tutte le informazioni della sessione ma NON il cookie PHPSESSID (da usare con cautela!)
    - MySQLi (API per la comunicazione con un Database in PHP)
        ```php
        <?php
            $servername = "localhost";
            $username = "username";
            $password = "password";
            $sql = "CREATE DATABASE dbname;"; // Solo esempio, non fare cosi, usa i tool
            
            // Apertura connessione
            $conn = new mysqli($servername, $username, $password);
            // Esecuzione query
            $conn->query($sql) === TRUE;
            // Chiusura connessione
            $conn->close();
        ?>
        ```
        - Esecuzione di una query con i prepared statement (BETTER)
            ```php
            <?php
                $stmt = $conn->prepare("SELECT * FROM table WHERE columnname= ?");
                // s = string, i = int, d = double, b = BLOB
                $stmt->bind_param('s',$variabile);
                $stmt->execute();
            ?>
            ```

### Parte 15

- Usabilità e User experience
    - Usabilità
        - Definizione: grado in cui un prodotto può essere usato da particolari utenti per raggiungere certi obiettivi con efficacia, efficienza e soddisfazione in uno specifico contesto d'uso
        - Le tecniche di usabilità si propongono di mettere l’utente al centro dell’attenzione progettuale partendo dalla osservazione che un prodotto deve essere progettato per l’utente, dato che sarà l’utente ad usarlo
    - User experience: le percezioni e le reazioni di un utente che derivano dall’uso o dall’aspettativa d’uso di un prodotto, sistema o servizio
        - Tre dimensioni
            - Pragmatica: funzionalità e usabilità del sistema
            - Estetica/edonistica: piacevolezza estetica, emotiva e ludica del sistema
            - Simbolica: attributi sociali, forza del brand, identifi-cazione
        - Soggettiva: riguarda i pensieri e le sensazioni di un certo individuo nei confronti di un sistema
        - Dinamica: concerne gli aspetti esperienziali, affettivi... e include anche le percezioni personali su aspetti quali l’utilità, la semplicità d’utilizzo e l’efficienza del sistema che sono soggetti a mutamenti
        - Design
            - Target: design user-centered, mette l’utente ad operare al centro del design e dell’applicazione
                - Personas: descrizioni relative all'applicativo fatte dagli utenti dell'applicativo stesso
                - Scenarios: descrive in modo realistico la sequenza di azioni che una persona compie utilizzando un servizio
                - Experience prototype: si crea un'intera esperienza fittizia in cui l'utente sperimenta l’applicazione nel suo contesto d'uso; serve a comprendere le reali potenzialità del prodotto, le reazioni dell’utente, i punti di forza e le debolezze e tutto ciò che può essere migliorato in termini di UX, prima che il prodotto venga realmente sviluppato
                - Mock-up: appresentazione grafica della schermata (interfaccia) di interazione con il sistema che serve a mostrare come sarà un prodotto senza realizzarlo
                    - Balsamiq Mockup
                    - Adobe XD
                - Focus group: discussioni riguardo al prodotto fatte insieme a membri dell’utenza target; sono diversi dai test perchè mettono in luce ciò che gli utenti dicono che fanno e non il modo in cui effettivamente operano sul prodotto
                - Test con gli utenti: test che prevedono osservazioni, questionari e altri strumenti
                - Guidelines: indicazioni su come ottenere una interazione più efficace
    - Importante: i due concetti sopradescritti si sovrappongono ma
        - Usabilità: fa riferimento ai soli aspetti pragmatici (la capacità di svolgere un compito con efficienza, efficacia e soddisfazione)
        - Esperienza d’uso: include anche gli aspetti legati alla sfera delle emozioni
